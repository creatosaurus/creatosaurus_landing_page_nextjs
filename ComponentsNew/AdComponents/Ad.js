import { useEffect, useRef } from 'react'
import AppIntegrationImage from '../../public/NewAssets/HeroSection/app-integration.webp'
import DesignEditorImage from '../../public/NewAssets/HeroSection/design-editor.webp'
import AiChatImage from '../../public/NewAssets/HeroSection/ai-chat.webp'
import StockAssetsImage from '../../public/NewAssets/HeroSection/stock-assets.webp'
import SocialAnalyticsImage from '../../public/NewAssets/HeroSection/social-analytics.webp'
import VideoEditorImage from '../../public/NewAssets/HeroSection/video-editor.webp'
import DocEditorImage from '../../public/NewAssets/HeroSection/doc-editor.webp'
import SocialInboxImage from '../../public/NewAssets/HeroSection/social-inbox.webp'
import BrandKitImage from '../../public/NewAssets/HeroSection/brand-kit.webp'
import PostSchedularImage from '../../public/NewAssets/HeroSection/post-schedular.webp'
import AiImageToolsImage from '../../public/NewAssets/HeroSection/ai-image-tools.webp'
import Image from 'next/image'

const Ad = ({ setShowLogin, refresh }) => {
    const scrollContainerRef = useRef(null);

    const data = [{
        title: "App Integrations",
        color: "#FF4359",
        img: AppIntegrationImage
    }, {
        title: "Social Analytics",
        color: "#0078FF",
        img: SocialAnalyticsImage
    }, {
        title: "Video Editor",
        color: "#8A43FF",
        img: VideoEditorImage
    }, {
        title: "AI Chat",
        color: "#00B0A7",
        img: AiChatImage
    }, {
        title: "Design Editor",
        color: "#FF8A25",
        img: DesignEditorImage
    }, {
        title: "AI Image Tools",
        color: "#FF4359",
        img: AiImageToolsImage
    }, {
        title: "Doc Editor",
        color: "#1410C7",
        img: DocEditorImage
    }, {
        title: "Post Scheduler",
        color: "#0078FF",
        img: PostSchedularImage
    }, {
        title: "Social Inbox",
        color: "#B938D9",
        img: SocialInboxImage
    }, {
        title: "Stock Asset",
        color: "#009B77",
        img: StockAssetsImage
    }, {
        title: "Brand Kit",
        color: "#FF6543",
        img: BrandKitImage
    }]

    useEffect(() => {
        const scrollWidth = scrollContainerRef.current.scrollWidth;
        const containerWidth = scrollContainerRef.current.clientWidth;
        scrollContainerRef.current.scrollLeft = (scrollWidth - containerWidth) / 2;
    }, [refresh]);


    const handleScrollRight = () => {
        scrollContainerRef.current.scrollBy({
            left: scrollContainerRef.current.clientWidth,
            behavior: 'smooth'
        })
    }

    const handleScrollLeft = () => {
        scrollContainerRef.current.scrollBy({
            left: -scrollContainerRef.current.clientWidth,
            behavior: 'smooth'
        })
    }

    const getCookie = (name) => {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const checkUserLoggedIn = () => {
        const token = getCookie("Xh7ERL0G")

        if (token) {
            window.open("https://www.app.creatosaurus.io/", "_blank")
        } else {
            setShowLogin(true)
        }
    }

    return (
        <div onClick={checkUserLoggedIn} className='border-[1px] border-[#DCDCDC] bg-[#FFF1F180] mt-[20px] py-[25px] rounded-[20px] flex flex-col cursor-pointer w-full hover:shadow-[0_0_3px_0px_rgba(0,0,0,0.3)]'>
            <div className='px-[10px] flex flex-col justify-center  items-center'>
                <span className='text-[24px] font-semibold text-center'>AI Social Media Marketing Tool</span>
                <p className='text-[12px] text-center mt-[10px]'>Creatosaurus makes it easy for marketing teams to create and share visually stunning content, together.</p>
                <button onClick={checkUserLoggedIn} className='h-[30px] bg-black text-white mt-[15px] rounded-[5px] text-[14px] font-medium w-full max-w-[220px] m-auto transition-transform duration-300 ease-in-out transform hover:scale-[1.02]'>
                    Start Creating—It's Free
                </button>
            </div>

            <div className='relative flex items-center mt-[15px]'>
                <div className='flex items-center gap-x-[10px] overflow-x-scroll w-full h-[160px]' ref={scrollContainerRef}>
                    {
                        data.map((info, index) => {
                            return <div
                                key={`${info.title + index}`}
                                style={{ backgroundColor: info.color }}
                                onClick={checkUserLoggedIn}
                                className='w-[120px] min-w-[120px] h-[150px] rounded-[15px] p-[10px] flex flex-col transition-transform duration-300 ease-in-out transform hover:scale-[1.02] hover:z-10 cursor-pointer'>
                                <span className='mt-[5px] text-[11px] font-medium text-white text-center mb-[10px]'>{info.title}</span>
                                <div className='relative rounded-[10px] overflow-hidden'>
                                    <Image src={info.img} alt='' />
                                </div>
                            </div>
                        })
                    }
                </div>

                <svg onClick={handleScrollLeft} className='absolute size-[20px] left-[10px] group cursor-pointer z-20' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle className='group-hover:stroke-black' cx="12.5" cy="12.5" r="12" fill="white" stroke="#333333" />
                    <path className='group-hover:stroke-black' d="M14.25 9L10.5 12.75L14.25 16.5" stroke="#333333" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>

                <svg onClick={handleScrollRight} className='absolute size-[20px] right-[10px] group cursor-pointer z-20' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle className='group-hover:stroke-black' cx="12.5" cy="12.5" r="12" fill="white" stroke="#333333" />
                    <path className='group-hover:stroke-black' d="M10.625 8.75L14.375 12.5L10.625 16.25" stroke="#333333" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </div>
        </div>
    )
}

export default Ad