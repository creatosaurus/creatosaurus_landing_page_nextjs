import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

const ImageShowCase = () => {
    return (
        <div className='w-full max-w-[768px] mt-[50px] self-center'>
            <Link href="https://www.muse.creatosaurus.io/">
                <a>
                    <div>
                        <Image
                            src="https://d1vxvcbndiq6tb.cloudfront.net/37a3a5de-3638-4ae0-b2d4-c7fe3ca26220"
                            width={100}
                            height={55}
                            priority={true}
                            layout='responsive'
                            alt='poster' />
                    </div>
                </a>
            </Link>
            <Link href="https://www.muse.creatosaurus.io/">
                <a>
                    <div className="mt-[30px] text-[14px] sm:text-[18px] font-semibold flex justify-center items-center text-[#0078FF] hover:underline">
                        <span>Try Free Online Quote Post Maker & Creator</span>
                        <svg className='ml-[10px] size-[12px] sm:size-[16px]' width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.25 7.5H14.75M14.75 7.5L8.375 1.125M14.75 7.5L8.375 13.875" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                    </div>
                </a>
            </Link>
        </div>
    )
}

export default ImageShowCase