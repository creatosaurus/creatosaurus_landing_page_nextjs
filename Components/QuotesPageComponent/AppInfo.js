import React from 'react'
import style from '../../styles/Quotes.module.css'
import QuoteBanner from '../../public/Assets/QuoteBanner.png';
import AnshulImage from '../../public/Assets/Anshul.jpg'
import Image from 'next/image';

const AppInfo = () => {
    return (
        <div className={style.appInfoContainer}>
            <div className={style.appInfo}>
                <h1>Free online quote poster maker and generator</h1>
                <p>Create quote post at scale with our
                    massive 1 million library of quotes and
                    easy to design beautiful templates.</p>

                <a href='https://www.muse.creatosaurus.io/' target='_blank' rel="noreferrer">Free quote poster maker <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                    <path d="M1.07227 7.5H12.6348M12.6348 7.5L8.88476 3.75M12.6348 7.5L8.88476 11.25" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                </svg></a>

                <div className={style.founderMessage}>
                    <p>“Creatosaurus combines creator workflow
                        with design unlocking productivity for creators.”</p>
                </div>

                <div className={style.founderInfo}>
                    <div className={style.leftSide}>
                        <div className={style.founderImage}>
                            <Image src={AnshulImage} width={50} height={50} objectFit="cover" />
                        </div>
                    </div>

                    <div className={style.rightSide}>
                        <span>Anshul Motwani</span>
                        <p>Founder, Wittypens</p>
                    </div>
                </div>
            </div>

            <div className={style.appBanner}>
                <Image src={QuoteBanner} />
            </div>
        </div>
    );
}

export default AppInfo;