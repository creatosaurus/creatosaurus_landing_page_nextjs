import React, { useState } from 'react'
import Link from 'next/link'

const FAQSection = () => {
    const [activeIndex, setActiveIndex] = useState([0])

    const faq = [
        {
            question: "Can I use Creatosaurus for free?",
            ans: "Creatosaurus is free for anyone using it for personal, individual purposes. If you're using Creatosaurus for work, be sure to upgrade your plan. Whether you're a creator, a startup, or an agency, we have tailored plans to suit your needs. With Creatosaurus Teams, you’ll get all the premium features of the Creatosaurus Pro plan, plus enhanced tools for team collaboration—including streamlined approval workflows, real-time communication, and centralized asset management. Ready to try it out? Start a free 14-day trial of either Creatosaurus Pro or Creatosaurus Teams today."
        }, {
            question: "Is Creatosaurus Pro available for teams?",
            ans: "Creatosaurus Pro unlocks advanced tools and features for individual creators or professionals looking to tell their story through visually stunning content. However, if you're collaborating with your team members, you'll need the Creatosaurus Teams plan, which allows you to seamlessly share, edit, and communicate with your team in one convenient platform."
        }, {
            question: "Is Creatosaurus safe for my social accounts?",
            ans: (
                <>
                    Creatosaurus utilizes the official developer APIs of Instagram, Facebook, TikTok, X, and Pinterest to access data. We are regulated by the API guidelines of these platforms and do not perform any unauthorized actions on your social accounts. Additionally, we do not store any data from your social accounts on our servers. For more information, please read our <span className='underline text-[#0000EE]'><Link href="/privacy">Privacy Policy</Link></span>.
                </>
            )
        }, {
            question: "Is Creatosaurus available on mobile or desktop?",
            ans: "We offer both a web application and mobile app available on the Google Play Store and Apple App Store."
        }, {
            question: "Which Creatosaurus plan is better for me?",
            ans: "If you’re an individual creator looking to craft unique content and tell your story, try Creatosaurus Pro. If you’re a startup seeking better collaboration and communication within your team, try Creatosaurus Teams. For agencies that need to maintain brand voice while managing multiple clients, Creatosaurus Agency is the ideal solution. And if you’re a global organization with a diverse team across various locations, aiming to maintain content and brand consistency, try Creatosaurus Enterprise."
        }, {
            question: "Can I change my plan?",
            ans: (
                <>
                    Yes, you can! You can upgrade your current plan to a higher tier at any time, which will enhance your workflow. If you wish to downgrade your plan, feel free to reach out to our <span className='underline text-[#0000EE]'><Link href="/contact">sales team.</Link></span>  The difference in amount will be applied to your billing cycle.
                </>
            )
        }, {
            question: "How many social channels are supported?",
            ans: "At Creatosaurus, users can easily add and manage all their essential social channels in one place. We support platforms like Facebook, LinkedIn, Instagram, YouTube, X, Threads, Pinterest, Tumblr, TikTok, and more."
        }, {
            question: "What are AI credits?",
            ans: (
                <>
                    Credits function as the currency within Creatosaurus, granting access to a range of AI-driven features such as AI Chat, AI Image Generator, AI Background Remover, and more. For text generation, credits are calculated based on the total number of words used in both the input and output fields, with the corresponding words deducted from your credits each time. In terms of image generation, credits are measured by "credit tokens," with each AI image operation requiring a specific number of tokens. For example, 2 credit tokens are consumed for image generation. You can find detailed information about the distribution of AI image credits in the help document here. Each creation requires credits, serving as tokens to inspire your creativity. These credits reset every month, and for paid plans, they are refreshed every 30 days from the date of purchase. Please note that credits do not roll over. Learn more about using credits here. If you run low on credits and all your credits are utilized, you can purchase additional add on credits by reaching out to our <span className='underline text-[#0000EE]'><Link href="/contact">sales team.</Link></span>
                </>
            )
        }, {
            question: "Does Creatosaurus offer discounts for nonprofit or educational organizations?",
            ans: 'Yes, we offer a 50% off for nonprofits & educational institutes on any of our plans billed annually.'
        }, {
            question: "How many social media channels can I manage?",
            ans: (
                <>
                    In Creatosaurus, you can publish to multiple channels within a workspace, with the number of connected social accounts depending on your plan. For example, the Creatosaurus Teams plan allows you to connect up to 15 channels, which can include Facebook, LinkedIn, Instagram, YouTube, X, Threads, Pinterest, Tumblr, TikTok, and more. You can even connect multiple accounts from the same platform. If you need to publish to more channels than your plan permits, you can purchase a social media channel add-on to increase your limit. For additional channel options, <span className='underline text-[#0000EE]'><Link href="/contact">contact sales.</Link></span>
                </>
            )
        }, {
            question: "How can I learn more about how to use Creatosaurus?",
            ans: (
                <>
                    We offer comprehensive <span className='underline text-[#0000EE]'><Link href="https://creatosaurus.tawk.help/">help documentation</Link></span> for Creatosaurus, detailing how to use each feature and tool extensively. Additionally, you can watch our <span className='underline text-[#0000EE]'><Link href="https://www.youtube.com/watch?v=eS5tpAUEuzA">video tutorial</Link></span> for more guidance. We also host a free online  <span className='underline text-[#0000EE]'><Link href="https://calendly.com/malavwarke/creatosaurus">live demo</Link></span> every week, where you can interact with our team and learn valuable tips and tricks about Creatosaurus. If your team requires onboarding training, don’t hesitate to contact our <span className='underline text-[#0000EE]'><Link href="/contact">sales team.</Link></span>
                </>
            )
        }, {
            question: "What payment methods can I use?",
            ans: (
                <>
                    We accept all major credit cards, including Visa, Mastercard, and American Express. Additionally, we support other online payment methods in certain countries like debit card, net banking, UPI, wallet and more.  For more information, check out this <span className='underline text-[#0000EE]'><Link href="https://creatosaurus.tawk.help/">help article.</Link></span>
                </>
            )
        }, {
            question: "Will I receive an invoice for my transactions?",
            ans: "You can access the invoice for all transactions in your account. Go to your settings select 'Payments & Invoices' tab."
        }, {
            question: "What is the refund policy?",
            ans: (
                <>
                    You can request a full refund within the first 14 days of purchase, with only transaction and payment gateway fees deducted. No refunds will be issued after 15 days. To request a refund within the 14-day period, please contact us through the <span className='underline text-[#0000EE]'><Link href="/contact">sales form.</Link></span>
                </>
            )
        }, {
            question: "How can I contact the sales team?",
            ans: "We provide support through live chat, WhatsApp chat, Facebook group, Discord community and Email support."
        }, {
            question: "What if I have more questions?",
            ans: (
                <>
                    Feel free to ask your additional questions! We are here to help. You can either reach out to us via chat or through the contact <span className='underline text-[#0000EE]'><Link href="/contact">sales form.</Link></span>
                </>
            )
        }
    ]

    const removeIndex = (index) => {
        let filterData = activeIndex.filter(data => data !== index)
        setActiveIndex(filterData)
    }

    return (
        <div className="mt-[100px] flex flex-col justify-center items-center max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <span className="text-[28px] text-center sm:text-[36px] md:text-[44px] font-semibold">Frequently Asked Questions</span>
            <p className="text-[14px] sm:text-[16px] md:text-[18px] mb-[20px] text-[#333333] max-w-[1000px] text-center mt-[15px]">We hope these frequently asked questions about Creatosaurus pricing plans provide clarity. If you have any more questions, please don't hesitate to reach out to our support team for assistance.</p>

            {
                faq.map((data, index) => {
                    return <div
                        style={activeIndex.includes(index) ? { backgroundColor: '#FAFAFA' } : null}
                        key={data}
                        onClick={() => activeIndex.includes(index) ? removeIndex(index) : setActiveIndex((prev) => [...prev, index])}
                        className="mt-[20px] py-[20px] px-[25px] rounded-[10px] w-full max-w-[1050px] hover:bg-[#FAFAFA] cursor-pointer">
                        <div className="flex justify-between items-center">
                            <span className="text-[14px] sm:text-[16px] md:text-[18px] font-medium">{data.question}</span>
                            {
                                activeIndex.includes(index) ?
                                    <svg className="cursor-pointer" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M8.35686 16.0713L13.4997 10.9284L18.6426 16.0713" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                    :
                                    <svg className="cursor-pointer" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M18.6431 10.9282L13.5003 16.0711L8.35742 10.9282" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                            }
                        </div>

                        {
                            activeIndex.includes(index) &&
                            <p className="text-[12px] sm:text-[14px] text-[#333333] mt-[10px] cursor-pointer">{data.ans}</p>
                        }
                    </div>
                })
            }
        </div>
    )
}

export default FAQSection