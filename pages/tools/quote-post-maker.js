import React, { useState } from 'react'
import Head from 'next/head';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar'

import museStyle from '../../styles/muse.module.css';
import Image from 'next/image';
import FrequentlyAskedQuestions from '../../Components/FaqSection';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../Components/LandingPageComponents/Footer';
import apps from "../../appsInfo";

import Image1 from '../../public/Assets/QuotePostMaker/image1.gif'
import Image2 from '../../public/Assets/QuotePostMaker/image2.gif'
import Image3 from '../../public/Assets/QuotePostMaker/image3.gif'
import Image4 from '../../public/Assets/QuotePostMaker/Image5.png'
import Image7 from '../../public/Assets/QuotePostMaker/image6.gif'
import Image8 from '../../public/Assets/QuotePostMaker/image8.gif'
import Image9 from '../../public/Assets/QuotePostMaker/image9.gif'
import Image6 from '../../public/Assets/Muse/5threads.webp'

import RatingComponent from '../../Components/RatingComponent';

const ScheduleInstagramStories = () => {
    const openUrl = (url) => {
        window.open(url)
    }

    const [showVideo, setShowVideo] = useState(false)

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };

    let faq = [
        {
            id: 1,
            title: "Is quote post maker free to use?",
            content: "Yes, it is free to use. No credit card is required to sign up. Get started now.",
        }, {
            id: 2,
            title: "How to add photos to quote posters?",
            content: "To templates to quote posters, choose a template with photo options, upload your desired image, adjust placement and size, then save and share.",
        }, {
            id: 3,
            title: "What is the best font for quotes?",
            content: "The best font for quotes depends on the desired style, but popular choices include Open Sans, Lucinda Grande, Playfair Display, Century School, Proxima Nova, Gotham, Effra, Vollkorn, Signalist, Georgia, Platin, and Helvetica.",
        }, {
            id: 4,
            title: "What makes a good quote poster?",
            content: "A good quote poster combines visually appealing design, legible typography, and relevant imagery to create an impactful and inspiring visual for quote posts.",
        }, {
            id: 5,
            title: "What is Quote Post Maker?",
            content: "Quote post maker & creator helps you to create captivating quote posters to inspire people with your words of wisdom and help them to push through a tough day.",
        }
    ]

    let data = [
        {
            title: "Inspire everyone with your quote posters",
            p: "In today's digital age, quote posts have become an effective way for sharing inspiration and motivation with people around you.With Creatosaurus’s Free Quote Post Maker, you can easily create visually appealing quote posts with 100s of captivating designs that share words of wisdom, help your audience push through a tough day, and convey your message in a creative way.",
            image: Image2
        }
    ]

    let data2 = [
        {
            title: "The Power of Quote Posts: How to Captivate Your Audience with Inspiring Words",
            p: "Unlock the Power of Quote Posts with Creatosaurus’s Quote Post Maker. Create inspirational, motivational quote posts that captivate your audience. Engage your audience with words that uplift, motivate, and ignite their inner fire. Let your quote posts be a source of inspiration, spreading positivity and fueling dreams with noteworthy designed quote posters. Customize and design unique visuals with ease using Quote Post Maker, your ultimate tool for crafting compelling quote posts.",
            image: Image3
        },
        {
            title: "Quote Post Maker: Inspire and Motivate with Free, Customizable Motivational Quotes",
            p: "Unlock the Power of Inspiration with Quote Post Maker: Create engaging, motivational quote posts that captivate your audience. Customize and design unique visuals with ease using Quote Post Maker, your ultimate tool for crafting compelling quote posts. Create Quote Post Now today!",
            image: Image4
        },
        {
            title: "1 million+ quotes library for your quote design",
            p: "Creatosaurus’s template library Unlocks the power of creativity with our Quote Post Generator! Access over 1 million quotes from our vast library and transform them into stunning designs using our Quote Post Maker. Share wisdom, inspiration, and motivation like never before. Let your words resonate with the world!",
            image: Image7
        },
        {
            title: "100s of Templates for your quote design",
            p: "Discover endless possibilities with our Quote Post Maker! Access 100s of templates to craft stunning quote designs. Engage your audience, spread inspiration, and make your message stand out. Elevate your content and create impactful quote posts effortlessly. Unleash your creativity now!",
            image: Image8
        },
        {
            title: "Efficiently Create and Save Drafts of Quotes with our quote post maker",
            p: "Maximize Productivity with our Quote Post Maker: Seamlessly Create and Save Drafts of Inspirational Quotes for Instagram Posts. Harness the Power of Efficiency to Craft Engaging Visuals and Inspire Your Audience. Get Started Now!",
            image: Image9
        }
    ]

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Free Quote Post Maker with 1M+ Quotes | Online Quote Creator & Generator</title>
                <meta
                    name="title"
                    content="Free Quote Post Maker with 1M+ Quotes | Online Quote Creator & Generator"
                />
                <meta
                    name="description"
                    content="Create and share quote posters with 1 Million+ quotes library using Quote Post Maker. Hundreds of free templates to customize image designs & share your quote post."
                />
                <meta
                    property="og:title"
                    content="Free Quote Post Maker with 1M+ Quotes | Online Quote Creator & Generator"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Create and share quote posters with 1 Million+ quotes library using Quote Post Maker. Hundreds of free templates to customize image designs & share your quote post."
                    key="description"
                />

                <meta name="keywords" content="quote post maker (10 – 100), quote post (1K – 10K), quote post generator, free, online, muse, creatosaurus, templates, tool, quote maker, quote creator" />
            </Head>
            <NavigationBar />


            <div className={museStyle.top_block}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{ boxShadow: 'none' }}>
                        <Image src={Image1} alt='Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates' />
                    </div>
                    <div className={museStyle.blockText}>
                        <div className={museStyle.blockText_title}>
                            <h1>Introducing Quote Post Maker & Creator</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>Unlock your creativity with 100s of captivating quote post templates. Using Creatosaurus’s 1 million+ quotes library generate quote posts for free.</p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button
                                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                onClick={() => {
                                    openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&id=636525eec129de00088b933f");
                                }}>
                                Create Quote Post Now
                            </button>
                        </div>
                        <span style={{ color: '#828282', fontSize: 10 }}>No credit card required</span>
                    </div>
                </div>
            </div>

            <RatingComponent />

            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&id=636525eec129de00088b933f");
                                        }}>Create Quote Post Now
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.how_to_container}>
                <h2>How to make quote post in 3 easy steps</h2>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M7.5 12l-2.004 2.672a2 2 0 00.126 2.552l3.784 4.128c.378.413.912.648 1.473.648H15.5c2.4 0 4-2 4-4 0 0 0 0 0 0V9.429M16.5 10v-.571c0-2.286 3-2.286 3 0" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M13.5 10V8.286c0-2.286 3-2.286 3 0V10M10.5 10V7.5c0-2.286 3-2.286 3 0 0 0 0 0 0 0V10M10.5 10V7.5 3.499A1.5 1.5 0 009 2v0a1.5 1.5 0 00-1.5 1.5V15" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>1. Select your quote post template</h4>
                        <p>Get Inspired with 100+ Quote Post Templates: Spark Creativity and Share Wisdom with Captivating Visuals. Create Memorable Quotes Today!</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M14.363 5.652l1.48-1.48a2 2 0 012.829 0l1.414 1.414a2 2 0 010 2.828l-1.48 1.48m-4.243-4.242l-9.616 9.615a2 2 0 00-.578 1.238l-.242 2.74a1 1 0 001.084 1.085l2.74-.242a2 2 0 001.24-.578l9.615-9.616m-4.243-4.242l4.243 4.242" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>2. Customize with 1 million+ quotes</h4>
                        <p>Upload your own photos, browse through free stock image libraries, and choose quotes from our 1 million+ quote library. Edit & customise it until you are happy.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M12 13v9m0 0l3.5-3.5M12 22l-3.5-3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>3. Download, Share, or Print</h4>
                        <p>Download your quote post design to your device in high quality to share or print it. Post it on social accounts straight from the design editor using Cache.</p>
                    </div>
                </div>
            </div>


            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data2.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 10 }}>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&id=636525eec129de00088b933f");
                                        }}>Create Quote Post Now
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>

            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/5C3AXWNxalo'
                    title='Threads Post Templates for Free | Best Design Templates for Threads Instagram App'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>


            <div className={museStyle.tipsBlock}>
                <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 30 }}>5 tips for creating quotes post to maximize engagement</h2>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>1. Be concise :</span> Keep your quotes short and impactful to grab attention and make them easily shareable.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>2. Use eye-catching visuals :</span> Pair your quotes with visually appealing images or elements to enhance their impact and attract viewers.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>3. Stay on-brand :</span> Align your quotes with your brand&apos;s tone and values to maintain consistency and reinforce your brand identity.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>4. Incorporate variety :</span> Mix up the types of quotes you share, including motivational, funny, or thought-provoking quotes, to cater to different interests and emotions.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>5. Encourage interaction :</span> Include a call-to-action in your quotes to prompt your audience to engage by liking, sharing, or commenting on the post.</p>
            </div>


            <div className={museStyle.redPoster} style={{ backgroundColor: '#FF8A25' }}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h2 style={{ fontSize: 34, fontWeight: 700, color: "#FFFFFF" }}>Muse - Online Photo Editing Tool & Free Graphic Design Editor</h2>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Our pro design tool offers a wide range of templates for your social media designs. Create stunning visuals, customize templates, and make your brand stand out. Elevate your design game with our user-friendly online graphic design tool today!
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&id=636525eec129de00088b933f")}>Create Quote Post Now {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <svg onClick={() => setShowVideo(true)} style={showVideo ? { display: 'none' } : null} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg" color="#FFF">
                            <path d="M6.906 4.537A.6.6 0 006 5.053v13.894a.6.6 0 00.906.516l11.723-6.947a.6.6 0 000-1.032L6.906 4.537z" stroke="#FFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <div style={showVideo ? null : { display: 'none' }}>
                            <iframe
                                className={museStyle.videoBlock}
                                src='https://www.youtube.com/embed/eS5tpAUEuzA?start=492&autoplay=1'
                                title='YouTube video player'
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            >
                            </iframe>
                        </div>

                        <div style={!showVideo ? null : { display: 'none' }}>
                            <Image src={Image6} alt='muse_img loading...' />
                        </div>
                    </div>
                </div>
            </div>

            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>
                        })}
                        <button onClick={() => window.open("https://www.creatosaurus.io/tools/schedule-instagram-stories")}>Instagram Story Scheduler Tool</button>
                    </div>
                </div>
            </div>

            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default ScheduleInstagramStories