import React from 'react'
import PopupLayout from '../PopupLayout/PopupLayout'
import CloseButton from './CloseButton'
import Link from 'next/link'

const PaymentFailer = ({ close }) => {
    return (
        <PopupLayout close={close}>
            <div className='relative'>
                <CloseButton close={close} />
                <div className='flex flex-col bg-white w-[80vw] max-w-[500px] rounded-[10px] relative overflow-hidden p-[15px]' onClick={(e) => e.stopPropagation()}>
                    <span className='text-[14px] font-semibold pb-[10px] border-b-[0.5px] border-[#DCDCDC] w-full'>Oops! Your payment didn’t go through</span>

                    <p className='text-[12px] text-[#333333] mt-[15px]'>Welcome to the next level of creativity and growth. With your Pro plan, you now have access to powerful AI tools designed to supercharge your content creation, marketing, and social media strategy.</p>

                    <button className='bg-black rounded-[5px] text-[12px] font-semibold text-white h-[34px] mt-[15px]' onClick={close}>Try again</button>
                </div>
            </div>
        </PopupLayout>
    )
}

export default PaymentFailer