import React, { useEffect } from 'react'

const PopupLayout = ({ children, close }) => {

    // Disable background scroll when popup is open
    useEffect(() => {
        document.body.style.overflow = 'hidden'

        return () => {
            document.body.style.overflow = 'auto'
        };
    }, []);

    return (
        <div onClick={close} className='fixed top-0 left-0 right-0 bottom-0 bg-[#00000A73] backdrop-blur-[5px] z-[101] flex justify-center items-center'>
            {children}
        </div>
    )
}

export default PopupLayout