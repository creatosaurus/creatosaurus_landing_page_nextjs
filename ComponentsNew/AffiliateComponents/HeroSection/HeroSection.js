import React, {useState} from 'react'
import styles from './HeroSection.module.css'
import Image from 'next/image'
import Banner from '../../../public/Assets/affiliateBanner.png'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'

const HeroSection = () => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={styles.heroSectionContainer}>
                <div className={styles.leftSideBar}>
                    <span className={styles.subHead}>AFFILIATE PARTNER PROGRAM</span>
                    <h1>Recommend Creatosaurus, Earn 30% Recurring Revenue</h1>
                    <p>Looking to create an income stream with a reliable affiliate program? Look no further than Creatosaurus. Ready made marketing material and full access to our platform. Start earning today!</p>
                    <button className={styles.mainButton} onClick={()=> setShowLogin(true)}>Join Now</button>
                    <div className={styles.placeHolder}>
                        <div className={styles.item}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                                <path d="M3.125 8.125L5.625 10.625L11.875 4.375" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                            Powerful Tracking
                        </div>
                        <div className={styles.item}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                                <path d="M3.125 8.125L5.625 10.625L11.875 4.375" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                            Reliable Payments
                        </div>
                        <div className={styles.item}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                                <path d="M3.125 8.125L5.625 10.625L11.875 4.375" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                            30 Day Cookie
                        </div>
                    </div>
                </div>

                <div className={styles.rightSide}>
                    <Image priority={true} src={Banner} alt="hero" objectFit='contain' />
                </div>
            </div>
        </>
    );
}

export default HeroSection;