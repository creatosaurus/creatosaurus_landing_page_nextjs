import React, { useState } from 'react'
import style from './PriceCutSection.module.css'
import Image from 'next/image'
import Group from '../../../public/NewAssets/Home/group.png'
import Group2 from '../../../public/NewAssets/Home/group2.png'
import Group3 from '../../../public/NewAssets/Home/group3.png'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'
import Link from 'next/link'

const PriceCutSection = ({ pricing }) => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={style.priceCutContainer}>
                <h2>
                    {
                        pricing ?
                            "Consolidate AI Marketing tools & save cost."
                            :
                            "Struggling to Scale Social Media Marketing? Consolidate social media tools. Cut costs."
                    }
                </h2>
                <p>
                    {
                        pricing ?
                            "Dealing with losing audience engagement, missing deadlines, collaboration issues, content quality concerns, poor ROI, and frustrated teams?"
                            :
                            "Dealing with losing audience engagement, missing deadlines, collaboration issues, content quality concerns, poor ROI, and frustrated teams?"
                    }
                </p>
                <div className={style.imageContainer}>
                    <div className={`${style.imageContainer} ${style.laptop}`}>
                        <div className={style.compitiar}>
                            <Image src={Group2} alt='group' />
                        </div>
                        <div>
                            <Image src={Group} alt='group' />
                        </div>
                    </div>

                    <div className={`${style.imageContainer} ${style.mobile}`}>
                        <Image src={Group3} alt='group' />
                    </div>

                    {
                        pricing ?
                            <Link href="#pricingCards">
                                <button style={{ backgroundColor: 'black' }} className='text-white transition-transform duration-300 ease-in-out transform hover:scale-[1.04]'>Get Creatosaurus & Save 80% Cost</button>
                            </Link>
                            :
                            <button onClick={() => setShowLogin(true)} style={{ cursor: 'pointer' }}>Get Started—It&apos;s Free</button>
                    }
                </div>
            </div>
        </>
    )
}

export default PriceCutSection