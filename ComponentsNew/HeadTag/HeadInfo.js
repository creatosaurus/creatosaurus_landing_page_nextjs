import React from 'react'
import Head from 'next/head'

const HeadInfo = () => {
    return (
        <Head>
            <link rel="icon" href="/Assets/creatosaurusfavicon.ico" />
            <title>Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale</title>
            <meta
                name="title"
                content="Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale"
            />
            <meta
                name="description"
                content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
            />
            <meta
                property="og:title"
                content="Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale"
                key="title"
            />
            <meta
                property="og:description"
                content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps."
                key="description"
            />

            <meta property="og:image" content="https://d1vxvcbndiq6tb.cloudfront.net/dca8a378-2f80-4c3b-b4a5-f90db8526d22" />
            <meta property="og:url" content="https://www.creatosaurus.io/" />

            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:site" content="@creatosaurushq" />
            <meta name="twitter:creator" content="@creatosaurushq" />
            <meta name="twitter:title" content="Creatosaurus - All in One Creative & Marketing Platform | Storytelling at Scale" />
            <meta name="twitter:description" content="Creatosaurus helps marketing teams streamline their workflow and run a scalable marketing strategy—from your curation and collaboration to creation and distribution to analytics and apps." />
            <meta name="twitter:image" content="https://d1vxvcbndiq6tb.cloudfront.net/dca8a378-2f80-4c3b-b4a5-f90db8526d22" />
        </Head>
    )
}

export default HeadInfo