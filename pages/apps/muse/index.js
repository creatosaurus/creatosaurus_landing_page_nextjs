import React, { useState, useEffect } from 'react'
import styles from '../../../styles/Muse1.module.css'
import styles1 from '../../../Components/LandingPageComponentCss/NavigationBar.module.css'
import Image from 'next/image';
import Link from 'next/link';
import Logo from '../../../public/Assets/Logo.svg';
import Head from 'next/head';
import RatingComponent from '../../../Components/RatingComponent';
import SupportedBy from '../../../Components/LandingPageComponents/SupportedBy';
import MuseBanner from '../../../public/Assets/Muse/youtube.jpg'
import Play from '../../../public/Assets/Play2.svg';
import museStyle from '../../../styles/muse.module.css';
import Image1 from '../../../public/Assets/Muse/image1.jpg'
import Image2 from '../../../public/Assets/Muse/image2.jpg'
import Image3 from '../../../public/Assets/Muse/image3.jpg'
import Image4 from '../../../public/Assets/Muse/image4.jpg'
import Image5 from '../../../public/Assets/Muse/image5.jpg'
import Image6 from '../../../public/Assets/Muse/image6.jpg'
import Image7 from '../../../public/Assets/Muse/image7.jpg'
import Image8 from '../../../public/Assets/Muse/image8.jpg'
import Image9 from '../../../public/Assets/Muse/image9.jpg'
import Image10 from '../../../public/Assets/Muse/image10.jpg'
import Image11 from '../../../public/Assets/Muse/image11.jpg'
import Menu from '../../../public/Assets/menu.svg';
import Close from '../../../public/Assets/close.svg';
import FifthBlackSection from '../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../Components/LandingPageComponents/Footer';
import WaitListSpotAuth from '../../../Components/WaitListSpotAuth/WaitListSpotAuth';
import WaitlistJoinAuth from '../../../Components/WaitlistJoinAuth/WaitlistJoinAuth';
import jwtDecode from 'jwt-decode';

const Muse = () => {
    const [loginStatus, setLoginStatus] = useState(false)
    const [showLogin, setShowLogin] = useState(false)
    const [showVideo, setShowVideo] = useState(false)
    const [showSecondLogin, setShowSecondLogin] = useState(false)
    const [open, setopen] = useState(false)

    const toggle = () => {
        setopen(prev => !prev)
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length >= 2) return parts.pop().split(";").shift();
    }

    useEffect(() => {
        const value = getCookie('Xh7ERL0G');
        if (value) {
            const decoded = jwtDecode(value);
            const expirationTime = (decoded.exp * 1000) - 60000
            if (Date.now() >= expirationTime) {

            } else {
                setLoginStatus(true)
            }
        }
    }, [])

    let data = [
        {
            title: "Experience design beyond the ordinary with Muse AI photo editor",
            p: "The Future is Here with Muse AI Photo Editor, Transformative and Futuristic.Elevate Your Visuals with Cutting-Edge AI Tools – Effortlessly! Delete Objects Silently, Crop, Rotate, Convert Text to Image, Filters – You Name It! Craft from Scratch with our Free online Muse AI Image Editor. No More hustle with Pro Editing Needed, Let AI Handle the Heavy Lifting While You Create.",
            image: Image1
        }, {
            title: "Transform design with fewer boundaries using Generative AI",
            p: "Have you ever thought about tweaking or changing things in your design, but felt stuck due to complex editing tools? Well, guess what? You can now work magic on your photos with online Muse AI Photo Editor, all thanks to the power of Muse AI. No fancy skills are needed! Just Select the area of the image you want to edit, enter the text and BOOM your design is ready.",
            image: Image2
        },
    ]

    let data2 = [
        {
            title: "Effortlessly Remove Unwanted Elements from the Background with Our Muse AI Photo Editor",
            p: "Are you tired of unwanted elements ruining your images like a stray cat, a random street sign, or a stranger in the wrong place at the wrong time but didn’t know how to save your image? Make your image clean by removing all the unwanted objects, people, or backgrounds with Muse AI photo editor. Give it a try and see how effortlessly you can transform your photos into stunning works of art.",
            image: Image3
        }, {
            title: "Design Beyond Imagination: The Magic of AI-Generated Images",
            p: "AI isn't here to replace human creativity — but it can amplify it and take it even further. If the perfect image you dreamed about doesn’t exist yet then you can create it! Convert your thoughts into the design by typing them out. Visualize the image and start creating it by entering a short text prompt in our AI image generator. Create your own perfect art with Muse AI photo editor.",
            image: Image4
        }, {
            title: "Rediscover Lost Moments with AI Uncrop",
            p: "Uncrop Photos with AI. Muse AI photo editor lets you effortlessly create and expand the subjects or backgrounds in your photos, and regenerate your portraits with a fresh perspective. Expand the frame, revealing hidden details and capturing more memories. With a simple click, watch your images come to life in their full, uncropped glory. Preserve the bigger picture with AI-powered uncropping.",
            image: Image5
        }, {
            title: "Elevate Your Images with AI Image Enhancer",
            p: "Ever wanted to elevate your images but didn’t have editing skills? Enhance your image to the level of perfection with Creatosaurus’s AI image Enhancer. Intensify the colors, brightness, clarity, and denoise, and fix night photos, giving them a professional look that will make them stand out. So, try Enhancer today and watch your images come to life!",
            image: Image6
        }, {
            title: "Chat To Edit Your Design",
            p: "Always wondered about how can you do complex editing to make your design stunning. Struggling with complex design editing tools? Worry no more! With Muse AI photo editor you can now effortlessly edit your designs using a simple text prompt. Just describe the changes you envision, and watch as our Muse AI Photo Editor brings your ideas to life with its enchanting capabilities.",
            image: Image7
        }, {
            title: "Create mesmerizing designs using Creatosaurus’s Design templates",
            p: "Are you tired of staring at a blank canvas, grappling with creative blockage every time you attempt to design? Look no further. Creatosaurus's Design Templates are your key to unlocking boundless inspiration and crafting mesmerizing designs that captivate. Embrace the templates, ignite your creativity, and watch your designs soar to new heights!",
            image: Image8
        }, {
            title: "AI writer to make your design more expressive",
            p: "As a writer, have you ever felt stuck or boxed? Have you ever wished for a helping hand to guide you through writing? Captionator by Creatosaurus is an AI Content Writer tool for you. Generate high-quality content effortlessly, saving you time and energy. Captionator offers a helping hand throughout the writing process, from generating ideas to polishing your final draft. Don't let writer's block hold you back - embrace the power of Captionator AI Content Writer today!",
            image: Image9
        }, {
            title: "Do you find it frustrating to handle tasks across various apps?",
            p: "Managing work across numerous apps and coordinating with multiple team members can often feel like a daunting task. However, with the introduction of Creatosaurus's App Store, the process of cross-collaboration becomes a seamless endeavor. This innovative solution streamlines your workflow and also significantly minimizes the efforts required for effective collaboration – by saving at least 54% of your time. Now you dont need to juggle between multiple platforms. Welcome a more efficient, cohesive way of working.",
            image: Image10
        }, {
            title: "Working with Remote teams is a task? Not anymore",
            p: "Have you always desired prompt feedback from clients or peers on your work? Your wait ends now. Creatosaurus’ real-time collaboration, enables swift sharing of your work files with colleagues or clients through a single click and link. You can now work in real-time with your peers or clients. Embrace the liberty of working remotely while staying seamlessly connected with your stakeholders.",
            image: Image11
        },
    ]

    const apps = [
        {
            title: 'Cache',
            desc: 'Schedule content and share',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.cache.creatosaurus.io/",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: '#Tags',
            desc: 'Organise your hashtag groups in one place',
            longDesc: '',
            url: "https://www.hashtags.creatosaurus.io/",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Noice',
            desc: 'Free asset library that will make your say noice',
            longDesc: '',
            url: "https://www.noice.creatosaurus.io/home",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Quote Post Maker',
            desc: 'Massive library of quotes & dialogue',
            longDesc: 'Search & save quotes, authors and more. Quotes by Creatosaurus.',
            url: "https://www.creatosaurus.io/tools/quote-post-maker",
            privacy: "https://www.creatosaurus.io/privacy"
        },
        {
            title: 'Captionator - AI Content Writer',
            desc: 'AI content generator & writing assistant',
            longDesc: 'Generate content with AI writer. Captionator by Creatosaurus.',
            url: "https://www.creatosaurus.io/apps/captionator/templates/funny-new-year-resolution-idea-generator",
            privacy: "https://www.creatosaurus.io/privacy"
        }
    ];

    return (
        <div className={styles.museNew}>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Muse AI Photo Editor - Free online AI image editor | Creatosaurus</title>
                <meta
                    name="title"
                    content="Muse AI Photo Editor - Free online AI image editor | Creatosaurus"
                />
                <meta
                    name="description"
                    content="Design beyond boundaries with Muse AI photo editor. Quick add, erase, & make AI photo edits by chatting with our AI assistant using an AI image editor by Creatosaurus."
                />
                <meta
                    property="og:title"
                    content="Muse AI Photo Editor - Free online AI image editor | Creatosaurus"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Design beyond boundaries with Muse AI photo editor. Quick add, erase, & make AI photo edits by chatting with our AI assistant using an AI image editor by Creatosaurus."
                    key="description"
                />

                <meta property="og:image" content="https://creatosaurus-muse.s3.ap-south-1.amazonaws.com/23809807-51b4-4f1b-ba58-47fc9451db15" />

                <meta property="og:url" content="https://www.creatosaurus.io/apps/muse" />

                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="Muse AI Photo Editor - Free online AI image editor | Creatosaurus" />
                <meta name="twitter:description" content="Design beyond boundaries with Muse AI photo editor. Quick add, erase, & make AI photo edits by chatting with our AI assistant using an AI image editor by Creatosaurus." />
                <meta name="twitter:image" content="https://creatosaurus-muse.s3.ap-south-1.amazonaws.com/5ec58264-22ed-4734-af6e-f9e22a4170eb" />
            </Head>

            {showLogin ? <WaitListSpotAuth close={() => setShowLogin(false)} /> : null}
            {showSecondLogin ? <WaitlistJoinAuth close={() => setShowSecondLogin(false)} /> : null}

            <nav>
                <div className={`${styles1.navbar} ${styles.newMuseNav}`}>
                    <React.Fragment>
                        {
                            open ?
                                // side nav bar
                                <div className={styles1.sideMenu}>
                                    <div className={styles1.header}>
                                        <Link href="/" passHref>
                                            <span className={styles1.big}> CREATOSAURUS <span className={styles1.small}>| Stories Matter</span> </span>
                                        </Link>


                                        <div className={styles1.menu} onClick={toggle}>
                                            <Image src={Close} alt='' />
                                        </div>
                                    </div>

                                    <div className={styles.rightSide}>
                                        <button onClick={() => window.open("https://twitter.com/intent/tweet?text=Just found @creatosaurushq's Muse AI Photo Editor - Chat, Generate, and Design with a simple text prompt. I just edited my designs by chatting with Muse AI.👉 Join the waitlist: https://creatosaurus.io/apps/muse #GenerativeAI #Photoeditor #AItools #Designtool #Creatosaurus")}>
                                            <span>Share</span>
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M21.1056 6.24544C20.5112 6.50125 19.8863 6.67939 19.2463 6.77544C19.5455 6.72415 19.9857 6.1856 20.161 5.96762C20.4272 5.63881 20.6301 5.26345 20.7594 4.8606C20.7594 4.83068 20.7893 4.78794 20.7594 4.76656C20.7443 4.75833 20.7274 4.75402 20.7102 4.75402C20.693 4.75402 20.6761 4.75833 20.6611 4.76656C19.9662 5.14288 19.2266 5.43009 18.4599 5.62141C18.4331 5.62957 18.4047 5.6303 18.3776 5.62353C18.3505 5.61675 18.3257 5.60272 18.306 5.58294C18.2463 5.51187 18.1821 5.44477 18.1136 5.38205C17.8009 5.10185 17.4461 4.87253 17.0622 4.70245C16.544 4.48984 15.9842 4.39777 15.4252 4.43318C14.8828 4.46743 14.3532 4.61292 13.8694 4.8606C13.393 5.12172 12.9742 5.47646 12.6384 5.9035C12.2851 6.34304 12.0301 6.85314 11.8904 7.39948C11.7752 7.91916 11.7622 8.45627 11.852 8.98093C11.852 9.07069 11.852 9.08351 11.775 9.07069C8.72751 8.6219 6.22711 7.54052 4.18404 5.21963C4.09428 5.11705 4.04727 5.11705 3.97461 5.21963C3.08557 6.57028 3.51727 8.70738 4.62856 9.76311C4.77816 9.90416 4.93203 10.0409 5.09445 10.1692C4.58493 10.133 4.08785 9.99491 3.63267 9.76311C3.54719 9.70755 3.50017 9.73747 3.4959 9.84005C3.48378 9.98226 3.48378 10.1253 3.4959 10.2675C3.58508 10.949 3.85368 11.5947 4.27423 12.1384C4.69478 12.6821 5.25218 13.1044 5.88945 13.362C6.0448 13.4285 6.20667 13.4787 6.37243 13.5116C5.90074 13.6045 5.41696 13.6189 4.94057 13.5543C4.83799 13.533 4.79953 13.5885 4.83799 13.6868C5.4663 15.3965 6.82977 15.918 7.82993 16.2086C7.96671 16.23 8.10348 16.23 8.25735 16.2642C8.25735 16.2642 8.25735 16.2642 8.23171 16.2898C7.93679 16.8284 6.74429 17.1917 6.19719 17.3798C5.1986 17.7384 4.13391 17.8755 3.07703 17.7815C2.91033 17.7559 2.87186 17.7602 2.82912 17.7815C2.78638 17.8029 2.82912 17.8499 2.87614 17.8927C3.08985 18.0337 3.30356 18.1577 3.52582 18.2773C4.18748 18.6382 4.88699 18.9249 5.61162 19.1322C9.36437 20.1665 13.5873 19.4057 16.404 16.6061C18.618 14.4092 19.3959 11.3788 19.3959 8.34408C19.3959 8.22867 19.537 8.16028 19.6182 8.10045C20.1783 7.664 20.6721 7.14858 21.0842 6.57028C21.1556 6.48408 21.1922 6.37432 21.1868 6.26254C21.1868 6.19842 21.1868 6.21125 21.1056 6.24544Z" fill="#000000" />
                                            </svg>
                                        </button>

                                        <button onClick={() => setShowLogin(true)}>
                                            <span>Check Spot</span>
                                            <svg style={{ width: 14, height: 14 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                                                <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#000000" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </button>

                                        <button onClick={() => setShowSecondLogin(true)}>
                                            <span>Join Waitlist</span>
                                            <svg style={{ width: 14, height: 14 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                                                <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#000000" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                :
                                // navigation bar
                                <React.Fragment>
                                    <div className={styles1.row}>
                                        <Link href="/" passHref>
                                            <div className={styles1.logo}>
                                                <Image src={Logo} alt='' />
                                            </div>
                                        </Link>

                                        <Link href="/" passHref>
                                            <span className={styles1.big}> CREATOSAURUS <span className={styles1.small}>| Stories Matter</span> </span>
                                        </Link>
                                    </div>

                                    <div className={styles1.menu} onClick={toggle}>
                                        <Image src={Menu} alt='' />
                                    </div>
                                </React.Fragment>
                        }
                    </React.Fragment>
                </div>

                <div className={styles.navigationBarMuse}>
                    <div className={styles.leftSide}>
                        <Link href="/" passHref>
                            <div className={styles.logo}>
                                <Image src={Logo} alt='' />
                            </div>
                        </Link>

                        <Link href="/" passHref>
                            <span className={styles.big}> CREATOSAURUS <span className={styles.small}>| Stories Matter</span> </span>
                        </Link>
                    </div>


                    <div className={styles.rightSide}>
                        <button onClick={() => window.open("https://twitter.com/intent/tweet?text=Just found @creatosaurushq's Muse AI Photo Editor - Chat, Generate, and Design with a simple text prompt. I just edited my designs by chatting with Muse AI.👉 Join the waitlist: https://creatosaurus.io/apps/muse #GenerativeAI #Photoeditor #AItools #Designtool #Creatosaurus")}>
                            <span>Share</span>
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M21.1056 6.24544C20.5112 6.50125 19.8863 6.67939 19.2463 6.77544C19.5455 6.72415 19.9857 6.1856 20.161 5.96762C20.4272 5.63881 20.6301 5.26345 20.7594 4.8606C20.7594 4.83068 20.7893 4.78794 20.7594 4.76656C20.7443 4.75833 20.7274 4.75402 20.7102 4.75402C20.693 4.75402 20.6761 4.75833 20.6611 4.76656C19.9662 5.14288 19.2266 5.43009 18.4599 5.62141C18.4331 5.62957 18.4047 5.6303 18.3776 5.62353C18.3505 5.61675 18.3257 5.60272 18.306 5.58294C18.2463 5.51187 18.1821 5.44477 18.1136 5.38205C17.8009 5.10185 17.4461 4.87253 17.0622 4.70245C16.544 4.48984 15.9842 4.39777 15.4252 4.43318C14.8828 4.46743 14.3532 4.61292 13.8694 4.8606C13.393 5.12172 12.9742 5.47646 12.6384 5.9035C12.2851 6.34304 12.0301 6.85314 11.8904 7.39948C11.7752 7.91916 11.7622 8.45627 11.852 8.98093C11.852 9.07069 11.852 9.08351 11.775 9.07069C8.72751 8.6219 6.22711 7.54052 4.18404 5.21963C4.09428 5.11705 4.04727 5.11705 3.97461 5.21963C3.08557 6.57028 3.51727 8.70738 4.62856 9.76311C4.77816 9.90416 4.93203 10.0409 5.09445 10.1692C4.58493 10.133 4.08785 9.99491 3.63267 9.76311C3.54719 9.70755 3.50017 9.73747 3.4959 9.84005C3.48378 9.98226 3.48378 10.1253 3.4959 10.2675C3.58508 10.949 3.85368 11.5947 4.27423 12.1384C4.69478 12.6821 5.25218 13.1044 5.88945 13.362C6.0448 13.4285 6.20667 13.4787 6.37243 13.5116C5.90074 13.6045 5.41696 13.6189 4.94057 13.5543C4.83799 13.533 4.79953 13.5885 4.83799 13.6868C5.4663 15.3965 6.82977 15.918 7.82993 16.2086C7.96671 16.23 8.10348 16.23 8.25735 16.2642C8.25735 16.2642 8.25735 16.2642 8.23171 16.2898C7.93679 16.8284 6.74429 17.1917 6.19719 17.3798C5.1986 17.7384 4.13391 17.8755 3.07703 17.7815C2.91033 17.7559 2.87186 17.7602 2.82912 17.7815C2.78638 17.8029 2.82912 17.8499 2.87614 17.8927C3.08985 18.0337 3.30356 18.1577 3.52582 18.2773C4.18748 18.6382 4.88699 18.9249 5.61162 19.1322C9.36437 20.1665 13.5873 19.4057 16.404 16.6061C18.618 14.4092 19.3959 11.3788 19.3959 8.34408C19.3959 8.22867 19.537 8.16028 19.6182 8.10045C20.1783 7.664 20.6721 7.14858 21.0842 6.57028C21.1556 6.48408 21.1922 6.37432 21.1868 6.26254C21.1868 6.19842 21.1868 6.21125 21.1056 6.24544Z" fill="#000000" />
                            </svg>
                        </button>

                        <button onClick={() => setShowLogin(true)}>
                            <span>Check Spot</span>
                            <svg style={{ width: 14, height: 14 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                                <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#000000" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </button>

                        <button onClick={() => setShowSecondLogin(true)}>
                            <span>Join Waitlist</span>
                            <svg style={{ width: 14, height: 14 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                                <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#000000" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </button>
                    </div>
                </div>
            </nav>

            <div className={styles.heroSection}>
                <h1>Chat, generate & design without limit with Muse AI Photo editor</h1>
                <p>Instant Ideas, Infinite Designs with Muse AI — pen, brush, blend, asset, template, effect, filter, layer, tool, language, mask, clip, share & collaborate real time and much more…</p>
                <button onClick={() => setShowSecondLogin(true)}>
                    Join Waitlist
                    <svg style={{ width: 16, height: 16, marginLeft: 5 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="3" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                        <path d="M6 19L19 6m0 0v12.48M19 6H6.52" stroke="#FFFFFF" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </button>
                <div className={styles.imageWrapper}>
                    {
                        showVideo ? <div className={styles.wrapper}>
                            <iframe src="https://www.youtube.com/embed/CzzYRx-Kx9k?autoplay=1" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        </div> : <div className={styles.img} >
                            <Image priority={true} src={MuseBanner} alt="" objectFit='contain' />
                            <div className={styles.youtube} onClick={() => setShowVideo(true)} >
                                <Image src={Play} alt="" objectFit='contain' />
                            </div>
                        </div>
                    }
                </div>
                <SupportedBy />
                <div style={{ marginTop: -50 }}>
                    <RatingComponent />
                </div>
            </div>

            <div className={museStyle.dataBlocks} style={{ marginTop: 80 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage} style={{ width: "40%" }}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => setShowSecondLogin(true)}
                                    >Join Waitlist
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.dataBlocks} style={{ marginTop: 80 }}>
                {data2.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage} style={{ width: "40%" }}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => setShowSecondLogin(true)}
                                    >Join Waitlist
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.how_to_container}>
                <h2>With Muse AI you can go beyond perfection using our Pro Design editing features</h2>
                <div className={museStyle.card_container2}>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M14.363 5.652l1.48-1.48a2 2 0 012.829 0l1.414 1.414a2 2 0 010 2.828l-1.48 1.48m-4.243-4.242l-9.616 9.615a2 2 0 00-.578 1.238l-.242 2.74a1 1 0 001.084 1.085l2.74-.242a2 2 0 001.24-.578l9.615-9.616m-4.243-4.242l4.243 4.242" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>

                        <h4 style={{ marginBottom: 10 }}>Pen</h4>
                        <p>Precisely draw and create custom shapes, providing intricate control over your design elements.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M12 17a1 1 0 100-2 1 1 0 000 2z" fill="#000000" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M21 7.353v9.294a.6.6 0 01-.309.525l-8.4 4.666a.6.6 0 01-.582 0l-8.4-4.666A.6.6 0 013 16.647V7.353a.6.6 0 01.309-.524l8.4-4.667a.6.6 0 01.582 0l8.4 4.667a.6.6 0 01.309.524z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M20.5 16.722l-8.209-4.56a.6.6 0 00-.582 0L3.5 16.722M3.528 7.294l8.18 4.544a.6.6 0 00.583 0l8.209-4.56M12 3v9M12 19.5V22" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>

                        <h4 style={{ marginBottom: 10 }}>Blend Mode</h4>
                        <p>Alter the way layers interact, achieving various visual effects and seamless combinations.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M12 14.5a6 6 0 100-12 6 6 0 000 12z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M16 21.5a6 6 0 100-12 6 6 0 000 12z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M8 21.5a6 6 0 100-12 6 6 0 000 12z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>

                        <h4 style={{ marginBottom: 10 }}>Effects & Filters</h4>
                        <p>Apply a variety of pro enhancements, altering color, texture, and lighting to achieve desired atmospheres.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M21 3.6v16.8a.6.6 0 01-.6.6H3.6a.6.6 0 01-.6-.6V3.6a.6.6 0 01.6-.6h16.8a.6.6 0 01.6.6z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M10 17.659a6 6 0 004-11.317m-4 11.317a6 6 0 114-11.317m-4 11.317L14 6.34" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>Masking</h4>
                        <p>Isolate specific parts of an image, allowing for targeted editing and seamless integration of elements.</p>
                    </div>

                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M7 19.988C6.25 19.988 5.50833 19.8047 4.775 19.438C4.04167 19.0713 3.45 18.588 3 17.988C3.43333 17.988 3.875 17.8172 4.325 17.4755C4.775 17.1338 5 16.638 5 15.988C5 15.1547 5.29167 14.4463 5.875 13.863C6.45833 13.2797 7.16667 12.988 8 12.988C8.83333 12.988 9.54167 13.2797 10.125 13.863C10.7083 14.4463 11 15.1547 11 15.988C11 17.088 10.6083 18.0297 9.825 18.813C9.04167 19.5963 8.1 19.988 7 19.988ZM12.75 13.988L10 11.238L18.95 2.288C19.1333 2.10467 19.3625 2.00883 19.6375 2.0005C19.9125 1.99217 20.15 2.088 20.35 2.288L21.7 3.638C21.9 3.838 22 4.07133 22 4.338C22 4.60467 21.9 4.838 21.7 5.038L12.75 13.988Z" fill="white" stroke="black" strokeWidth="1.5" />
                        </svg>

                        <h4 style={{ marginBottom: 10 }}>Brush</h4>
                        <p>Paint and blend colors, textures, and effects with precision, adding artistic flair to your work.</p>
                    </div>

                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2.38086 10.4043L9.99991 14.2138L17.6351 10.4043" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path>
                            <path d="M2.38086 14.2129L9.99991 18.0224L17.6351 14.2129" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path>
                            <path fillRule="evenodd" clipRule="evenodd" d="M2.38086 6.7442L10.0075 10.4042L17.6351 6.7442L10.0075 2.78516L2.38086 6.7442Z" stroke="#000000" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"></path>
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>Layers</h4>
                        <p>Organize and manipulate elements individually, enabling complex designs by stacking, rearranging, and modifying components.</p>
                    </div>
                </div>
            </div>

            <div className={museStyle.discover_apps_container} style={{ marginTop: 100 }}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>
                        })}
                        <button onClick={() => window.open("https://www.creatosaurus.io/tools/schedule-instagram-stories")}>Instagram Story Scheduler Tool</button>
                    </div>
                </div>
            </div>

            <div style={{ marginTop: 100 }}>
                <FifthBlackSection text="Chat, generate & design without limit with Muse AI Photo editor" button="Join Waitlist" />
            </div>
            <Footer />
        </div>
    )
}

export default Muse