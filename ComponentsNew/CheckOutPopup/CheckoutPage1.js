import React, { useState, useEffect } from 'react'
import PopupLayout from '../PopupLayout/PopupLayout'
import Image from 'next/image'
import Poster from '../../public/NewAssets/Home/poster.webp'
import CloseButton from './CloseButton'
import Link from 'next/link'
import axios from 'axios'
import constant from '../../constant'
import { toast } from 'react-toastify'

const CheckoutPage1 = ({ isYearlyDuration, isIndian, selectedPlan, changeDuration, close, next, handleCode, couponCode, exclusiveDeal }) => {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    const [showCouponCode, setShowCouponCode] = useState(false)
    const [codeText, setCodeText] = useState("")
    const [isCouponCodeValid, setIsCouponCodeValid] = useState(false)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [loadingUpdagrade, setLoadingUpdagrade] = useState(false)

    const codes = [{
        name: "first",
        duration: "1"
    }, {
        name: "first3",
        duration: "3"
    }, {
        name: "first6",
        duration: "6"
    }, {
        name: "wework6",
        duration: "6"
    }, {
        name: "yc6",
        duration: "6"
    }, {
        name: "build6",
        duration: "6"
    }, {
        name: "damini3",
        duration: "3"
    }, {
        name: "hoc6",
        duration: "6"
    }, {
        name: "legaverse6",
        duration: "6"
    }, {
        name: "iitmandi6",
        duration: "6"
    }]

    const getCookie = (name) => {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    useEffect(() => {
        const token = getCookie("Xh7ERL0G")
        if (token) {
            localStorage.setItem("token", token)
        }

        if (couponCode) {
            setCodeText(couponCode.code)
            setShowCouponCode(true)
            setIsCouponCodeValid(true)
        }
    }, [])

    const checkCurrencyType = () => {
        return isIndian ? "₹" : "$"
    }

    const check12MonthPrice = () => {
        const number = Number(selectedPlan.month.replace(/,/g, ''));
        return formatPrice(number * 12)
    }

    const checkEndDate = () => {
        const currentDate = new Date();
        if (isYearlyDuration) {
            const nextYearDate = new Date(currentDate.setFullYear(currentDate.getFullYear() + 1));
            return `${nextYearDate.getDate()} ${months[nextYearDate.getMonth()]} ${nextYearDate.getFullYear()}`;
        } else {
            const nextMonthDate = new Date(currentDate.setMonth(currentDate.getMonth() + 1));
            return `${nextMonthDate.getDate()} ${months[nextMonthDate.getMonth()]} ${nextMonthDate.getFullYear()}`;
        }
    }

    const checkCouponCode = async () => {
        try {
            setLoading(true)
            setError(false)
            const token = localStorage.getItem("token")
            const config = {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            }

            const res = await axios.get(`${constant.url}coupon/code/find?code=${codeText}`, config)

            handleCode(res.data)
            setIsCouponCodeValid(true)
            setLoading(false)
        } catch (error) {
            setCodeText("")
            setIsCouponCodeValid(false)
            setError(true)
            setLoading(false)
        }
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            checkCouponCode()
        }
    }

    const handleDiscount = () => {
        let price = isYearlyDuration ? selectedPlan.year : selectedPlan.month

        if (couponCode) {
            let discountAmount = (price * couponCode.discount) / 100;
            let finalPrice = price - discountAmount;
            return formatPrice(finalPrice)
        }

        return formatPrice(price)
    }

    const formatPrice = (number) => {
        number = parseInt(number)
        return number.toLocaleString('en-IN');
    }

    const removeCouponCode = () => {
        handleCode(null)
        setIsCouponCodeValid(false)
        setCodeText("")
        setError(false)
    }

    const handleCodeChange = (e) => {
        const value = e.target.value.trim()
        if (value === "") return removeCouponCode()
        setCodeText(value)
    }

    const checkFreeUpgrade = () => {
        if (!couponCode) return false

        const isFreeCouponCode = codes.find((data) => data.name === couponCode.code)
        if (!isFreeCouponCode) return false
        return true
    }

    const upgradeForFree = async () => {
        try {
            if (loadingUpdagrade) return

            const token = localStorage.getItem("token")
            const config = {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            }

            const freeCodeDetail = codes.find(data => data.name === couponCode.code)
            if (!freeCodeDetail) return

            setLoadingUpdagrade(true)
            await axios.post(constant.url + "v2/userfeaturefactory/upgrade", {
                duration: freeCodeDetail.duration
            }, config)

            setLoadingUpdagrade(false)
            window.open("https://www.app.creatosaurus.io/", "_self")
        } catch (error) {
            setLoadingUpdagrade(false)
            console.log(error)
            toast.error("Oops! Something went wrong. Please contact us to complete your upgrade.")
        }
    }

    return (
        <PopupLayout close={close}>
            <div className='relative'>
                <CloseButton close={close} />
                <div className='flex bg-white w-[80vw] max-w-[814px] rounded-[10px] relative overflow-hidden' onClick={(e) => e.stopPropagation()}>
                    <div className='hidden lg:flex w-[407px] min-w-[407px] h-[560px] object-contain'>
                        <Image src={Poster} alt='' />
                    </div>

                    <div className='pt-[30px] pb-[25px] px-[30px] w-full'>
                        <span className='text-[22px] font-semibold'>Upgrade {selectedPlan.planName}</span>
                        <div className='flex items-center mt-[5px]'>
                            <svg className='mr-[5px]' width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.75 10.25L6.75 13.25L14.25 5.75" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>

                            <span className='text-[14px] text-[#333333]'>No auto renewal. Upgrade anytime.</span>
                        </div>

                        <div className='mt-[20px]'>
                            {
                                exclusiveDeal ? <>
                                    <div className='flex items-center'>
                                        <div
                                            onClick={() => changeDuration("yearly")}
                                            className={`size-[18px] ${isYearlyDuration ? "bg-[#333333]" : "border-[1px] border-[#C4C4C4]"} rounded-full flex justify-center items-center cursor-pointer`}>
                                            <div className={`size-[6px] ${isYearlyDuration ? "bg-white" : "bg-[#C4C4C4]"} rounded-full`} />
                                        </div>

                                        <div className='ml-[7px]'>
                                            <div className='flex items-center'>
                                                <span className='text-[14px] font-medium text-[#333333] mr-[10px]'>Yearly Deal</span>
                                                <button className='w-[67px] h-[17px] rounded-[15px] bg-[#FF4359] text-[10px] font-medium text-white'>Best Value</button>
                                            </div>
                                            <span className='text-[12px] text-[#707070]'><span className='line-through'>{checkCurrencyType()}{check12MonthPrice()}</span> {checkCurrencyType()}{formatPrice(selectedPlan.year)}</span>
                                        </div>
                                    </div>
                                </> : <>
                                    <div className='flex items-center'>
                                        <div
                                            onClick={() => changeDuration("yearly")}
                                            className={`size-[18px] ${isYearlyDuration ? "bg-[#333333]" : "border-[1px] border-[#C4C4C4]"} rounded-full flex justify-center items-center cursor-pointer`}>
                                            <div className={`size-[6px] ${isYearlyDuration ? "bg-white" : "bg-[#C4C4C4]"} rounded-full`} />
                                        </div>

                                        <div className='ml-[7px]'>
                                            <div className='flex items-center'>
                                                <span className='text-[14px] font-medium text-[#333333] mr-[10px]'>12 Months</span>
                                                <button className='px-[10px] h-[17px] rounded-[15px] bg-[#FF4359] text-[10px] font-medium text-white'>Best value</button>
                                            </div>
                                            <span className='text-[12px] text-[#707070]'><span className='line-through'>{checkCurrencyType()} {check12MonthPrice()}</span> {checkCurrencyType()} {formatPrice(selectedPlan.year)}</span>
                                        </div>
                                    </div>

                                    <div className='flex items-center mt-[10px]'>
                                        <div
                                            onClick={() => changeDuration("monthly")}
                                            className={`size-[18px] ${isYearlyDuration ? "border-[1px] border-[#C4C4C4]" : "bg-[#333333]"} rounded-full flex justify-center items-center cursor-pointer`}>
                                            <div className={`size-[6px] ${isYearlyDuration ? "bg-[#C4C4C4]" : "bg-white"} rounded-full`} />
                                        </div>

                                        <div className='ml-[7px] flex flex-col'>
                                            <span className='text-[14px] font-medium text-[#333333] mr-[10px]'>1 Month</span>
                                            <span className='text-[12px] text-[#707070]'>{checkCurrencyType()}{formatPrice(selectedPlan.month)}</span>
                                        </div>
                                    </div>
                                </>
                            }


                            <div className='flex items-center mt-[15px]' onClick={() => setShowCouponCode(prev => !prev)}>
                                <div className={`size-[16px] rounded-[2px] ${showCouponCode ? "bg-[#000000]" : "bg-[#DCDCDC]"} flex justify-center items-center cursor-pointer`}>
                                    <svg width="10" height="8" viewBox="0 0 10 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.800781 4.6L3.20078 7L9.20078 1" stroke="white" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                                <span className='ml-[10px] mr-[5px] text-[12px] text-[#808080]'>I have coupon code</span>
                            </div>

                            {
                                showCouponCode &&
                                <div className='mt-[15px]'>
                                    <span className='text-[14px] font-medium text-[#333333]'>Enter your coupon code</span>
                                    <div className='relative flex items-center justify-center'>
                                        <input
                                            type='text'
                                            value={codeText}
                                            onChange={handleCodeChange}
                                            onKeyDown={handleKeyDown}
                                            placeholder='Enter code'
                                            className='bg-[#FAFAFA] placeholder:text-[#C4C4C4] border-[1px] border-[#DCDCDC] h-[50px] px-[10px] text-[16px] rounded-[10px] w-full' />

                                        {
                                            loading && <span className='loader size-[14px] absolute right-[12px] cursor-pointer group' />
                                        }
                                        {
                                            (!loading && !isCouponCodeValid) &&
                                            <svg onClick={checkCouponCode} className='size-[14px] absolute right-[12px] cursor-pointer group' width="16px" height="16px" viewBox="0 0 24 24" strokeWidth="2.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#333">
                                                <path className='group-hover:stroke-black' d="M3 12L21 12M21 12L12.5 3.5M21 12L12.5 20.5" stroke="#333" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        }

                                        {
                                            (!loading && isCouponCodeValid) &&
                                            <svg onClick={removeCouponCode} className='size-[18px] absolute right-[12px] cursor-pointer group' height="16px" stroke-width="2.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                                                <path d="M6.75827 17.2426L12.0009 12M17.2435 6.75736L12.0009 12M12.0009 12L6.75827 6.75736M12.0009 12L17.2435 17.2426" stroke="#000000" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        }
                                    </div>

                                    {(error && codeText.length === 0) && <span className='text-[14px] text-[#c02d2d]'>Invalid coupon code</span>}

                                    {
                                        isCouponCodeValid && <span className='text-[14px] text-[#333333]'>
                                            {checkFreeUpgrade() ? "Upgrade for free 🎉" : `Yay! ${couponCode.discount}% off`}
                                        </span>
                                    }

                                </div>
                            }

                            {
                                checkFreeUpgrade() === false &&
                                <div className='mt-[20px]'>
                                    <div className='text-[12px] text-[#707070] flex justify-between items-center'>
                                        <span>Ends on</span>
                                        <span>{checkEndDate()}</span>
                                    </div>

                                    <div className='text-[14px] font-medium text-[#333333] flex justify-between items-center mt-[10px]'>
                                        <span>Due Today</span>
                                        <div>
                                            <span className='line-through mr-[8px]'>{isYearlyDuration && `${checkCurrencyType()}${check12MonthPrice()}`}</span>
                                            <span>{checkCurrencyType()} {handleDiscount()}</span>
                                        </div>
                                    </div>
                                </div>
                            }

                            {
                                checkFreeUpgrade() ?
                                    <button onClick={upgradeForFree} className='h-[50px] bg-black text-white w-full rounded-[10px] font-semibold mt-[20px] relative flex justify-center items-center'>
                                        {
                                            loadingUpdagrade ?
                                                <span className='loader' />
                                                :
                                                "Upgrade for Free"
                                        }
                                    </button>
                                    :
                                    <button onClick={next} className='h-[50px] bg-black text-white w-full rounded-[10px] font-semibold mt-[20px]'>Next</button>
                            }

                            <p className='text-[12px] mt-[10px] text-[#808080]'>By continuing, you agree to the Creatosaurus’s&nbsp;
                                <span className='underline'><Link href="/terms">Terms of Service.</Link></span>
                                &nbsp;Read our <span className='underline'><Link href="privacy">Privacy Policy.</Link></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </PopupLayout>
    )
}

export default CheckoutPage1