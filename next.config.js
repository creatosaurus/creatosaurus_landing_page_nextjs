module.exports = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains: ['ahijftavqd.creatosaurus.io', 'images.unsplash.com', 'static.ghost.org', 'd3u00cz8rshep5.cloudfront.net', 's3-ap-south-1.amazonaws.com', "creatosaurus-muse.s3.ap-south-1.amazonaws.com", "d1vxvcbndiq6tb.cloudfront.net", "www.gravatar.com", "www.creatosaurus.io"],
    minimumCacheTTL: 3600, // Cache images for 1 hour (3600 seconds)
  },
  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
  },
};
