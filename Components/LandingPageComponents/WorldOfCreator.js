import React from 'react';
import stylesWorld from '../LandingPageComponentCss/WorldOfCreator.module.css';
import Image from 'next/image';

const WorldOfCreator = () => {
  return (
    <div className={stylesWorld.worldOfCreator}>
      <div className={stylesWorld.head}>
        <h1>Deep dive in the world of creators</h1>
        <button>Visit blog</button>
      </div>
      <div className={stylesWorld.blogs}>
        <div className={stylesWorld.part1}>
          <div className={stylesWorld.imageContainer}>
            <div className={stylesWorld.box} />
            <Image
              src='https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
              alt=''
            />
          </div>
          <h3>
            Adopt expert guidance use timely resources and get to know all you
          </h3>
          <p>
            Adopt expert guidance use timely resources and get to know all you
            Adopt expert guidance use timely resources and get to know all you
          </p>
          <span>July 5, 2021</span>
        </div>
        <div className={stylesWorld.part2}>
          <div className={stylesWorld.card}>
            <div className={stylesWorld.info}>
              <p>
                Adopt expert guidance use timely resources and get to know all
                you
              </p>
              <span>June 5, 2021</span>
            </div>
            <div className={stylesWorld.imageContainer2}>
              <div className={stylesWorld.box2} />
              <Image
                src='https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
                alt=''
              />
            </div>
          </div>

          <div className={stylesWorld.card}>
            <div className={stylesWorld.info}>
              <p>
                Adopt expert guidance use timely resources and get to know all
                you
              </p>
              <span>June 5, 2021</span>
            </div>
            <div className={stylesWorld.imageContainer2}>
              <div className={stylesWorld.box2} />
              <Image
                src='https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
                alt=''
              />
            </div>
          </div>

          <div className={stylesWorld.card}>
            <div className={stylesWorld.info}>
              <p>
                Adopt expert guidance use timely resources and get to know all
                you
              </p>
              <span>June 5, 2021</span>
            </div>
            <div className={stylesWorld.imageContainer2}>
              <div className={stylesWorld.box2} />
              <Image
                src='https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
                alt=''
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WorldOfCreator;
