import React, { useState } from 'react';
import style from '../../../styles/Quotes.module.css';
import Navbar from '../../../Components/LandingPageComponents/NavigationBar';
import Image from 'next/image';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Link from 'next/link';
import axios from 'axios';
import Masonry from 'react-responsive-masonry';
import Save from '../../../public/Assets/Save.svg';
import Copy from '../../../public/Assets/Copy.svg';
import { toast } from 'react-toastify';
import QuoteSearch from '../../../Components/QuotesPageComponent/QuoteSearch';
import AppInfo from '../../../Components/QuotesPageComponent/AppInfo';
import QuoteOfTheDay from '../../../Components/QuotesPageComponent/QuoteOfTheDay';
import QuotesCategory from '../../../Components/QuotesPageComponent/QuotesCategory';
import Top100Quotes from '../../../Components/QuotesPageComponent/Top100Quotes';

export const getServerSideProps = async () => {
  const response = await axios.get(
    'https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/quotesoftheday'
  );
  const quotesdata = response.data;

  if (!quotesdata) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      quotesdata,
    },
  };
};

const Quotes = (quotesdata) => {
  const [search, setSearch] = useState('');
  const [quotes, setQuotes] = useState([]);



  const onFormSubmit = async (e) => {
    e.preventDefault();
    const res = await axios.get(
      `https://ug06ceeng4.execute-api.ap-south-1.amazonaws.com/latest/quotes/search?q=${search}`
    );

    setQuotes(res.data);
  };

  const handleChange = (event) => {
    setSearch(event.target.value);
  };

  const quotesType = [
    {
      heading: "Explore quotes by famous authors",
      type: [
        'A. P. J. Abdul Kalam',
        'Abraham Lincoln',
        'Albert Einstein',
        'Swami Vivekananda',
        'Elon Musk',
        'Winston Churchill',
        'Helen Keller',
        'Charles R. Swindoll',
        'David Amerland',
        'Nelson Mandela',
        'Aristotle',
        'Barack Obama',
        'Bill Gates',
        'Bruce Lee',
        'Buddha Confucius',
        'Dalai Lama',
        'Epictetus',
        'Ernest Hemingway',
        'Franklin D. Roosevelt',
        'Friedrich Nietzsche',
        'Henry Ford',
        'Isaac Newton',
        'Leonardo da Vinci',
        'Mahatma Gandhi',
        'Mark Twain',
        'Martin Luther King, Jr.',
        'Maya Angelou',
        'Mike Tyson',
        'Mother Teresa',
        'Muhammad Ali',
        'Oprah Winfrey',
        'Paulo Coelho',
        'Plato',
        'Rabindranath Tagore',
        'Robert Frost',
        'Socrates',
        'Steve Jobs',
        'Swami Sivananda',
        'Theodore Roosevelt',
        'Thomas A. Edison',
        'Walt Disney',
        'Warren Buffett',
        'William Shakespeare',
        'Zig Ziglar',
      ]
    },
    {
      heading: "Explore quotes by popular topics",
      type: [
        "Love",
        "Life",
        "Inspirational",
        "Humor",
        "Philosophy",
        "God",
        "Truth",
        "Wisdom",
        "Poetry",
        "Romance",
        "Death",
        "Happiness",
        "Hope",
        "Faith",
        "Writing",
        "Motivational",
        "Religion",
        "Relationships",
        "Spirituality",
        "Success",
        "Time",
        "Knowledge",
        "Science",
        "Art",
        "Nature",
        "Dreams",
        "Adventure",
        "Change",
        "Courage",
        "Compassion",
        "Gratitude",
        "Healing",
        "Kindness",
        "Laughter",
        "Mindfulness",
        "Patience",
        "Resilience",
        "Serenity",
        "Simplicity",
        "Transformation",
        "Understanding",
        "Unity",
        "Joy",
        "Balance",
        "Discovery",
        "Empathy",
        "Growth",
        "Harmony",
        "Peace",
        "Purpose"
      ]
    }
  ]



  const slugify = (string) => {
    return string
      .toString()
      .trim()
      .toLowerCase()
      .replace(/\s+/g, '-')
      .replace(/[^\w\-]+/g, '')
      .replace(/\-\-+/g, '-')
      .replace(/^-+/, '')
      .replace(/-+$/, '');
  };

  const copyText = (quote, author) => {
    toast('Copied!', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    navigator.clipboard.writeText(quote + ' :-' + author);
  };

  const searchResults = () => {
    return (
      <div className={style.searchContainerCenter}>
        <div style={{ overflow: 'scroll' }}>
          <Masonry columnsCount={3} gutter='20'>
            {quotes.map((data, index) => {
              const fiveWords = data.Queote.split(' ').slice(0, 5).join(' ');
              const slug = slugify(fiveWords);

              let trimedAuthor = '';
              if (data.Author === undefined) {
                trimedAuthor = 'Unknown author';
              } else {
                if (data.Author.includes(',')) {
                  trimedAuthor = data.Author.substring(
                    0,
                    data.Author.indexOf(',')
                  );
                } else {
                  trimedAuthor = data.Author;
                }
              }

              const slugAuthor = slugify(trimedAuthor);
              const author = slugAuthor.substring(0, 30);
              return (
                <div key={data._id}>
                  <Link
                    href={{
                      pathname: '/apps/quotes/[slug]',
                      query: data,
                    }}
                    as={`/apps/quotes/${slug}-quote-by-${author}-${data._id}`}
                    passHref
                  >
                    <div className={style.card} style={{ cursor: 'pointer' }}>
                      <p>{data.Queote}</p>
                      <div className={style.authorContainer}>
                        <span>- {data.Author}</span>
                      </div>
                    </div>
                  </Link>
                  <div className={style.functions}>
                    <div className={style.img1}>
                      <Image
                        src={Save}
                        alt=''
                        onClick={() => {
                          toast('Subscribe to save quote.');
                        }}
                      />
                    </div>
                    <div className={style.img2}>
                      <Image
                        src={Copy}
                        alt=''
                        onClick={() => copyText(data.Queote, data.Author)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
          </Masonry>
        </div>
      </div>
    );
  };

  return (
    <div>
      <Navbar />
      <div className={style.mainQuoteContainer}>
        <QuoteSearch onFormSubmit={onFormSubmit} handleChange={handleChange} search={search} />
        {search !== "" ? null : <React.Fragment>
          <AppInfo />
          <QuoteOfTheDay quotesdata={quotesdata} />
          <QuotesCategory quotesType={quotesType} slugify={slugify} />
          <Top100Quotes />
        </React.Fragment>}
      </div>

      {search !== '' ? searchResults() : null}

      <div style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }}>
        <div className={style.signUpBoxContainer}>
          <p>You focus on telling stories,
            we do everything else.</p>
          <button>Signup for Free</button>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Quotes;