import jwtDecode from "jwt-decode";

const getCookie = (name) => {
    let value = "; " + document.cookie;
    let parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}

const checkLoginStatus = () => {
    const token = getCookie('Xh7ERL0G');
    if (!token) return false;

    const { exp } = jwtDecode(token);
    const expirationTime = (exp * 1000) - 60000; // Expiration time minus 1 minute

    return Date.now() < expirationTime;
}

export {
    checkLoginStatus
}