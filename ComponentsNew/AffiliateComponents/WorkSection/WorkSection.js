import React from 'react'
import styles from './WorkSection.module.css'
import Image from 'next/image'
import stepOne from './assets/stepOne.svg'
import stepTwo from './assets/stepTwo.svg'
import stepThree from './assets/stepThree.svg'
import stepFour from './assets/stepFour.svg'

const WorkSection = () => {
    const steps = [
        {
            step: "Step 1",
            name: "Become a Partner",
            desc: "Join our affiliate program for free, you'll receive your affiliate link and be able to start earning.",
            logo: stepOne
        },
        {
            step: "Step 2",
            name: "Start Promoting",
            desc: "Access our ready made promotional content to share on your website, blog, and social media.",
            logo: stepTwo
        },
        {
            step: "Step 3",
            name: "Get Rewarded",
            desc: "Once your referrals pay for their accounts, earn a 30% recurring commission for one year.",
            logo: stepThree
        },
        {
            step: "Step 4",
            name: "Continue Earnings",
            desc: "Stack up your total monthly cash by inviting more referrals and make more money.",
            logo: stepFour
        }
    ]
    return (
        <div className={styles.workSectionContainer}>
            <div className={styles.headerContainer}>
                <h2>How Does It Work?</h2>
                <p>Joining the Creatosaurus Affiliate Program is Quick and Easy. Create your affiliate account and you can start earning straight away. We&apos;ll give you special banners and links. You can put them on your website and share them on your social media. When someone clicks on these links, our affiliate tool keeps track of it, and you get a reward based on their subscription.</p>
            </div>

            <div className={styles.stepsContainer}>
                {steps.map((data, index) => {
                    return (
                        <div className={styles.step} key={index}>
                            <Image priority={true} src={data.logo} alt="step" />
                            <h2>{data.step}</h2>
                            <span>{data.name}</span>
                            <p>{data.desc}</p>
                        </div>
                    )
                })}
            </div>
        </div>
    );
}

export default WorkSection;