import React, { useState } from 'react'
import styles from './ToolsInfo.module.css'
import Image from 'next/image'
import Image11 from '../../../public/NewAssets/Home/Tools/image11.png'
import Image12 from '../../../public/NewAssets/Home/Tools/image12.png'
import Image13 from '../../../public/NewAssets/Home/Tools/image13.png'

import Image21 from '../../../public/NewAssets/Home/Tools/image21.png'
import Image22 from '../../../public/NewAssets/Home/Tools/image22.png'
import Image23 from '../../../public/NewAssets/Home/Tools/image23.png'

import Image31 from '../../../public/NewAssets/Home/Tools/image31.png'
import Image32 from '../../../public/NewAssets/Home/Tools/image32.png'
import Image33 from '../../../public/NewAssets/Home/Tools/image33.png'

import Image41 from '../../../public/NewAssets/Home/Tools/image41.png'
import Image42 from '../../../public/NewAssets/Home/Tools/image42.png'
import Image43 from '../../../public/NewAssets/Home/Tools/image43.png'

import Ishwar from '../../../public/Assets/ishwar.webp'
import Anshul from '../../../public/Assets/Anshul.jpg'
import Sumeet from '../../../public/Assets/summet.webp'
import Abhishek from '../../../public/Assets/abhishek.webp'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'

const ToolsInfo = () => {
    const [showLogin, setShowLogin] = useState(false)

    const content = [{
        color: 'rgba(0, 120, 255, 0.10)',
        title: "Social Media Scheduler & Management",
        subHead: "Easily schedule posts, reels, stories and videos across various platforms. Enhance engagement with your audience when it matters most for better results.",
        features: ["Content Calendar", "Preview Post", "10+ Platform Support", "Queue Pipeline", "Draft & Reminders", "Customize Each"],
        userFeedback: "Creatosaurus is a cockpit for marketers with all controls in a single place",
        userImage: Ishwar,
        userName: "Ishwar Sarade",
        userPoistion: "Director, Bitsmith",
        images: [Image11, Image12, Image13],
        appGif: "https://d3u00cz8rshep5.cloudfront.net/cache.gif"
    }, {
        color: 'rgba(255, 138, 37, 0.10)',
        title: "Professional Graphic Design Editor",
        subHead: "A diverse library of design library at your fingertips. Enhance your workflow with unique, professional graphics and streamline design collaboration with ease.",
        features: ["Pro Editing Tool", "AI Photo Editor", "2500+ Modern Templates", "Multi Language", "Premium Stock Library", "Filters & Effects"],
        userFeedback: "Creatosaurus combines creator workflow with design unlocking productivity for creators.",
        userImage: Anshul,
        userName: "Anshul Motwani",
        userPoistion: "Founder, Wittypens",
        images: [Image21, Image22, Image23],
        appGif: "https://d3u00cz8rshep5.cloudfront.net/muse.gif"
    }, {
        color: 'rgba(138, 67, 255, 0.10)',
        title: "Comprehensive Social Inbox & Analytics",
        subHead: "Get insights from multiple social platforms in a single view. Spend less time sifting through clutter and more time engaging with meaningful interactions.",
        features: ["Shared Inbox", "Updates & Reminders", "Instant Updates", "Hashtag Insights", "Detailed Reports", "Competition Analysis "],
        userFeedback: "Easy to collaborate & helps to scale our social media marketing",
        userImage: Sumeet,
        userName: "Sumeet Shinde",
        userPoistion: "Founder, Rise n Shine",
        images: [Image31, Image32, Image33],
        appGif: "https://d3u00cz8rshep5.cloudfront.net/cache.gif"
    }, {
        color: 'rgba(0, 236, 194, 0.10)',
        title: "AI Content Writer and Image Generation",
        subHead: "Never worry about limited image resources and generate striking visuals for your designs. Efficiently craft content in various formats, while optimizing your blog.",
        features: ["AI Image Creator", "AI Content Writer", "Mixed Photo Editing", "100+ Copy Templates", "Multiple Styles", "Long Form Blog Editor"],
        userFeedback: "Creatosaurus allows creators & marketers to spend more time telling stories by optimising their complete process.",
        userImage: Abhishek,
        userName: "Abhishek Agrawal",
        userPoistion: "Co-Founder, Dyrect",
        images: [Image41, Image42, Image43],
        appGif: "https://d3u00cz8rshep5.cloudfront.net/captionator.gif"
    }]

    return (
        <div className={styles.toolsInfoContainer}>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <h2>Finally, all your social media marketing tool in one place!</h2>
            <div>
                {
                    content.map(data => {
                        return <>
                            <div className={styles.card}>
                                <div className={styles.info}>
                                    <h3>{data.title}</h3>
                                    <p>{data.subHead}</p>
                                    <div className={styles.grid}>
                                        {
                                            data.features.map(data => {
                                                return <div key={data} className={styles.row}>
                                                    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M4.16602 10.8335L7.49935 14.1668L15.8327 5.8335" stroke="#333333" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                    </svg>
                                                    <span>{data}</span>
                                                </div>
                                            })
                                        }
                                    </div>
                                    <button onClick={()=> setShowLogin(true)} style={{ cursor: 'pointer' }}>Get Started—It&apos;s Free</button>
                                    <p className={styles.subInfo}>&quot;{data.userFeedback}&quot;</p>
                                    <div className={styles.profile}>
                                        <div className={styles.userImage}>
                                            <Image src={data.userImage} alt={data.userName} width="50" height="50" objectFit='cover' />
                                        </div>
                                        <div className={styles.userDetails}>
                                            <span>{data.userName}</span>
                                            <span>{data.userPoistion}</span>
                                        </div>
                                    </div>
                                </div>

                                <div className={styles.box} style={{ borderColor: data.color }}>
                                    <Image unoptimized={true} src={data.appGif} alt={data.title} layout='fill' objectFit='cover' />
                                </div>
                            </div>

                            <div className={styles.imageContainer}>
                                {
                                    data.images.map((image, index) => {
                                        return <div className={styles.imageWrapper} key={index} style={{ backgroundColor: data.color }}>
                                            <Image src={image} alt="image info" />
                                        </div>
                                    })
                                }
                            </div>
                        </>
                    })
                }
            </div>
        </div>
    )
}

export default ToolsInfo