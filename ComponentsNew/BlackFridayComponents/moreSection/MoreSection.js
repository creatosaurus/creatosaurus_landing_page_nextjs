import React from 'react'
import styles from './MoreSection.module.css'
import Image51 from '../../../public/NewAssets/Home/Tools/image51.png'
import Image52 from '../../../public/NewAssets/Home/Tools/image52.png'
import Image53 from '../../../public/NewAssets/Home/Tools/image53.png'
import Image54 from '../../../public/NewAssets/Home/Tools/image54.png'
import Image55 from '../../../public/NewAssets/Home/Tools/image55.png'
import Image56 from '../../../public/NewAssets/Home/Tools/image56.png'
import Image from 'next/image'

const MoreSection = () => {
    const imageData = [{
        color: 'rgba(255, 67, 89, 0.10)',
        image: Image51
    }, {
        color: 'rgba(255, 67, 89, 0.10)',
        image: Image52
    }, {
        color: 'rgba(255, 215, 95, 0.10)',
        image: Image53
    }, {
        color: 'rgba(255, 67, 202, 0.10)',
        image: Image54
    }, {
        color: 'rgba(255, 67, 202, 0.10)',
        image: Image55
    }, {
        color: 'rgba(138, 67, 255, 0.10)',
        image: Image56
    },]

    return (
        <div className={styles.moreSectionContainer}>
            <h2>& lot more at one place...</h2>
            <div className={styles.imageGrid}>
                {
                    imageData.map((data, index) => {
                        return <div className={styles.imageWrapper} key={index} style={{ backgroundColor: data.color }}>
                            <Image src={data.image} alt="image info" />
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default MoreSection