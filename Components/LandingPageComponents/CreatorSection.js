import React, { useState } from 'react';
import stylesCreator from '../LandingPageComponentCss/CreatorSection.module.css';
import Image from 'next/image';

const CreatorSection = () => {
  const [value, setValues] = useState({
    creators: true,
    community: false,
    startups: false,
    marketers: false,
    influencers: false,
    social: false,
  });

  const { creators, community, startups, marketers, influencers, social } =
    value;
  return (
    <section className={stylesCreator.creatorSection}>
      <div className={stylesCreator.brands}>
        <h5>Trusted by the Best</h5>
        <div>
          <span>Creators</span>
          <span>Creators</span>
          <span>Creators</span>
          <span>Creators</span>
        </div>
      </div>

      {creators ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.unsplash.com/photo-1576919918680-b521456c2c0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fGNyZWF0b3J8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>🕺🏻</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      {/* Community Builders */}
      {community ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.pexels.com/photos/919606/pexels-photo-919606.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=500'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>🎸</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      {/* Startups */}
      {startups ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.pexels.com/photos/3618162/pexels-photo-3618162.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>🚀</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      {/* Marketers */}
      {marketers ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.pexels.com/photos/697244/pexels-photo-697244.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>🎯</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      {/* Influencers */}
      {influencers ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.pexels.com/photos/4256852/pexels-photo-4256852.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>💃🏻</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      {/* social */}
      {social ? (
        <div className={stylesCreator.infoContainer}>
          <Image
            src='https://images.pexels.com/photos/1478685/pexels-photo-1478685.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'
            alt=''
          />
          <div className={stylesCreator.info}>
            <span>💈</span>
            <p>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry&apos;s standard dummy
              text ever since the 1500s, when an unknown printer took a galley
              of type.
            </p>
            <span className={stylesCreator.name}>Raj Sharma</span>
            <span className={stylesCreator.designation}>
              Founder, Abc Music
            </span>
          </div>
        </div>
      ) : (
        ''
      )}

      <div className={stylesCreator.buttonContainer}>
        <button
          className={creators ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: true,
              community: false,
              startups: false,
              marketers: false,
              influencers: false,
              social: false,
            })
          }
        >
          🕺🏻 &nbsp;&nbsp;&nbsp;Creators
        </button>
        <button
          className={community ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: false,
              community: true,
              startups: false,
              marketers: false,
              influencers: false,
              social: false,
            })
          }
        >
          🎸 &nbsp;&nbsp;&nbsp;Community Builders
        </button>
        <button
          className={startups ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: false,
              community: false,
              startups: true,
              marketers: false,
              influencers: false,
              social: false,
            })
          }
        >
          🚀 &nbsp;&nbsp;&nbsp;Startups
        </button>
        <button
          className={marketers ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: false,
              community: false,
              startups: false,
              marketers: true,
              influencers: false,
              social: false,
            })
          }
        >
          🎯 &nbsp;&nbsp;&nbsp;Marketers
        </button>
        <button
          className={influencers ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: false,
              community: false,
              startups: false,
              marketers: false,
              influencers: true,
              social: false,
            })
          }
        >
          💃🏻 &nbsp;&nbsp;&nbsp;Influencers
        </button>
        <button
          className={social ? stylesCreator.button : ''}
          onClick={() =>
            setValues({
              ...value,
              creators: false,
              community: false,
              startups: false,
              marketers: false,
              influencers: false,
              social: true,
            })
          }
        >
          💈 &nbsp;&nbsp;&nbsp;Social Media Mangers
        </button>
      </div>
    </section>
  );
};

export default CreatorSection;
