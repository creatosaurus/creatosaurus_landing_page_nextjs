import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import { useGoogleOneTapLogin } from '@react-oauth/google';
import constant from '../../constant';
import { toast } from 'react-toastify'

const NavigationAuthGooglePopup = ({ appSumoCode }) => {
    const router = useRouter()
    const { query, pathname, push } = router

    const [invitationCode, setinvitationCode] = useState(null)
    const [loading, setLoading] = useState(false);
    const [redirectTo, setredirectTo] = useState(null);
    const [affiliate, setAffiliate] = useState(false)


    const setRefCookie = (name, value) => {
        if (process.env.NODE_ENV === 'development') {
            document.cookie = `${name}=${value}`
        } else {
            const currentDate = new Date();
            const expiryDate = new Date(currentDate.getTime() + 30 * 24 * 60 * 60 * 1000); // 30 days in milliseconds
            const expires = "expires=" + expiryDate.toUTCString();
            const domain = ".creatosaurus.io";

            document.cookie = `${name}=${value}; ${expires}; domain=${domain}; path=/`;
        }
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }


    useEffect(() => {
        setredirectTo(query.app)
        setinvitationCode(query.ref)
        setAffiliate(query.affiliate)

        if (query.ref !== undefined) {
            setRefCookie("ref", query.ref)
        } else {
            const value = getCookie('ref');

            if (value !== undefined) {
                setinvitationCode(value)
            }
        }
    }, [query]);


    function setCookie(name, value) {
        if (process.env.NODE_ENV === 'development') {
            document.cookie = `${name}=${value}`
        } else {
            const currentDate = new Date();
            const expiryDate = new Date(currentDate.getTime() + 15 * 24 * 60 * 60 * 1000); // 15 days in milliseconds
            const expires = "expires=" + expiryDate.toUTCString();
            const domain = ".creatosaurus.io";

            document.cookie = `${name}=${value}; ${expires}; domain=${domain}; path=/`;
        }
    }



    const addDataToLocalStorage = async (res, email) => {
        try {
            setCookie("Xh7ERL0G", res.data.token)

            if (pathname === "/apps/muse") return push("/apps/muse/waitlist")

            setLoading(false);

            const queryString = router.asPath.split("?")
            if (email === "mayurgaikwad859@gmail.com") {
                window.open("https://www.analytics.creatosaurus.io/", "_self");
            } else if (redirectTo === null) {
                window.open("https://www.app.creatosaurus.io/", "_self");
            } else if (redirectTo === "muse") {
                if (queryString[1] !== undefined) return window.open(`https://www.muse.creatosaurus.io/editor?${queryString[1]}`, "_self");
                window.open("https://www.muse.creatosaurus.io", "_self");
            } else if (redirectTo === "apollo") {
                window.open("https://www.apollo.creatosaurus.io", "_self");
            } else if (redirectTo === "cache") {
                window.open("https://www.cache.creatosaurus.io", "_self");
            } else if (redirectTo === "captionator") {
                window.open("https://www.captionator.creatosaurus.io/", "_self");
                const template = query.template
                if (template !== undefined) return window.open(`https://www.captionator.creatosaurus.io?${queryString[1]}`, "_self");
            } else if (redirectTo === "quotes") {
                window.open("https://www.quotes.creatosaurus.io/", "_self");
            } else if (redirectTo === "hashtags") {
                window.open("https://www.hashtags.creatosaurus.io", "_self");
            } else if (redirectTo === "steno") {
                window.open("https://www.steno.creatosaurus.io/", "_self");
            } else {
                window.open("https://www.app.creatosaurus.io/", "_self");
            }
        } catch (error) {
            setLoading(false);
        }
    }

    const loginWithGoogle = async (response, type) => {
        try {
            let token = type === "oneTap" ? response.credential : response.access_token

            setLoading(true);
            const res = await axios.post(constant.url + "login", {
                token: token,
                authType: "google",
                type: type,
                invitationCodeFromUser: invitationCode,
                affiliate: pathname === "/affiliate" ? "yes" : affiliate,
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                appSumoCode: appSumoCode === undefined || appSumoCode === null ? "" : appSumoCode
            })

            setLoading(false)
            addDataToLocalStorage(res)
        } catch (error) {
            toast.error(error.response.data.error)
            setLoading(false);
        }
    }

    useGoogleOneTapLogin({
        onSuccess: res => loginWithGoogle(res, "oneTap")
    })


    return (
        <>
        </>
    )
}

export default NavigationAuthGooglePopup