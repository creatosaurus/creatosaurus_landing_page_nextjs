import React from 'react'
import styles from './AppIntegrations.module.css'
import Image from 'next/image'
import Instagram from './assets/instagram.png'
import Facebook from './assets/facebook.png'
import X from './assets/x.png'
import Linkedin from './assets/linkedin.png'
import Youtube from './assets/youtube.png'
import Pinterest from './assets/pinterest.png'
import Tumblr from './assets/tumblr.png'
import Drive from './assets/googledrive.png'
import Photos from './assets/googlephotos.png'
import Dropbox from './assets/dropbox.png'
import Giphy from './assets/giphy.png'
import Tenor from './assets/tenor.png'
import Clearbit from './assets/clearbit.png'
import Bitmoji from './assets/bitmoji.png'
import Pexels from './assets/pexels.png'
import Unsplash from './assets/unsplash.png'
import Pixabay from './assets/pixabay.png'
import Slack from './assets/slack.png'
import Teams from './assets/teams.png'
import Box from './assets/box.png'
import Onedrive from './assets/onedrive.png'
import Qrcode from './assets/qrcode.png'
import Brandkit from './assets/brandKit.png'
import Muse from './assets/muse.png'
import Cache from './assets/cache.png'
import Noice from './assets/noice.png'
import Steno from './assets/steno.png'
import Hashtags from './assets/hashtag.png'
import Quotes from './assets/quotes.png'
import Emoji from './assets/emoji.png'
import Captionator from './assets/captionator.png'


const AppIntegrations = () => {
    const appsIntegrations = [
        {
            name: "Instagram",
            logo: Instagram
        },
        {
            name: "Facebook",
            logo: Facebook
        },
        {
            name: "X(Twitter)",
            logo: X
        },
        {
            name: "Linkedin",
            logo: Linkedin
        },
        {
            name: "Youtube",
            logo: Youtube
        },
        {
            name: "Pinterest",
            logo: Pinterest
        },
        {
            name: "Tumblr",
            logo: Tumblr
        },
        {
            name: "Drive",
            logo: Drive
        },
        {
            name: "Photos",
            logo: Photos
        },
        {
            name: "Dropbox",
            logo: Dropbox
        },
        {
            name: "Giphy",
            logo: Giphy
        },
        {
            name: "Tenor",
            logo: Tenor
        },
        {
            name: "Clearbit",
            logo: Clearbit
        },
        {
            name: "Bitmoji",
            logo: Bitmoji
        },
        {
            name: "Brandkit",
            logo: Brandkit
        },
        {
            name: "Pexels",
            logo: Pexels
        },
        {
            name: "Unsplash",
            logo: Unsplash
        },
        {
            name: "Pixabay",
            logo: Pixabay
        },
        {
            name: "Slack",
            logo: Slack
        },
        {
            name: "Teams",
            logo: Teams
        },
        {
            name: "Box",
            logo: Box
        },
        {
            name: "OneDrive",
            logo: Onedrive
        },
        {
            name: "Qrcode",
            logo: Qrcode
        },
        {
            name: "Muse",
            logo: Muse
        },
        {
            name: "Cache",
            logo: Cache
        },
        {
            name: "Noice",
            logo: Noice
        },
        {
            name: "Steno",
            logo: Steno
        },
        {
            name: "Tags",
            logo: Hashtags
        },
        {
            name: "Quotes",
            logo: Quotes
        },
        {
            name: "Emoji",
            logo: Emoji
        },
        {
            name: "Captionator",
            logo: Captionator
        }
    ]

    return (
        <div className={styles.appIntegrationContainer}>
            <h2>Say Hello to Apps & Integrations</h2>
            <div className={styles.wrapper}>
                {
                    appsIntegrations.map((data, index) => {
                        return <div key={index} className={styles.card}>
                            <div className={styles.imageWrapper}>
                                <Image src={data.logo} alt={data.name} />
                            </div>
                            <span>{data.name}</span>
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default AppIntegrations