import React, { useEffect, useState, useCallback } from 'react';
import axios from 'axios';
import jwtDecode from 'jwt-decode';
import { checkLoginStatus } from '../../utils/CheckLoginStatus';
import Image from 'next/image';
import Poster from '../../public/NewAssets/Home/poster.webp'
import styles from './invitation.module.css'
import Link from 'next/link';
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth';
import { toast } from 'react-toastify';
import constant from '../../constant';
import HeadInfo from '../../ComponentsNew/HeadTag/HeadInfo';

export async function getServerSideProps(context) {
    const { slug } = context.params;
    const [id, email] = slug || [];

    if (!id || !email) return { notFound: true };

    try {
        const response = await axios.get(`${constant.url}v2/workspace/invitation/details/${id}/${email}`);
        return { props: { data: response.data, email } };
    } catch (error) {
        return { notFound: true };
    }
}

const Index = ({ data, email }) => {
    const [showLogin, setShowLogin] = useState(false)
    const [sameEmailAsInviteEmail, setSameEmailAsInviteEmail] = useState(false)
    const [showLoginPopup, setShowLoginPopup] = useState(false)

    const getCookie = (name) => {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const checkWorkspaceUsersInfo = useCallback(() => {
        const { workspaceOwnerInfo, workspaceInfo } = data;
        const teamLength = workspaceInfo?.team.length - 1;
        const name = workspaceOwnerInfo?.firstName || '';
        const formattedName = `${name.charAt(0).toUpperCase()}${name.slice(1).toLowerCase()}`;

        if (teamLength === 1) {
            return `${formattedName} from your team is already part of the`;
        } else if (teamLength === 2) {
            return `${formattedName} & ${teamLength} other from your team is already part of the`;
        } else {
            return `${formattedName} & ${teamLength} others from your team are already part of the`;
        }
    }, [data]);

    useEffect(() => {
        if (!checkLoginStatus()) {
            setShowLogin(true)
        } else {
            const token = getCookie('Xh7ERL0G');
            const userName = jwtDecode(token).userName
            if (userName === data.userInfo?.userName) {
                setSameEmailAsInviteEmail(true)
            }
        }
    }, [])

    const acceptInvite = async () => {
        try {
            const token = getCookie('Xh7ERL0G');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            await axios.post(`${constant.url}v2/workspace/invitation/accept`, {
                id: data.workspaceInfo._id,
                email
            }, config)

            return window.open("https://www.app.creatosaurus.io/", "_self")
        } catch (error) {
            toast.error("Failed to accpet invitation")
        }
    }

    const cancelInvite = async () => {
        try {
            const token = getCookie('Xh7ERL0G');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            await axios.post(`${constant.url}v2/workspace/invitation/cancel`, {
                id: data.workspaceInfo._id,
                email
            }, config)

            return window.open("https://www.app.creatosaurus.io/", "_self")
        } catch (error) {
            toast.error("Failed to accpet invitation")
        }
    }

    const checkEmailAlreadyJoined = () => {
        const isJoined = data.workspaceInfo.team.find((data) => data.user_email === email && data.status === true)
        if (isJoined) return true
        return false
    }

    return (
        <>
            <HeadInfo />
            {showLoginPopup ? <NavigationAuth close={() => setShowLoginPopup(false)} /> : null}

            <div className='flex flex-row p-[10px] sm:p-[30px] gap-x-[20px] md:gap-x-[80px] justify-between pb-[0px]'>
                {/* Sidebar Image */}
                <div className="hidden lg:flex">
                    <div className={styles.img}>
                        <Image src={Poster} alt="Invitation Poster" />
                    </div>
                </div>


                {/* Main Content */}
                <div className='h-[calc(100vh-20px)] sm:h-[calc(100vh-60px)] overflow-scroll w-full lg:w-[60%]'>
                    <Link href="/">
                        <div className='flex items-center cursor-pointer mt-[100px]'>
                            <svg className='w-[50px] h-auto sm:w-[74px]' width="74" height="35" viewBox="0 0 74 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g clipPath="url(#clip0_3923_28766)">
                                    <path d="M49.5625 34.9997L46.3661 33.0225L45.8594 34.9997H49.5625Z" fill="black" />
                                    <path d="M54.2871 32.5083L50.0545 30.5615V32.5845L54.2871 32.5083Z" fill="black" />
                                    <path d="M45.452 34.8246L41.6956 31.2968L43.6462 27.9023L46.0234 32.6226L45.452 34.8246Z" fill="black" />
                                    <path d="M38.6094 7.93945L30.0146 17.5247L5.09516 17.3419L38.6094 7.93945Z" fill="black" />
                                    <path d="M59.9128 17.4258L59.9395 22.0736H54.9792L59.9128 17.4258Z" fill="black" />
                                    <path d="M57.0146 24.7022L59.9023 22.5078H57.0908L57.0146 24.7022Z" fill="black" />
                                    <path d="M41.4388 30.8741L47.5039 20.3135L39.5035 24.2261L41.4388 30.8741Z" fill="black" />
                                    <path d="M9.70117 15.601L4.77518 16.984L0.500652 14.4619L9.70117 15.601Z" fill="black" />
                                    <path d="M68.8273 13.9059L63.6041 12.1801L62.8688 7.68457L70.1035 13.3458L68.8273 13.9059Z" fill="black" />
                                    <path d="M49.623 32.5846L44.537 26.3518L46.7467 22.5078H49.623V32.5846Z" fill="black" />
                                    <path d="M47.4121 19.8788L39.1107 23.9362L30.5464 17.5739L39.0955 8.04199L45.3663 10.4497L47.4121 19.8788Z" fill="black" />
                                    <path d="M73.209 6.05341L64.7285 8.59069L62.8084 7.08584L61.9169 1.83984H71.0527L73.209 6.05341Z" fill="black" />
                                    <path d="M61.4818 1.83984L63.1543 12.0994L59.9198 16.8273L54.2052 11.2842L56.7578 6.38104L61.4818 1.83984Z" fill="black" />
                                    <path d="M54.35 22.0738H46.9934L47.9611 20.3899L45.8429 10.6104L53.5309 11.2275L59.6074 17.1212L54.35 22.0738Z" fill="black" />
                                    <path d="M66.5576 1.40579H68.6758L67.5633 0L66.5576 1.40579Z" fill="black" />
                                </g>
                                <defs>
                                    <clipPath id="clip0_3923_28766">
                                        <rect width="72.7087" height="35" fill="white" transform="matrix(-1 0 0 1 73.209 0)" />
                                    </clipPath>
                                </defs>
                            </svg>

                            <span className='text-[16px] sm:text-[20px] font-semibold ml-[5px]'>Creatosaurus</span>
                        </div>
                    </Link>

                    <>
                        {
                            checkEmailAlreadyJoined() ? <>
                                <div className='mt-[30px]'>
                                    <div className='text-[20px] sm:text-[24px] font-semibold'>Welcome Back!</div>
                                    <p className='text-[13px] sm:text-[15px] text-[#333333] mt-[10px]'>You’re already part of the workspace.</p>
                                    <p className='text-[13px] sm:text-[15px] text-[#333333] mt-[10px]'>Visit the <span onClick={() => window.open("https://www.app.creatosaurus.io/")} className='font-medium underline cursor-pointer'>Dashboard</span> to explore your projects and stay on top of your tasks.</p>
                                </div>
                            </>
                                :
                                <>
                                    <div className='mt-[30px]'>
                                        <div className='text-[20px] sm:text-[24px] font-semibold'>Join <span className='capitalize'>“{data.workspaceInfo?.workspace_name}”</span> on Creatosaurus</div>
                                        <p className='text-[13px] sm:text-[15px] text-[#333333] mt-[10px]'><span className='capitalize'>{data.workspaceOwnerInfo?.firstName + "  " + data.workspaceOwnerInfo?.lastName}</span> ({data.workspaceOwnerInfo?.email}) invited you ({email}) to join the Creatosaurus workspace <span className='capitalize font-medium'>“{data.workspaceInfo?.workspace_name}”</span>.</p>
                                        <p className='text-[13px] sm:text-[15px] text-[#333333] mt-[10px]'>{checkWorkspaceUsersInfo()} <span className='capitalize'>“{data.workspaceInfo?.workspace_name}”</span> workspace.</p>
                                        <p className='text-[13px] sm:text-[15px] text-[#333333] mt-[10px]'>Join now to start collaborating!</p>
                                    </div>


                                    {
                                        (showLogin || data.userInfo === null) ?
                                            <div className='mt-[25px]'>
                                                <button onClick={() => setShowLoginPopup(true)} className='bg-[#000000] text-white h-[30px] w-[130px] text-[12px] sm:text-[16px] sm:h-[45px] sm:w-[230px] rounded-[5px]'>{data.userInfo ? "Login" : "Sign Up"}</button>
                                            </div>
                                            :
                                            <>
                                                {
                                                    sameEmailAsInviteEmail &&
                                                    <div className='mt-[25px]'>
                                                        <button onClick={acceptInvite} className='bg-[#000000] text-white h-[30px] w-[130px] text-[12px] sm:text-[16px] sm:h-[45px] sm:w-[230px] rounded-[5px]'>Accept Invitation</button>
                                                        <button onClick={cancelInvite} className='text-[#808080] bg-[#F8F8F9] h-[30px] w-[130px] text-[12px] sm:text-[16px] sm:h-[45px] sm:w-[230px] border-[0.5px] border-[#C4C4C4] ml-[15px] sm:ml-[20px] rounded-[5px]'>Decline Invitation</button>
                                                    </div>
                                                }
                                            </>
                                    }

                                    {
                                        (!sameEmailAsInviteEmail && data.userInfo !== null && !showLogin) &&
                                        <>
                                            <div className='mt-[25px]'>
                                                <button onClick={() => setShowLoginPopup(true)} className='bg-[#000000] text-white h-[30px] w-[130px] text-[12px] sm:text-[16px] sm:h-[45px] sm:w-[230px] rounded-[5px]'>Switch account</button>
                                            </div>

                                            <div onClick={() => setShowLoginPopup(true)} className='mt-[25px] flex items-center cursor-pointer'>
                                                <div className='size-[30px] bg-[#979595] text-[14px] rounded-full uppercase flex justify-center items-center'>
                                                    {email.slice(0, 1)}
                                                </div>
                                                <div className='ml-[15px] flex flex-col '>
                                                    <span className='text-[14px]'>Join through ({email})</span>
                                                    <span className='text-[10px] text-[#808080] cursor-pointer'></span>
                                                </div>
                                            </div>
                                        </>
                                    }
                                </>
                        }
                    </>


                    <div className='h-[0.5px] w-full bg-[#DCDCDC] mt-[80px]' />

                    <div className='flex flex-col mt-[80px]'>
                        <span className='text-[16px] sm:text-[18px] font-medium'>What is Creatosaurus?</span>
                        <p className='text-[12px] sm:text-[14px] text-[#333333] mt-[15px]'>Creatosaurus is your all in one content creation & social media marketing tool powered by AI. It combines a graphic design editor, document editor, video editor, generative AI toolkit, social media scheduler, content calendar, social inbox, social analytics, apps, and more—all in one place.</p>

                        <div className='flex gap-x-[30px] mt-[13px]'>
                            <span className='text-[12px] text-[#808080] hover:underline hover:text-black'>
                                <Link href="/about">About</Link>
                            </span>

                            <span className='text-[12px] text-[#808080] hover:underline hover:text-black'>
                                <Link href="/blog">Blog</Link>
                            </span>

                            <span className='text-[12px] text-[#808080] hover:underline hover:text-black'>
                                <Link href="/terms">Terms</Link>
                            </span>

                            <span className='text-[12px] text-[#808080] hover:underline hover:text-black'>
                                <Link href="/privacy">Privacy</Link>
                            </span>

                            <span className='text-[12px] text-[#808080] hover:underline hover:text-black'>
                                <Link href="/contact">Contact</Link>
                            </span>
                        </div>

                        <span className='text-[12px] text-[#808080] mt-[13px]'>© 2024 Creatosaurus. All rights reserved by Creatosaurus</span>
                        <span className='text-[12px] text-[#808080] mt-[13px]'>Made with ❤️ for Creators</span>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Index
