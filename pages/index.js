import React from 'react'
import NavigationBar from '../Components/LandingPageComponents/NavigationBar'
import Trusted from '../ComponentsNew/BlackFridayComponents/trustedSection/Trusted'
import Testimonial from '../ComponentsNew/BlackFridayComponents/testimonialSection/Testimonial'
import PriceCutSection from '../ComponentsNew/BlackFridayComponents/priceCutSection/PriceCutSection'
import RatingComponent from '../Components/RatingComponent'
import UserRating from '../ComponentsNew/BlackFridayComponents/userRating/UserRating'
import StoryTelling from '../ComponentsNew/BlackFridayComponents/storyTelling/StoryTelling'
import ToolsInfo from '../ComponentsNew/BlackFridayComponents/toolsInfo/ToolsInfo'
import MoreSection from '../ComponentsNew/BlackFridayComponents/moreSection/MoreSection'
import ReviewSection from '../ComponentsNew/BlackFridayComponents/reviewSection/ReviewSection'
import AppIntegrations from '../ComponentsNew/BlackFridayComponents/AppIntegration/AppIntegrations'
import Footer from '../Components/LandingPageComponents/Footer'
import SecondBlackSection from '../ComponentsNew/BlackFridayComponents/secondBlackSection/SecondBlackSection'
import SupportedBy from '../Components/LandingPageComponents/SupportedBy'
import TemplateSection from '../ComponentsNew/BlackFridayComponents/templateSection/TemplateSection'
import axios from 'axios'
import RoadMap from '../ComponentsNew/BlackFridayComponents/roadMap/RoadMap'
import HeroSectionNew from '../ComponentsNew/BlackFridayComponents/heroSection/HeroSectionNew'
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo'

export async function getServerSideProps() {
    const res = await axios.get(`https://api.muse.creatosaurus.io/muse/admintemplates/best`)
    const midpoint = Math.floor(res.data.length / 2);

    let templateArray1 = res.data.slice(0, midpoint);
    let templateArray2 = res.data.slice(midpoint);

    return { props: { templateArray1, templateArray2 } }
}

const Index = ({ templateArray1, templateArray2 }) => {
    return (
        <div style={{ maxWidth: 1600, margin: 'auto' }}>
            <HeadInfo />
            <NavigationBar />
            <HeroSectionNew />
            <Trusted />
            <Testimonial />
            <PriceCutSection />
            <RatingComponent />
            <UserRating />
            <StoryTelling />
            <ToolsInfo />
            <MoreSection />
            <AppIntegrations />
            <ReviewSection />
            <TemplateSection templateArray1={templateArray1} templateArray2={templateArray2} />
            <RoadMap />
            <SupportedBy />
            <SecondBlackSection />
            <Footer />
        </div>
    )
}

export default Index