import React, { useState } from 'react'
import Head from 'next/head';
import Image from 'next/image';
import NavigationBar from '../../../../Components/LandingPageComponents/NavigationBar';
import museStyle from '../../../../styles/muse.module.css';
import Image1 from '../../../../public/Assets/Captionator/funny-new-year-resolution-idea-generator.png'
import apps from "../../../../appsInfo";
import FrequentlyAskedQuestions from '../../../../Components/FaqSection';
import FifthBlackSection from '../../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../../Components/LandingPageComponents/Footer';

import Image2 from '../../../../public/Assets/Captionator/Customized Funny New Year Resolution Ideas.jpg'
import Image3 from '../../../../public/Assets/Captionator/Diverse New Year Funny Resolution AI Copies.jpg'
import Image4 from '../../../../public/Assets/Captionator/Use Funny New Year Resolution Ideas Right Away.jpg'
import Image5 from '../../../../public/Assets/Captionator/Save Your Favourite New Year Funny Resolution Ideas.jpg'
import Image6 from '../../../../public/Assets/Captionator/Share Funny New Year Resolution Ideas Easily.jpg'



const FunnyNewYearIdea = () => {
    const [count, setcount] = useState(0)


    let funnyIdea = [
        {
            title: "Your passion - F1",
            p: "Install a race track in my house so I can learn the tricks of the trade track side."
        }, {
            title: "Your passion - Memes",
            p: "Go on a quest to find the funniest meme of all time."
        }, {
            title: "Your passion - Elon Musk",
            p: "Invest in SpaceX and then try to outsmart the brilliant mind of Elon Musk."
        }, {
            title: "Your passion - Butter Chicken",
            p: "Try an vegan version of butter chicken - vegan 'butter' and vegan 'chicken'!"
        }, {
            title: "Your passion - Exercise",
            p: "Create a new fitness routine based on how active a sloth would be."
        }, {
            title: "Your passion - AI",
            p: "Make AI my best friend by talking to it on a daily basis instead of humans."
        }, {
            title: "Your passion - Rajma Chawal",
            p: "Become 'Rajma Professionals' and save the world with this delicious and nutritious dish."
        }, {
            title: "Your passion - Uno",
            p: "Play Uno but with made-up rules that change every turn."
        }, {
            title: "Your passion - Justin Bieber",
            p: "Become an instrumentalist with the same passion as Justin Bieber, but without churning out hit singles every other week."
        }, {
            title: "Your passion - Shirts",
            p: "Ditch all the graphic t-shirts and find more fashionable options for a perfect shirt wardrobe."
        }, {
            title: "Your passion - Technology",
            p: "Be the only person who refuses to upgrade their smartphone despite the constant pressure from family and friends."
        }, {
            title: "Your passion - Blockchain",
            p: "Trying to explain Blockchain to your non-tech savvy parents."
        }, {
            title: "Your passion - Clouds",
            p: "Find a way to live amongst the clouds and pretend to be one of the fluffy white puffballs floating in the sky."
        }, {
            title: "Your passion - Poems",
            p: "Write a poem with rhythm and rhyme, but intentionally mis-spell every single word!"
        }, {
            title: "Your passion - Study",
            p: "Study hard and write an entire thesis with only emoji."
        }, {
            title: "Your passion - Reading",
            p: "Create a book club that only reads books that have never been published."
        }, {
            title: "Your passion - Inflation",
            p: "Resolution - Invest all of my extra coins into the stock market to beat inflation and feel like a big shot"
        }, {
            title: "Your passion - Stock Market Trading",
            p: "Create a portfolio of 5 stocks and commit to never trading them, no matter how tempting"
        }, {
            title: "Your passion - Economy",
            p: "Take a break from the stock market and invest in a piggy bank."
        }, {
            title: "Your passion - Climate Change",
            p: "Plant a tree for each Kardashian Tweet."
        }, {
            title: "Your passion - Social media",
            p: "Post constantly on Social Media, but don't look at the likes and comments until 6 months later"
        }, {
            title: "Your passion - Comedy",
            p: "Take a stand-up comedy class and use it as an excuse to tell all the bad jokes I can think of."
        }, {
            title: "Your passion - Movies",
            p: "Become a professional movie critic and only watch movies outside of my own genre."
        }, {
            title: "Your passion - Dieting",
            p: "Try the most outrageous diet trends and document the results with hilarious commentary."
        }, {
            title: "Your passion - Running",
            p: "Run eight miles every morning but never go anywhere."
        }, {
            title: "Your passion - Relax",
            p: "Hire a professional to come over and make all the decisions for you for one day."
        }, {
            title: "Your passion - Sport",
            p: "Start a new sport where we compete to see who can make the most creative excuses for why we don't want to join the game."
        }, {
            title: "Your passion - Space",
            p: "Create an inter-planetary delivery service that delivers memes to its users on other planets."
        }, {
            title: "Your passion - Anime",
            p: "Create a reality show where two anime characters must switch places and complete tasks without breaking character."
        }, {
            title: "Your passion - Relationship",
            p: "Attempt to maintain a relationship between two people from different time zones."
        }, {
            title: "Your passion - Marriage",
            p: "Start planning for an afterlife wedding to prepare for an afterlife marriage."
        }
    ]


    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };


    let data = [
        {
            title: "Customized Funny New Year Resolution Ideas",
            p: "Anything that rings a bell feels like it truly belongs to you — the same goes for new year funny resolutions. Harness the power of AI to make this happen. Provide keyword inputs about anything that lights you up or things you are passionate about — Captionator will wield them to tailor funny new year resolution ideas for you.",
            img : Image2
        }, {
            title: "Diverse New Year Funny Resolution AI Copies",
            p: "Who does not want to be left spoilt for choice? Captionator is your genie — it captures inputs about your passion to craft multiple funny new year resolution ideas personalized just for you. Choose the custom copies that work for you. Or tweak the same keyword inputs to generate extra AI output!",
            img : Image3
        }, {
            title: "Use Funny New Year Resolution Ideas Right Away",
            p: "When you find a humorous new year’s resolution idea that ticks all your boxes, wait no more. Spread chuckles to the people in your life. Captionator Funny New Year Resolution Generator unfolds this possibility in a fuss-free way — copy, save and share your favourite custom AI output to any platform in one click.",
            img : Image4
        }, {
            title: "Save Your Favourite New Year Funny Resolution Ideas",
            p: "The funny new year resolution AI output you like deserves a place of its own — whether it is for seamless storing or 24/7 access in your in-built dashboard. Captionator understands this — which is why it lets you save customized AI copies at the click of a button.",
            img : Image5
        }, {
            title: "Share Funny New Year Resolution Ideas Easily",
            p: "Once you land on AI-generated new year funny resolution ideas customized exclusively for you, go one step further. Double the fun and laughter by sharing your custom AI copy with your family and friends in one click — either through a personal message or a social media post.",
            img : Image6
        }
    ]


    let faq = [
        {
            id: 12,
            title: "Is Funny New Year Resolution AI Generator free?",
            content: "Free generation of Funny New Year Resolution Idea custom made for you using Captionator AI is limited to 5000 AI words. Upgrade to paid plans after this.",
        },  {
            id: 13,
            title: "What is Captionator AI writer and AI Assistant?",
            content: "Captionator is a free online AI writer & AI writing assistant. Research, inspiration, brainstorming — content creation is a creative and time-consuming journey. Let Captionator AI Writer take this task upon itself by generating high-quality, plagiarism-free AI copies without you having to lift a finger — anytime, anywhere, any kind. Get started for free.",
        },
        { 
            id: 1,
            title: "Is Captionator AI Writing Assistant free?",
            content: "Free generation of AI marketing and sales copies for 100+ use cases is limited to 5000 AI words. Upgrade to paid plans after this.",
        }, {
            id: 2,
            title: "Is Captionator AI Writer easy to use?",
            content: "Choose your preferred copy type. Provide product and brand-related inputs, along with required keywords. Generate AI copy right away.",
        }, {
            id: 3,
            title: "Who can use Captionator AI Writer?",
            content: "Captionator AI Writing Assistant is for content creators, marketing professionals, and copywriters.",
        }, {
            id: 4,
            title: "Is the content generated by Captionator AI Writer plagiarism-free?",
            content: "All AI copies crafted by Captionator AI Writing Assistant go through intensive plagiarism checks to ensure original AI content but we still suggest you to fact and plagiarism check at your end",
        }, {
            id: 5,
            title: "Is Captionator AI Copywriting Tool SEO-friendly?",
            content: "Provide SEO keyword inputs in the description box. Captionator AI Writer will seamlessly blend them into your AI-generated copy.",
        }
    ]

    const showNext = () => {
        let countData = count + 1
        if (countData >= funnyIdea.length) {
            setcount(0)
        } else {
            setcount(countData)
        }
    }

    const showPrev = () => {
        let countData = count - 1
        if (countData < 0) {
            setcount(funnyIdea.length - 1)
        } else {
            setcount(countData)
        }
    }

    const openUrl = (url) => {
        window.open(url)
    }

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Funny New Year Resolution Ideas Generator | Captionator AI</title>
                <meta
                    name="title"
                    content="Funny New Year Resolution Ideas Generator | Captionator AI"
                />
                <meta
                    name="description"
                    content="Step into 2023 with giggles, smiles & zeal — Captionator AI crafts custom made funny new year resolution ideas in seconds for you. Generate your free New Year Funny Resolution today."
                />
                <meta
                    property="og:title"
                    content="Funny New Year Resolution Ideas Generator | Captionator AI"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Step into 2023 with giggles, smiles & zeal — Captionator AI crafts custom made funny new year resolution ideas in seconds for you. Generate your free New Year Funny Resolution today."
                    key="description"
                />
            </Head>
            <NavigationBar />

            <div className={museStyle.top_block} style={{margin: '150px 0px'}}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.funnyIdea}>
                        <h6>{funnyIdea[count].title}</h6>
                        <p>{funnyIdea[count].p}</p>
                        <div>
                            <button title='Previous' onClick={showPrev} style={{ marginRight: 20 }}>{"<"}</button>
                            <button title="Next" onClick={showNext}>{">"}</button>
                        </div>
                    </div>
                    <div className={museStyle.blockText1}>
                        <div className={museStyle.blockText_title}>
                            <h1>AI generated funny new year resolution ideas tailored for you!</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>
                                What better way than funny new year resolutions to tickle your tummy and bond with your loved ones? Unlock Witty Beginnings. Let Captionator AI do this for you.
                            </p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button style={{ width: 280 }} onClick={() => {
                                openUrl("https://www.captionator.creatosaurus.io/?app=captionator&template=funny%20new%20year%20resolution%20ideas");
                            }}>Create your AI Resolution</button>
                        </div>
                    </div>
                </div>
            </div>



            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/eS5tpAUEuzA?start=175'
                    title='YouTube video player'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>



            <div className={museStyle.dataBlocks} style={{marginTop:100}}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.img} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h1>{res.title}</h1>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button style={{ width: 280 }} onClick={() => {
                                        openUrl("https://www.captionator.creatosaurus.io/?app=captionator&template=funny%20new%20year%20resolution%20ideas");
                                    }}>Create your AI Resolution</button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.redPoster}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h1>Free online AI Content Writer and AI Writing Assistant</h1>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                            Any content creation strategy calls for captivating storytelling — generate AI copies in seconds with Captionator AI Writing Assistant. Use AI Writer for free today.
                            </p>
                        </div>
                        <div onClick={()=> openUrl("https://www.captionator.creatosaurus.io/")} className={museStyle.redPosterButton}>
                            <button>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <Image src={Image1} alt='muse_img loading...' />
                    </div>
                </div>
            </div>



            <div className={museStyle.how_to_container}>
                <h1>How Does Captionator New Year Funny Resolutions Generator Work?</h1>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card} style={{padding:'0px 10px'}}>
                        <p>1. Select Funny New Year Resolution Ideas copy type.</p>
                    </div>
                    <div className={museStyle.card} style={{padding:'0px 10px'}}>
                        <p>2. Key in brief inputs related to any of your passion.</p>
                    </div>
                    <div className={museStyle.card} style={{padding:'0px 10px'}}>
                        <p>3. And voila! Generate custom AI new year funny resolution ideas.</p>
                    </div>
                </div>
            </div>


            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>;
                        })}
                    </div>
                </div>
            </div>


            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default FunnyNewYearIdea