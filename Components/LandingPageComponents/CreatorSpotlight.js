import React from 'react';
import stylesCreatorSpot from '../LandingPageComponentCss/CreatorSpotlight.module.css';
import Quote from '../../public/Assets/Quote.svg';
import ArrowPoint from '../../public/Assets/ArrowPoint.svg';
import ArrowRight from '../../public/Assets/ArrowRight.svg';
import Image from 'next/image';

const CreatorSpotlight = () => {
  return (
    <div className={stylesCreatorSpot.creatorSpotlight}>
      <div>
        <h1>Creators spotlight</h1>
        <p>
          The world&apos;s biggest influencers, creators, publishers, startups
          and brands use <br />
          Creatosaurus for their creative workflow.
        </p>
        <div className={stylesCreatorSpot.emailContainer}>
          <input type='text' placeholder='Enter your email' />
          <button>Join the tribe</button>
        </div>
        <div className={stylesCreatorSpot.slider}>
          <div className={stylesCreatorSpot.card1}>
            <div className={stylesCreatorSpot.quote}>
              <Image src={Quote} alt='' />
            </div>
            <div className={stylesCreatorSpot.creatorInfo}>
              <div>Raj Sharma |</div> <span>Creator</span>
              <Image src={ArrowPoint} alt='' />
            </div>
            <p style={{ fontWeight: 400, marginTop: 10 }}>
              The world&apos;s biggest influencers, creators, publishers,
              startups and brands use
              <br />
              Creatosaurus for their creative workflow.
            </p>
          </div>

          <div className={stylesCreatorSpot.card1}>
            <div className={stylesCreatorSpot.quote}>
              <Image src={Quote} alt='' />
            </div>
            <div className={stylesCreatorSpot.creatorInfo}>
              <div>Raj Sharma |</div> <span>Creator</span>
              <Image src={ArrowPoint} alt='' />
            </div>
            <p style={{ fontWeight: 400, marginTop: 10 }}>
              The world&apos;s biggest influencers, creators, publishers,
              startups and brands use
              <br />
              Creatosaurus for their creative workflow.
            </p>
          </div>
        </div>
      </div>
      <div className={stylesCreatorSpot.slider2}>
        <div className={stylesCreatorSpot.profileContainer}>
          <div className={stylesCreatorSpot.card2}>
            <div className={stylesCreatorSpot.profileImage} />
          </div>
          <div className={stylesCreatorSpot.card2}>
            <div className={stylesCreatorSpot.profileImage2} />
          </div>
        </div>
        <div className={stylesCreatorSpot.arrow}>
          <Image src={ArrowRight} alt='' />
        </div>
      </div>
    </div>
  );
};

export default CreatorSpotlight;
