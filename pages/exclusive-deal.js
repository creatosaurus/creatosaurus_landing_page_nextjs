import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import axios from 'axios'
import NavigationBar from '../Components/LandingPageComponents/NavigationBar'
import HeroSectionNew from '../ComponentsNew/BlackFridayComponents/heroSection/HeroSectionNew'
import Trusted from '../ComponentsNew/BlackFridayComponents/trustedSection/Trusted'
import PriceCutSection from '../ComponentsNew/BlackFridayComponents/priceCutSection/PriceCutSection'
import TeamsSection from '../ComponentsNew/BlackFridayComponents/teamsSection/TeamsSection'
import RatingComponent from '../Components/RatingComponent'
import StoryTelling from '../ComponentsNew/BlackFridayComponents/storyTelling/StoryTelling'
import ToolsInfo from '../ComponentsNew/BlackFridayComponents/toolsInfo/ToolsInfo'
import BlackSection from '../ComponentsNew/BlackFridayComponents/blackSection/BlackSection'
import AppIntegrations from '../ComponentsNew/BlackFridayComponents/AppIntegration/AppIntegrations'
import ReviewSection from '../ComponentsNew/BlackFridayComponents/reviewSection/ReviewSection'
import TemplateSection from '../ComponentsNew/BlackFridayComponents/templateSection/TemplateSection'
import RoadMap from '../ComponentsNew/BlackFridayComponents/roadMap/RoadMap'
import LimitedUserSection from '../ComponentsNew/BlackFridayComponents/limitedUserSection/LimitedUserSection'
import SupportedBy from '../Components/LandingPageComponents/SupportedBy'
import FaqSection from '../ComponentsNew/BlackFridayComponents/FAQSection/FaqSection'
import SecondBlackSection from '../ComponentsNew/BlackFridayComponents/secondBlackSection/SecondBlackSection'
import Footer from '../Components/LandingPageComponents/Footer'
import HeroSection from '../ComponentsNew/BlackFridayComponents/heroSection/HeroSection'
import PricingCards from '../ComponentsNew/BlackFridayComponents/pricingCards/PricingCards'
import PaymentSuccess from '../ComponentsNew/CheckOutPopup/PaymentSuccess'
import CheckoutPage1 from '../ComponentsNew/CheckOutPopup/CheckoutPage1'
import CheckoutPage2 from '../ComponentsNew/CheckOutPopup/CheckoutPage2'
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo'

export async function getServerSideProps() {
    const res = await axios.get(`https://api.muse.creatosaurus.io/muse/admintemplates/best`)
    const midpoint = Math.floor(res.data.length / 2);

    let templateArray1 = res.data.slice(0, midpoint);
    let templateArray2 = res.data.slice(midpoint);

    return { props: { templateArray1, templateArray2 } }
}


const ExclusiveDeal = ({ templateArray1, templateArray2 }) => {
    const [showLogin, setshowLogin] = useState(false)
    const [isYearlyDuration, setIsYearlyDuration] = useState(true)
    const [isIndian, setIsIndian] = useState(false)
    const [selectedPlan, setSelectedPlan] = useState("")
    const [showCheckOut, setShowCheckOut] = useState(false)
    const [showSecondCheckout, setShowSecondCheckout] = useState(false)
    const [paymentComplete, setPaymentComplete] = useState(false)

    useEffect(() => {
        if (Intl.DateTimeFormat().resolvedOptions().timeZone === "Asia/Calcutta") {
            setIsIndian(true)
        }

        const script = document.createElement('script');
        script.src = 'https://checkout.razorpay.com/v1/checkout.js';
        script.async = true;
        document.body.appendChild(script);
    }, [])


    const changeDuration = (type) => {
        if (type === "yearly") {
            setIsYearlyDuration(true)
        } else {
            setIsYearlyDuration(false)
        }
    }

    const selectPlan = (planDetails) => {
        setSelectedPlan(planDetails)
        setShowCheckOut(true)
    }

    const closeCheckoutPage1 = () => {
        setShowCheckOut(false)
    }

    const openSecondCheckoutPage = () => {
        setShowCheckOut(false)
        setShowSecondCheckout(true)
    }

    const handlePaymetComplete = () => {
        setPaymentComplete(true)
    }

    const handleSecondCheckoutBack = () => {
        setShowCheckOut(true)
        setShowSecondCheckout(false)
    }

    return (
        <div style={{ maxWidth: 1600, margin: 'auto' }}>
            <HeadInfo />
            {
                paymentComplete && <PaymentSuccess
                    selectedPlan={selectedPlan}
                    close={() => setPaymentComplete(false)} />
            }

            {
                showCheckOut &&
                <CheckoutPage1
                    isYearlyDuration={isYearlyDuration}
                    isIndian={isIndian}
                    lifeTimeDeal={true}
                    selectedPlan={selectedPlan}
                    changeDuration={changeDuration}
                    next={openSecondCheckoutPage}
                    close={closeCheckoutPage1} />
            }

            {
                showSecondCheckout &&
                <CheckoutPage2
                    isYearlyDuration={isYearlyDuration}
                    isIndian={isIndian}
                    lifeTimeDeal={true}
                    back={handleSecondCheckoutBack}
                    selectedPlan={selectedPlan}
                    handlePaymetComplete={handlePaymetComplete}
                    close={() => setShowSecondCheckout(false)} />
            }

            <NavigationBar />
            <HeroSection selectPlan={selectPlan} />
            <HeroSectionNew hideTitles={true} />
            <Trusted />
            <PriceCutSection />
            <TeamsSection />
            <RatingComponent />
            <PricingCards isYearlyDuration={isYearlyDuration} isIndian={isIndian} selectPlan={selectPlan} />
            <StoryTelling />
            <ToolsInfo />
            <BlackSection />
            <AppIntegrations />
            <ReviewSection />
            <TemplateSection templateArray1={templateArray1} templateArray2={templateArray2} />
            <RoadMap />
            <LimitedUserSection />
            <SupportedBy />
            <FaqSection />
            <SecondBlackSection />
            <Footer />
        </div>
    )
}

export default ExclusiveDeal