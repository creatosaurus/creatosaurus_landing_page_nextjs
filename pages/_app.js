import '../styles/globals.css';
import NextNProgress from 'nextjs-progressbar';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import dynamic from 'next/dynamic'
import { GoogleOAuthProvider } from '@react-oauth/google';
import Head from 'next/head';

/*const FacebookPixel = dynamic(() => import("./scripts/FacebookPixel"), {
  ssr: false
})*/

const Reb2b = dynamic(() => import("./scripts/Reb2b"), {
  ssr: false
})

const ClearBit = dynamic(() => import("./scripts/ClearBit"), {
  ssr: false
})

const Cookie = dynamic(() => import("./Cookie"), {
  ssr: false
})

const GoogleAnalytics = dynamic(() => import("./scripts/GoogleAnalytics"), {
  ssr: false
})

const MicrosoftClarity = dynamic(() => import("./scripts/MicrosoftClearity"), {
  ssr: false
})

/*const Tawkto = dynamic(() => import("./scripts/Tawkto"), {
  ssr: false
})*/

const Plausible = dynamic(() => import("./scripts/Plausible"), {
  ssr: false
})


const FactorsAi = dynamic(() => import("./scripts/FactorsAi"), {
  ssr: false
})

function MyApp({ Component, pageProps }) {
  return (
    <>
      <GoogleOAuthProvider clientId="349325227003-mg6k6mpcusljic2ukt1iukv1uepm6sr2.apps.googleusercontent.com">
        <ToastContainer
          position='top-right'
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Head>
          <meta name="yandex-verification" content="4fd8d467f650b823" />
        </Head>
        <NextNProgress height={2} color='#ff4359' />
        <Component {...pageProps} />

        <MicrosoftClarity />
        <GoogleAnalytics />
        <Reb2b />
        {/**<FacebookPixel /> */}
        <Plausible />
        <ClearBit />
        <FactorsAi />
        {/* Tawk.to         <Tawkto /> */}
        <Cookie />
      </GoogleOAuthProvider>
    </>
  );
}

export default MyApp;
