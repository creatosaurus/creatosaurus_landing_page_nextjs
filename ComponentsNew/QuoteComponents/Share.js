import React from 'react'
import { toast } from 'react-toastify';
import { useRouter } from 'next/router'

const Share = ({ showQuote, quote, author, imageURL }) => {
    const router = useRouter()
    const fullUrl = "https://creatosaurus.io" + useRouter().asPath;

    const getCookie = (name) => {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");

        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const checkUserLogin = () => {
        const value = getCookie('Xh7ERL0G');

        if (value) {
            const decoded = jwtDecode(value);
            const expirationTime = (decoded.exp * 1000) - 60000

            if (Date.now() >= expirationTime) {
                router.push('/login')
            } else {
                window.open("https://www.quotes.creatosaurus.io/", "_blank")
            }
        } else {
            router.push('/login')
        }
    }

    const copy = () => {
        toast.info('Copied')
        navigator.clipboard.writeText(quote + ' ' + author);
    }

    const shareToFacebook = () => {
        const postUrl = `https://www.facebook.com/sharer/sharer.php?u=${fullUrl}`
        window.open(postUrl, '_blank')
    }

    function shareToTwitter() {
        const encodedText = encodeURIComponent(quote);
        const encodedURL = encodeURIComponent(fullUrl);

        const tweetUrl = `https://twitter.com/intent/tweet?text=${encodedText}&url=${encodedURL}`
        window.open(tweetUrl, '_blank');
    }

    function shareToPinterest() {
        // Pinterest API endpoint
        const pinterestEndpoint = 'https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(window.location.href) + '&media=' + encodeURIComponent(imageURL) + '&description=' + encodeURIComponent(quote);
        window.open(pinterestEndpoint, '_blank');
    }

    function shareToLinkedIn() {
        const shareUrl = `https://www.linkedin.com/sharing/share-offsite/?url=${encodeURIComponent(fullUrl)}`;
        window.open(shareUrl, '_blank');
    }

    function shareViaEmail() {
        const encodedSubject = encodeURIComponent(quote);
        const encodedBody = encodeURIComponent(fullUrl);

        // Construct the mailto link
        const mailtoLink = `mailto:?subject=${encodedSubject}&body=${encodedBody}`;
        window.location.href = mailtoLink;
    }

    return (
        <div className="mt-[30px] self-center flex items-center gap-[20px]">
            {showQuote ? <span className='text-[14px] font-medium cursor-pointer underline' onClick={checkUserLogin}>Save Quote</span> : null}

            <svg className='cursor-pointer' onClick={copy} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.358 14.0355L10.0009 16.3926C9.18581 17.2077 8.20372 17.6153 7.05467 17.6153C5.90562 17.6153 4.92353 17.2077 4.10839 16.3926C3.29325 15.5774 2.88568 14.5953 2.88568 13.4463C2.88568 12.2972 3.29325 11.3151 4.10839 10.5L6.46541 8.14299L7.64392 9.3215L5.2869 11.6785C4.79585 12.1696 4.55033 12.7588 4.55033 13.4463C4.55033 14.1338 4.79585 14.723 5.2869 15.2141C5.77795 15.7051 6.3672 15.9506 7.05467 15.9506C7.74213 15.9506 8.33139 15.7051 8.82243 15.2141L11.1795 12.857L12.358 14.0355ZM8.23318 13.4463L7.05467 12.2678L11.7687 7.55373L12.9472 8.73224L8.23318 13.4463ZM13.5365 12.857L12.358 11.6785L14.715 9.3215C15.206 8.83045 15.4516 8.2412 15.4516 7.55373C15.4516 6.86627 15.206 6.27701 14.715 5.78596C14.2239 5.29492 13.6347 5.04939 12.9472 5.0494C12.2598 5.04939 11.6705 5.29492 11.1795 5.78596L8.82243 8.14299L7.64392 6.96448L10.0009 4.60745C10.8161 3.79232 11.7982 3.38475 12.9472 3.38475C14.0963 3.38475 15.0784 3.79232 15.8935 4.60745C16.7086 5.42259 17.1162 6.40468 17.1162 7.55373C17.1162 8.70278 16.7086 9.68487 15.8935 10.5L13.5365 12.857Z" fill="#242424" />
            </svg>

            <svg className='cursor-pointer' onClick={shareToFacebook} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.1635 17.9976V11.1676H13.4677L13.8102 8.49346H11.1635V6.79012C11.1635 6.01846 11.3785 5.49012 12.486 5.49012H13.8894V3.10596C13.2069 3.03263 12.5202 2.99762 11.8335 3.00012C9.79688 3.00012 8.39854 4.24346 8.39854 6.52596V8.48846H6.10938V11.1626H8.40354V17.9976H11.1635Z" fill="#242424" />
            </svg>

            <svg className='cursor-pointer' onClick={shareToTwitter} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M16.3602 7.16412C16.371 7.30995 16.371 7.45495 16.371 7.59995C16.371 12.0375 12.9935 17.1508 6.82102 17.1508C4.91935 17.1508 3.15268 16.6 1.66602 15.6433C1.93602 15.6741 2.19602 15.685 2.47685 15.685C4.04602 15.685 5.49018 15.155 6.64435 14.2508C5.16852 14.22 3.93185 13.2533 3.50518 11.9233C3.71268 11.9541 3.92102 11.975 4.13935 11.975C4.44018 11.975 4.74268 11.9333 5.02352 11.8608C3.48435 11.5491 2.33185 10.1983 2.33185 8.56662V8.52495C2.77935 8.77412 3.29852 8.92995 3.84852 8.95079C2.94435 8.34912 2.35185 7.31995 2.35185 6.15579C2.35185 5.53245 2.51768 4.96079 2.80852 4.46245C4.46102 6.49829 6.94518 7.82912 9.73018 7.97495C9.67852 7.72495 9.64685 7.46579 9.64685 7.20579C9.64685 5.35579 11.1435 3.84912 13.0035 3.84912C13.9702 3.84912 14.8427 4.25412 15.456 4.90912C16.2143 4.76329 16.941 4.48245 17.586 4.09829C17.3368 4.87745 16.806 5.53245 16.1102 5.94829C16.786 5.87495 17.441 5.68829 18.0427 5.42829C17.586 6.09329 17.0152 6.68579 16.3602 7.16412Z" fill="#242424" />
            </svg>

            <svg className='cursor-pointer' onClick={shareToPinterest} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10 2.5C5.58882 2.5 2 6.08882 2 10.5C2 13.938 4.18003 16.8757 7.23004 18.005C7.1422 17.2219 7.15597 15.9393 7.30093 15.3183C7.43667 14.735 8.17788 11.601 8.17788 11.601C8.17788 11.601 7.95412 11.1529 7.95412 10.4911C7.95412 9.45102 8.5567 8.67502 9.30711 8.67502C9.94535 8.67502 10.2531 9.15419 10.2531 9.7283C10.2531 10.3703 9.84496 11.3292 9.63356 12.218C9.45724 12.9627 10.0069 13.5698 10.7409 13.5698C12.07 13.5698 13.0917 12.1682 13.0917 10.1454C13.0917 8.3549 11.8054 7.10286 9.96837 7.10286C7.8408 7.10286 6.59192 8.69888 6.59192 10.3484C6.59192 10.991 6.83929 11.6801 7.14848 12.0552C7.20945 12.1294 7.21837 12.1941 7.20026 12.2695C7.1436 12.5056 7.01762 13.0136 6.99259 13.1177C6.96009 13.2549 6.88415 13.2834 6.74263 13.2175C5.80845 12.7826 5.22455 11.4172 5.22455 10.3202C5.22455 7.96143 6.93822 5.79473 10.166 5.79473C12.7603 5.79473 14.7763 7.64302 14.7763 10.114C14.7763 12.6914 13.1509 14.7655 10.8957 14.7655C10.1378 14.7655 9.42561 14.3717 9.182 13.9066C9.182 13.9066 8.80694 15.3344 8.71576 15.6844C8.55838 16.2898 7.8847 17.5439 7.54651 18.1136C8.32016 18.3635 9.14428 18.5 10 18.5C14.4113 18.5 18 14.9113 18 10.5C18 6.08882 14.4113 2.5 10 2.5Z" fill="#242424" />
            </svg>

            <svg className='cursor-pointer' onClick={shareToLinkedIn} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M4.15341 6.49774C5.16041 6.49774 5.97675 5.68141 5.97675 4.67441C5.97675 3.66741 5.16041 2.85107 4.15341 2.85107C3.14641 2.85107 2.33008 3.66741 2.33008 4.67441C2.33008 5.68141 3.14641 6.49774 4.15341 6.49774Z" fill="#242424" />
                <path d="M7.69841 7.87891V17.9947H10.8392V12.9922C10.8392 11.6722 11.0876 10.3939 12.7242 10.3939C14.3384 10.3939 14.3584 11.9031 14.3584 13.0756V17.9956H17.5009V12.4481C17.5009 9.72307 16.9142 7.62891 13.7292 7.62891C12.2001 7.62891 11.1751 8.46807 10.7559 9.26224H10.7134V7.87891H7.69841ZM2.58008 7.87891H5.72591V17.9947H2.58008V7.87891Z" fill="#242424" />
            </svg>

            <svg className='cursor-pointer' onClick={shareViaEmail} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M3.33268 17.1668C2.87435 17.1668 2.48199 17.0036 2.1556 16.6772C1.82921 16.3509 1.66602 15.9585 1.66602 15.5002V5.50016C1.66602 5.04183 1.82921 4.64947 2.1556 4.32308C2.48199 3.99669 2.87435 3.8335 3.33268 3.8335H16.666C17.1243 3.8335 17.5167 3.99669 17.8431 4.32308C18.1695 4.64947 18.3327 5.04183 18.3327 5.50016V15.5002C18.3327 15.9585 18.1695 16.3509 17.8431 16.6772C17.5167 17.0036 17.1243 17.1668 16.666 17.1668H3.33268ZM9.99935 11.3335L3.33268 7.16683V15.5002H16.666V7.16683L9.99935 11.3335ZM9.99935 9.66683L16.666 5.50016H3.33268L9.99935 9.66683ZM3.33268 7.16683V5.50016V15.5002V7.16683Z" fill="#242424" />
            </svg>
        </div>
    )
}

export default Share