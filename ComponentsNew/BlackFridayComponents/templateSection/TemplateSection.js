import React, { useState } from 'react'
import style from './TemplateSection.module.css'
import Image from 'next/image'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'

const TemplateSection = ({ templateArray1, templateArray2 }) => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <div className={style.templateSectionContainer}>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={style.info}>
                <h2>Thousands of modern premium templates</h2>
                <p>An ever-expanding selection of trendy high quality template sets, ready for your unique touch.</p>
                <button onClick={() => setShowLogin(true)} style={{ cursor: 'pointer' }}>Get Started Now {"->"}</button>
            </div>
            <div className={style.imageGrid}>
                <div className={style.row}>
                    {
                        templateArray1?.map(data => {
                            return <div key={data._id} className={style.imageWrapper}>
                                <Image src={data.image} alt={data.description} width="200" height="250" objectFit='contain' />
                            </div>
                        })
                    }
                </div>

                <div className={style.row}>
                    {
                        templateArray2?.map(data => {
                            return <div key={data._id} className={style.imageWrapper}>
                                <Image src={data.image} alt={data.description} width="200" height="250" objectFit='contain' />
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default TemplateSection