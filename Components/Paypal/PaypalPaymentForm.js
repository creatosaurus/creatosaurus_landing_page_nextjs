import { useState, useRef } from "react";
import styles from "./PaypalPaymentForm.module.css";
import {
    PayPalHostedFieldsProvider,
    PayPalHostedField,
    PayPalButtons,
    usePayPalHostedFields,
} from "@paypal/react-paypal-js";
import axios from 'axios';
import constant from '../../constant'

const PaypalPaymentForm = ({ payment }) => {
    const [message, setMessage] = useState("");

    const createOrderCallback = async () => {
        try {
            const response = await axios.post(`${constant.url}paypal/create/order`, {
                amount: payment.amount,
                currency: payment.currency,
                planName: payment.planName,
                planId: payment.planId,
                hasm: payment.hasm,
                hpsl: payment.hpsl
            })

            const orderData = response.data.order;

            if (orderData.id) {
                return orderData.id;
            } else {

                const errorDetail = orderData?.details?.[0];
                const errorMessage = errorDetail
                    ? `${errorDetail.issue} ${errorDetail.description} (${orderData.debug_id})`
                    : JSON.stringify(orderData);

                throw new Error(errorMessage);
            }
        } catch (error) {
            console.log(error)
        }
    }

    const onApproveCallback = async (data, actions) => {
        try {
            const response = await axios.post(`${constant.url}paypal/order/${data.orderID}/capture`, {}, {
                headers: {
                    "Content-Type": "application/json",
                },
            });

            const orderData = response.data.order;

            const transaction =
                orderData?.purchase_units?.[0]?.payments?.captures?.[0] ||
                orderData?.purchase_units?.[0]?.payments?.authorizations?.[0];
            const errorDetail = orderData?.details?.[0];

            if (errorDetail?.issue === "INSTRUMENT_DECLINED" && !data.card && actions) {
                return actions.restart();
            } else if (
                errorDetail ||
                !transaction ||
                transaction.status === "DECLINED"
            ) {
                let errorMessage;
                if (transaction) {
                    errorMessage = `Transaction ${transaction.status}: ${transaction.id}`;
                } else if (errorDetail) {
                    errorMessage = `${errorDetail.description} (${orderData.debug_id})`;
                } else {
                    errorMessage = JSON.stringify(orderData);
                }

                throw new Error(errorMessage);
            } else {
                const response = await axios.post(`${constant.url}paypal/upgrade/plan`, {
                    ...payment,
                    payment: orderData
                })
                if (response.data.message === "success") {
                    window.open('https://www.app.creatosaurus.io/confirmation', "_self",)
                }
            }
        } catch (error) {
            return `Sorry, your transaction could not be processed...${error}`;
        }
    };


    const SubmitPayment = ({ onHandleMessage }) => {
        const { cardFields } = usePayPalHostedFields();
        const cardHolderName = useRef(null);

        const submitHandler = () => {
            if (typeof cardFields.submit !== "function") return;
            cardFields
                .submit({
                    cardholderName: cardHolderName?.current?.value,
                })
                .then(async (data) => onHandleMessage(await onApproveCallback(data)))
                .catch((orderData) => {
                    onHandleMessage(
                        `Sorry, your transaction could not be processed...${JSON.stringify(
                            orderData,
                        )}`,
                    );
                });
        };

        return (
            <button onClick={submitHandler} className="btn btn-primary">
                Pay
            </button>
        );
    };

    const Message = ({ content }) => {
        return <p>{content}</p>;
    };

    return (
        <div className={styles.form}>
            <PayPalButtons
                style={{
                    shape: "rect",
                    //color:'blue' change the default color of the buttons
                    layout: "vertical", //default value. Can be changed to horizontal
                }}
                styles={{ marginTop: "4px", marginBottom: "4px", width: "100%" }}
                createOrder={createOrderCallback}
                onApprove={async (data) => setMessage(await onApproveCallback(data))}
            />

            <PayPalHostedFieldsProvider createOrder={createOrderCallback}>
                <div style={{ marginTop: "4px", marginBottom: "4px" }}>
                    <PayPalHostedField
                        id="card-number"
                        hostedFieldType="number"
                        options={{
                            selector: "#card-number",
                            placeholder: "Card Number",
                        }}
                        className={styles.input}
                    />
                    <div className={styles.container}>
                        <PayPalHostedField
                            id="expiration-date"
                            hostedFieldType="expirationDate"
                            options={{
                                selector: "#expiration-date",
                                placeholder: "Expiration Date",
                            }}
                            className={styles.input}
                        />
                        <PayPalHostedField
                            id="cvv"
                            hostedFieldType="cvv"
                            options={{
                                selector: "#cvv",
                                placeholder: "CVV",
                            }}
                            className={styles.input}
                        />
                    </div>
                    <div className={styles.container}>
                        <input
                            id="card-holder"
                            type="text"
                            placeholder="Name on Card"
                            className={styles.input}
                        />

                        <input
                            id="card-billing-address-country"
                            type="text"
                            placeholder="Country Code"
                            className={styles.input}
                        />
                    </div>
                    <SubmitPayment onHandleMessage={setMessage} />
                </div>
            </PayPalHostedFieldsProvider>
            <Message content={message} />
        </div>
    );
}

export default PaypalPaymentForm;