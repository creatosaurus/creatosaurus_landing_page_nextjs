import React from 'react'
import styles from './ReviewSection.module.css'
import Image from 'next/image'
import Stars from './assets/stars.svg'

const ReviewSection = () => {
    return (
        <div className={styles.reviewSectionContainer}>
            <Image priority={true} src={Stars} alt="stars" />
            <p>Being a Creatosaurus affiliate partner has been a game-changer for me.  I not only earn money but also authentically endorse a product I truly believe in. This program is a powerhouse in building my credibility and trust as an evangelist. Guiding newcomers to evolve into proficient marketers and storytellers is immensely fulfilling. The ease of getting started and the instant impact I&apos;ve observed make it a win-win.</p>
            <span>Chaitanya Badave, Buy or Skip</span>
        </div>
    );
}

export default ReviewSection;