import React, { useEffect, useState } from 'react'
import styles from './HeroSection.module.css'
import Image from 'next/image';
import HeroImage from "./assets/HeroSection.webp"
import Link from 'next/link';
import { getAvailableCodes } from '../../../utils/CheckAvailableCodes';

const HeroSection = ({ selectPlan, plans }) => {
    const [isIndian, setIsIndian] = useState(false)
    const [countdown, setCountdown] = useState({
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
    });
    const [showVideo, setShowVideo] = useState(false);
    const [selectedPlan, setSelectedPlan] = useState("6247085137b553074fb6b25a")

    const deadline = "December, 06, 2024";

    const getTime = () => {
        const time = Date.parse(deadline) - Date.now();

        setCountdown({
            days: Math.floor(time / (1000 * 60 * 60 * 24)),
            hours: Math.floor((time / (1000 * 60 * 60)) % 24),
            minutes: Math.floor((time / 1000 / 60) % 60),
            seconds: Math.floor((time / 1000) % 60)
        })
    };

    useEffect(() => {
        const interval = setInterval(() => getTime(deadline), 1000);
        if (Intl.DateTimeFormat().resolvedOptions().timeZone === "Asia/Calcutta") {
            setIsIndian(true)
        }

        return () => clearInterval(interval);
    }, []);

    const data = ["Graphic Design Editor", "Document Editor", "15+ AI Models Chat", "AI Image Generation Toolkit", "All In One Social Inbox", "Social Analytics", "Social Media Scheduling"]

    const checkCurrency = () => {
        return isIndian ? "₹" : "$"
    }

    const handlePlanChange = (e) => {
        const value = e.target.value
        setSelectedPlan(value)
    }

    const handleBuyPlan = () => {
        const planInfo = plans.find((data) => data.id === selectedPlan)
        selectPlan(planInfo)
    }

    const checkActiveSelectedPlan = () => {
        const planInfo = plans.find((data) => data.id === selectedPlan)
        if (planInfo) return `${checkCurrency()}${formatPrice(parseInt(planInfo.year / 2))}`
    }


    const checkSelectedPlanDiscount = () => {
        const planInfo = plans.find((data) => data.id === selectedPlan)
        if (planInfo) return `${checkCurrency()}${formatPrice(planInfo.year)}`
    }

    const checkSelectedPlanName = () => {
        const planInfo = plans.find((data) => data.id === selectedPlan)
        if (planInfo) return `${planInfo.planName}`
    }

    const formatPrice = (number) => {
        number = parseInt(number)
        return number.toLocaleString('en-IN');
    }

    return (
        <div className='mt-[150px] max-[960px]:px-[20px] min-[960px]:px-[80px] grid grid-cols-1 md:grid-cols-[360px_calc(100%-360px)] gap-[40px] lg:gap-[100px] items-center'>
            <div className='md:w-[400px] md:max-w-[400px] flex flex-col justify-center items-center md:justify-start md:items-start'>
                <span className='text-[14px] font-semibold text-[#0078FF]'>Only {getAvailableCodes()} License Available</span>
                <h1 className='flex flex-col text-[32px] md:text-[40px] font-bold mt-[5px] text-center md:text-left'>
                    <span>All in One Social</span>
                    <span className='mt-[-10px]'>Media Toolkit w/ AI</span>
                </h1>

                <div>
                    {
                        data.map(text => {
                            return <div className='flex items-center mt-[5px] text-[12px] md:text-[14px]'>
                                <svg className='size-[16px] md:size-[20px] mr-[10px]' width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.16602 10.8333L7.49935 14.1666L15.8327 5.83331" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                                <span>{text}</span>
                            </div>
                        })
                    }
                </div>

                <div className='mt-[10px]'>
                    <span className='text-[20px] md:text-[24px] font-semibold text-[#FF4359]'>50%</span>
                    <span className='text-[20px] md:text-[24px] font-semibold mx-[10px]'>{checkActiveSelectedPlan()}</span>
                    <span className='text-[12px] md:text-[14px] text-[#808080] line-through'>{checkSelectedPlanDiscount()}</span>
                </div>

                <div className='mt-[10px] w-full'>
                    <div className='flex justify-between w-full'>
                        <label className='text-[13px] font-medium'>Select a yearly plan</label>
                        <Link href="#price">
                            <span className='text-[13px] text-[#0078FF] cursor-pointer'>View plan details</span>
                        </Link>
                    </div>
                </div>


                <button className='h-[44px] border-[0.8px] border-[#333333] w-full rounded-[5px] flex justify-between items-center  pl-[15px] pr-[5px] mt-[10px] text-[16px] relative cursor-pointer'>
                    <span>{checkSelectedPlanName()} : {checkActiveSelectedPlan()} &nbsp;<span className='line-through text-[#808080]'>{checkSelectedPlanDiscount()}</span></span>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M20.7137 12.1431L14.9994 17.8574L9.28516 12.1431" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <select
                        value={selectedPlan}
                        onChange={handlePlanChange}
                        className='absolute top-0 left-0 right-0 bottom-0 opacity-0 cursor-pointer'>
                        {
                            plans.map((data, index) => {
                                if (index === 0) return
                                return <option key={data.id} value={data.id}>{data.planName}</option>
                            })
                        }
                    </select>
                </button>

                <button
                    onClick={handleBuyPlan}
                    className='h-[44px] rounded-[50px] bg-[#006CE6] text-[18px] font-bold text-white w-full mt-[15px]'>
                    Buy {checkSelectedPlanName()}
                </button>

                <div className='flex items-center justify-between mt-[15px] text-[13px] w-full'>
                    <span>Deal Ends In</span>
                    <span className='text-[#808080]'>14 Day Money Back Guarantee</span>
                </div>

                <div className='flex w-full mt-[10px]'>
                    <div className='text-[14px] h-[30px] border-[1px] border-[#808080] rounded-[5px] flex justify-center items-center w-full'>
                        {countdown.days}
                        <span className='text-[#333333]'>&nbsp;Days</span>
                    </div>
                    <span className='mx-[5px]'>:</span>
                    <div className='text-[14px] h-[30px] border-[1px] border-[#808080] rounded-[5px] flex justify-center items-center w-full'>
                        {countdown.hours}
                        <span className='text-[#333333]'>&nbsp;Hours</span>
                    </div>
                    <span className='mx-[5px]'>:</span>
                    <div className='text-[14px] h-[30px] border-[1px] border-[#808080] rounded-[5px] flex justify-center items-center w-full'>
                        {countdown.minutes}
                        <span className='text-[#333333]'>&nbsp;Mins</span>
                    </div>
                    <span className='mx-[5px]'>:</span>
                    <div className='text-[14px] h-[30px] border-[1px] border-[#808080] rounded-[5px] flex justify-center items-center w-full'>
                        {countdown.seconds}
                        <span className='text-[#333333]'>&nbsp;Secs</span>
                    </div>
                </div>
            </div>

            <div className={styles.video}>
                {showVideo ?
                    <iframe src="https://www.youtube.com/embed/eS5tpAUEuzA?autoplay=1" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                    : <>
                        <Image priority={true} src={HeroImage} alt="hero" objectFit='contain' />
                        <div className={styles.youtube}>
                            <button aria-label="search" onClick={() => {
                                setShowVideo(true)
                            }}>
                                <svg width="20" height="15" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M0.7549 0.780703C0.4216 0.583187 0 0.82342 0 1.21084V12.7892C0 13.1766 0.4216 13.4169 0.7549 13.2194L10.5242 7.43019C10.8509 7.23652 10.8509 6.76352 10.5242 6.56985L0.7549 0.780703Z" fill="white" fillOpacity="0.75" />
                                </svg>
                            </button>
                        </div>
                    </>}
            </div>
        </div>
    )
}

export default HeroSection