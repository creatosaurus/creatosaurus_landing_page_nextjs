import React from "react";
import privacyStyle from "../styles/privacy.module.css";
import Footer from "../Components/LandingPageComponents/Footer";
import NavigationBar from "../Components/LandingPageComponents/NavigationBar";
import Link from "next/link";

const Refund = () => {
    return (
        <>
            <NavigationBar />
            <div className={privacyStyle.container}>
                <h1>Refund Policy</h1>
                <h2>October 30, 2024</h2>
                <div className={privacyStyle.points}>
                    <h1>Please read the following Refund policy carefully</h1>
                    <p style={{ paddingTop: 10 }}>
                        When opting for premium features within Creatosaurus products and services,
                        users are required to pay the corresponding fees, which are displayed prior
                        to payment for review and acceptance. All fees are denominated in U.S.
                        Dollars and Indian Rupees. Users can request a full refund within the first
                        14 days of purchase, with only transaction and payment gateway fees deducted.
                        No refunds will be issued after the 15-day period. To request a refund within
                        this 14-day window, please reach out through our contact sales form. Fee structures
                        vary based on the selected plan and accommodate individual users, teams, and organizations.
                        For inquiries or further clarification, please contact our customer <span className="underline text-[#0000EE]" style={{ fontWeight: 400 }}><Link href="/contact">support team.</Link></span>
                        <br />
                        <br />
                        Check the full <span className="underline text-[#0000EE]" style={{ fontWeight: 400 }}><Link href="/terms">terms</Link></span> of service.
                    </p>

                    <h1>Contact</h1>
                    <p style={{ paddingTop: 10 }}>
                        Please contact us at contact@creatosaurus.io with any questions
                        regarding this Agreement.
                    </p>
                </div>
            </div>
            <Footer />
        </>
    );
};

export default Refund;
