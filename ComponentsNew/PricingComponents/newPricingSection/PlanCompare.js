import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router';

const PlanCompare = ({ isIndian, selectPlan, checkLogin }) => {
    const data = [
        {
            title: "Plan Overview",
            data: [
                { info: ["Workspace", "1", "2", "3", "10"] },
                { info: ["Brand kits", "2", "25", "100", "1000"] },
                { info: ["Social accounts", "3", "5", "15", "50"] },
                { info: ["AI Word credits", "5k", "30k", "100k", "300k"] },
                { info: ["AI Image credits", "20", "100", "250", "750"] },
                { info: ["Cloud storage", "1 GB", "10 GB", "50 GB", "250 GB"] },
                { info: ["Team members", "5", "Unlimited", "Unlimited", "Unlimited"] },
                { info: ["Mobile App", "yes", "yes", "yes", "yes"] },
                { info: ["Additonal social accounts", "no", "Add on", "Add on", "Add on"] },
            ]
        },
        {
            title: "Content Creation",
            data: [
                { info: ["Stock photos, graphics, fonts, videos, and audio", "3+ million", "100+ million", "100+ million", "100+ million"] },
                { info: ["Templates", "Free", "Premium", "Premium", "Premium"] },
                { info: ["Design editor", "yes", "yes", "yes", "yes"] },
                { info: ["Document editor", "yes", "yes", "yes", "yes"] },
                { info: ["Create content with custom dimensions", "yes", "yes", "yes", "yes"] },
                { info: ["Customisable templates", "no", "yes", "yes", "yes"] },
                { info: ["Save designs as templates", "no", "yes", "yes", "yes"] },
                { info: ["Advance editing capabilities", "no", "yes", "yes", "yes"] },
                { info: ["Custom font upload", "no", "yes", "yes", "yes"] },
                { info: ["Export videos as MP4, GIF, MOV", "yes", "yes", "yes", "yes"] },
                { info: ["Export docs as HTML, PDF, Markdown, Docx", "yes", "yes", "yes", "yes"] },
                { info: ["Export designs as PDF, JPG, PNG, JPEG, WEBP", "yes", "yes", "yes", "yes"] },
                { info: ["Transparent PNG export", "no", "yes", "yes", "yes"] },
                { info: ["Export in high resolution", "no", "yes", "yes", "yes"] },
            ]
        }, {
            title: "AI Tools",
            data: [
                { info: ["Access to 15+ AI models", "no", "yes", "yes", "yes"] },
                { info: ["AI Image generation", "yes", "yes", "yes", "yes"] },
                { info: ["AI Image outpainting", "yes", "yes", "yes", "yes"] },
                { info: ["AI Image inpainting", "yes", "yes", "yes", "yes"] },
                { info: ["AI Object remover", "yes", "yes", "yes", "yes"] },
                { info: ["AI Upscaler", "yes", "yes", "yes", "yes"] },
                { info: ["AI Background remover", "yes", "yes", "yes", "yes"] },
                { info: ["AI Chat", "yes", "yes", "yes", "yes"] },
                { info: ["AI Text Generation", "yes", "yes", "yes", "yes"] },
                { info: ["AI Translate", "yes", "yes", "yes", "yes"] },

            ]
        }, {
            title: "Planning and Scheduling",
            data: [
                { info: ["Multiple profile scheduling", "yes", "yes", "yes", "yes"] },
                { info: ["Customise each", "no", "yes", "yes", "yes"] },
                { info: ["Post  scheduling queue", "100", "Unlimited", "Unlimited", "Unlimited"] },
                { info: ["Draft and reminders", "yes", "yes", "yes", "yes"] },
                { info: ["Customisation for each channel", "yes", "yes", "yes", "yes"] },
                { info: ["Post preview", "yes", "yes", "yes", "yes"] },
                { info: ["Calendar view", "yes", "yes", "yes", "yes"] },
                { info: ["Kanban view", "no", "yes", "yes", "yes"] },
            ]
        }, {
            title: "Analytics, Insights & Reporting",
            data: [
                { info: ["Performance overview", "no", "yes", "yes", "yes"] },
                { info: ["Individual post analytics", "no", "yes", "yes", "yes"] },
                { info: ["Content insights", "no", "yes", "yes", "yes"] },
                { info: ["Audience demographics", "no", "yes", "yes", "yes"] },
                { info: ["Exportable reports", "no", "yes", "yes", "yes"] },
                { info: ["Branded reports", "no", "yes", "yes", "yes"] },
                { info: ["Competitor analytics", "no", "no", "yes", "yes"] },
                { info: ["Best time to post", "no", "yes", "yes", "yes"] },
            ]
        }, {
            title: "Engagement and Inbox",
            data: [
                { info: ["All in one centralized inbox", "no", "yes", "yes", "yes"] },
                { info: ["Comments", "no", "yes", "yes", "yes"] },
                { info: ["Mentions", "no", "yes", "yes", "yes"] },
                { info: ["Direct messages", "no", "yes", "yes", "yes"] },
                { info: ["Tags", "no", "yes", "yes", "yes"] },
                { info: ["Sentiment analysis", "no", "yes", "yes", "yes"] },
                { info: ["AI Replies", "no", "yes", "yes", "yes"] },
                { info: ["Profile overviews", "no", "yes", "yes", "yes"] },
            ]
        }, {
            title: "Team Management and Collaboration",
            data: [
                { info: ["Invite additional users", "yes", "yes", "yes", "yes"] },
                { info: ["Access and permission", "yes", "yes", "yes", "yes"] }
            ]
        }, {
            title: "Social Channels",
            data: [
                { info: ["Instagram (Post, Story and Reel)", "yes", "yes", "yes", "yes"] },
                { info: ["Facebook (Post, Story and Reel)", "yes", "yes", "yes", "yes"] },
                { info: ["X (formerly Twitter)", "yes", "yes", "yes", "yes"] },
                { info: ["LinkedIn (Personal and Company)", "yes", "yes", "yes", "yes"] },
                { info: ["Pinterest", "yes", "yes", "yes", "yes"] },
                { info: ["Tumblr", "yes", "yes", "yes", "yes"] },
                { info: ["YouTube (Long form and Shorts)", "yes", "yes", "yes", "yes"] },
            ]
        }, {
            title: "Quotes Library",
            data: [
                { info: ["Access to quotes library", "100K+", "1 Million+", "1 Million+", "1 Million+"] },
            ]
        }, {
            title: "Hashtag Analysis and Management",
            data: [
                { info: ["Hashtag search", "no", "no", "yes", "yes"] },
                { info: ["Hashtag analysis", "no", "no", "yes", "yes"] },
                { info: ["Hashtag management", "no", "yes", "yes", "yes"] },
                { info: ["AI Hashtag generation", "yes", "yes", "yes", "yes"] },
            ]
        }, {
            title: "App Integrations",
            data: [
                { info: ["Access to more than 15+ 3rd party apps", "no", "no", "yes", "yes"] }
            ]
        }, {
            title: "Support",
            data: [
                { info: ["Tutorials, knowledge base and self-help", "yes", "yes", "yes", "yes"] },
                { info: ["Support tickets and live chat", "yes", "yes", "yes", "yes"] },
                { info: ["Priority handling", "no", "no", "yes", "yes"] },
                { info: ["Dedicated support case management", "no", "no", "no", "yes"] },
            ]
        }
    ]

    const plans = [{
        color: "#F8F8F9",
        borderColor: "#E8E8E8",
        tag: "For individual",
        showPremium: false,
        planName: "Creatosaurus Free",
        info: "Create anything & turn your ideas into reality at no cost",
        month: 0,
        year: 0,
        buttonName: "Get Started",
        buttonColor: "#808080",
        planHead: "Features you'll love:",
        showTrial: false,
        hideMonthYear: true,
        features: ["1  Workspace", "2 Brand Kits", "3 Social accounts", "5k AI words credits", "20 AI image credits", "1 GB Cloud storage", "Easy drag-and-drop editor", "Plan and schedule social content", "AI-generated writing and images", "Pro design, video & document editor", "1000+ content types (social posts, videos, docs and more)", "Professionally designed templates", "3M+ stock photos and graphics"]
    }, {
        id: "6247085137b553074fb6b25a",
        color: "#E6F2FF80",
        borderColor: "#CCE4FF",
        tag: "For creator",
        showPremium: false,
        planName: "Creatosaurus Pro",
        info: "Access premium templates, robust content tools and AI features",
        month: isIndian ? "1000" : "25",
        year: isIndian ? "10000" : "250",
        buttonName: "Get Creatosaurus Pro",
        buttonColor: "#0078FF",
        planHead: "Everything in Free, plus:",
        showTrial: false,
        features: ["2  Workspace", "5 Brand Kits", "5 Social accounts", "25k AI words credits", "250 AI image credits", "10 GB Cloud storage", "Unlimited team members", "All in one social inbox", "Social media analytics", "Unlimited schedule social content", "Quickly resize and translate designs", "Remove backgrounds in a click", "Boost creativity with 20+ AI tools", "Unlimited premium templates", "100M+ photos, videos, graphics, audio", "Online customer support"]
    }, {
        id: "6247081137b553074fb6b258",
        color: "#E6F2FF80",
        borderColor: "#0078FF",
        tag: "For startup",
        showPremium: true,
        planName: "Creatosaurus Teams",
        info: "Enhance collaboration, grow your brand & streamline your workflows.",
        month: isIndian ? "2000" : "50",
        year: isIndian ? "20000" : "500",
        buttonName: "Get Creatosaurus Teams",
        buttonColor: "#0078FF",
        planHead: "Everything in Pro, plus:",
        showTrial: false,
        features: ["3  Workspace", "10 Brand Kits", "15 Social accounts", "75k AI words credits", "750 AI image credits", "50 GB Cloud storage", "Reports and insights", "Competitor analysis", "Social media automations", "Hashtag analysis & management", "Ensure brand consistency with approvals", "Edit, comment, and collaborate in real time", "Generate on brand copy with AI", "Online customer support"]
    }, {
        id: "625fd4c1c7770407d20177fc",
        color: "#E6F2FF80",
        borderColor: "#CCE4FF",
        tag: "For agency",
        showPremium: false,
        planName: "Creatosaurus Agency",
        info: "Empower your clients with an all in one creative workplace solution",
        month: isIndian ? "4000" : "100",
        year: isIndian ? "40000" : "1000",
        buttonName: "Get Creatosaurus Agency",
        buttonColor: "#0078FF",
        planHead: "Everything in Teams, plus:",
        showTrial: false,
        features: ["10  Workspace", "100 Brand Kits", "50 Social accounts", "250k AI words credits", "2000 AI image credits", "250 GB Cloud storage", "Branded Reports", "Brand templates", "Individual approvals and permission by use case", "Org wide Brand Kits and Templates", "Admin Controls", "Scale your brand and centralise assets", "New app integrations", "Priority customer support"]
    }]

    const router = useRouter();
    const [openTabsIndex, setOpenTabsIndex] = useState([0])
    const [showCompareButton, setShowCompareButton] = useState(true)

    useEffect(() => {
        if (router.asPath.includes('#compare-plan-features')) {
            setShowCompareButton(false)
        }
    }, [router])


    const removeIndex = (index) => {
        let filterData = openTabsIndex.filter((data) => data !== index)
        setOpenTabsIndex(filterData)
    }

    const addIndex = (index) => {
        setOpenTabsIndex((prev) => [...prev, index])
    }

    const handleSelectPlan = (type) => {
        if (type === "Creatosaurus Free") return checkLogin()
        const planInfo = plans.find(data => data.planName === type)
        selectPlan(planInfo)
    }

    return (
        <div className="mt-[100px] hidden md:flex flex-col max-[960px]:px-[20px] min-[960px]:px-[80px]">
            <div className="h-[140px] grid grid-cols-5">
                <div className="text-[24px] font-semibold flex flex-col justify-center">
                    <span>Compare plan</span>
                    <span>and features</span>
                </div>

                <div className="bg-[#F8F8F9] py-[25px] px-[20px] rounded-tl-[15px] rounded-bl-[15px] border-r-[1px] border-[#DCDCDC]">
                    <span className="text-[20px] font-semibold">Free</span>
                    <button
                        onClick={() => handleSelectPlan('Creatosaurus Free')}
                        className="mt-[20px] rounded-[5px] text-[#808080] hover:text-white hover:bg-[#808080] border-[1px] border-[#808080] h-[40px] w-full text-[14px] font-semibold">Get Started</button>
                </div>

                <div className="bg-[#E6F2FF80] py-[25px] px-[20px] border-r-[1px] border-[#DCDCDC]">
                    <span className="text-[20px] font-semibold">Pro</span>
                    <button
                        onClick={() => handleSelectPlan('Creatosaurus Pro')}
                        className="mt-[20px] rounded-[5px] text-white bg-[#0078FF] hover:bg-[#006ce6] h-[40px] w-full text-[14px] font-semibold">Get Creatosaurus Pro</button>
                </div>

                <div className="bg-[#E6F2FF80] py-[25px] px-[20px] border-r-[1px] border-[#DCDCDC]">
                    <div className="flex justify-between items-center">
                        <span className="text-[20px] font-semibold">Teams</span>
                        <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0_3730_33049)">
                                <circle cx="12.5" cy="13" r="12.5" fill="#FFBF00" />
                                <path d="M11.8141 8.63914C11.1091 10.6132 10.3195 11.572 9.60043 11.6425C8.79672 11.7412 8.09172 11.3887 7.42902 10.5145C7.3884 10.4615 7.34099 10.4141 7.28801 10.3735C7.47859 10.1193 7.57779 9.80834 7.56954 9.49081C7.56129 9.17329 7.44607 8.86785 7.24254 8.62399C7.03902 8.38013 6.75912 8.21213 6.4482 8.1472C6.13727 8.08228 5.81353 8.12425 5.52943 8.2663C5.24534 8.40835 5.01752 8.64216 4.8829 8.92985C4.74828 9.21755 4.71474 9.54227 4.78772 9.8514C4.8607 10.1605 5.03591 10.436 5.28498 10.6331C5.53405 10.8302 5.84238 10.9375 6.16001 10.9375L6.17411 11.1067L7.33031 15.6751C7.44338 16.1325 7.70593 16.539 8.07631 16.8302C8.44669 17.1214 8.90369 17.2806 9.37483 17.2825H15.6353C16.1064 17.2806 16.5634 17.1214 16.9338 16.8302C17.3042 16.539 17.5667 16.1325 17.6798 15.6751L18.836 11.1067C18.847 11.051 18.8517 10.9942 18.8501 10.9375C19.1677 10.9375 19.476 10.8302 19.7251 10.6331C19.9742 10.436 20.1494 10.1605 20.2224 9.8514C20.2953 9.54227 20.2618 9.21755 20.1272 8.92985C19.9926 8.64216 19.7647 8.40835 19.4806 8.2663C19.1965 8.12425 18.8728 8.08228 18.5619 8.1472C18.251 8.21213 17.9711 8.38013 17.7675 8.62399C17.564 8.86785 17.4488 9.17329 17.4405 9.49081C17.4323 9.80834 17.5315 10.1193 17.7221 10.3735C17.67 10.4097 17.6226 10.4523 17.5811 10.5004C16.8902 11.3746 16.1711 11.7271 15.4097 11.6425C14.7047 11.572 13.9432 10.5991 13.1959 8.63914C13.4677 8.48637 13.6812 8.24782 13.803 7.96082C13.9248 7.67381 13.948 7.35453 13.8691 7.05291C13.7901 6.7513 13.6134 6.48436 13.3666 6.29385C13.1198 6.10334 12.8168 6 12.505 6C12.1933 6 11.8903 6.10334 11.6435 6.29385C11.3967 6.48436 11.22 6.7513 11.141 7.05291C11.062 7.35453 11.0853 7.67381 11.2071 7.96082C11.3289 8.24782 11.5424 8.48637 11.8141 8.63914ZM16.7351 18.34C16.922 18.34 17.1014 18.4143 17.2336 18.5465C17.3658 18.6787 17.4401 18.858 17.4401 19.045C17.4401 19.232 17.3658 19.4113 17.2336 19.5435C17.1014 19.6757 16.922 19.75 16.7351 19.75H8.27502C8.08804 19.75 7.90872 19.6757 7.77651 19.5435C7.64429 19.4113 7.57002 19.232 7.57002 19.045C7.57002 18.858 7.64429 18.6787 7.77651 18.5465C7.90872 18.4143 8.08804 18.34 8.27502 18.34H16.7351Z" fill="white" />
                            </g>
                            <defs>
                                <clipPath id="clip0_3730_33049">
                                    <rect width="25" height="25" fill="white" transform="translate(0 0.5)" />
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                    <button
                        onClick={() => handleSelectPlan('Creatosaurus Teams')}
                        className="mt-[20px] rounded-[5px] text-white bg-[#0078FF] hover:bg-[#006ce6] h-[40px] w-full text-[14px] font-semibold">Get Creatosaurus Teams</button>
                </div>

                <div className="bg-[#E6F2FF80] py-[25px] px-[20px] rounded-tr-[15px] rounded-br-[15px]">
                    <span className="text-[20px] font-semibold">Agency</span>
                    <button
                        onClick={() => handleSelectPlan('Creatosaurus Agency')}
                        className="mt-[20px] rounded-[5px] text-white bg-[#0078FF] hover:bg-[#006ce6] h-[40px] w-full text-[14px] font-semibold">Get Creatosaurus Agency</button>
                </div>
            </div>

            <div id="compare-plan-features" className={`relative ${showCompareButton ? "h-[260px] overflow-hidden" : ""}`}>
                <div className={`${showCompareButton} ? "h-[250px] overflow-hidden" : ""`}>
                    {
                        data.map((type, index) => {
                            return <div className="mt-[20px]" key={type.title}>
                                {
                                    openTabsIndex.includes(index) ?
                                        <div onClick={() => removeIndex(index)} className="bg-[#DCDCDC80] h-[54px] flex justify-between items-center px-[20px] rounded-tl-[15px] rounded-tr-[15px] border-l-[1px] border-t-[1px] border-r-[1px] border-[#DCDCDC] cursor-pointer">
                                            <span className="text-[16px] font-semibold">{type.title}</span>
                                            <svg className='cursor-pointer' width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.90523 19.0479L16.0005 12.9526L22.0957 19.0479" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                        :
                                        <div onClick={() => addIndex(index)} className="bg-[#DCDCDC80] h-[54px] flex justify-between items-center px-[20px] rounded-[15px] border-[1px] border-[#DCDCDC] cursor-pointer">
                                            <span className="text-[16px] font-semibold">{type.title}</span>
                                            <svg className='cursor-pointer' width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M22.0948 12.9521L15.9995 19.0474L9.9043 12.9521" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                }


                                {
                                    openTabsIndex.includes(index) && <div className="grid grid-cols-5 border-l-[1px] border-b-[1px] border-r-[1px] border-[#DCDCDC] rounded-bl-[15px] rounded-br-[15px]">
                                        {
                                            type.data.map((info, i) => {
                                                return info.info.map((text, i2) => {
                                                    return <div key={index + i + i2}>
                                                        {
                                                            i2 === 0 ? <div className="px-[20px] flex flex-col justify-center border-r-[1px] border-[#DCDCDC] text-[14px] font-medium">
                                                                <span className="h-[44px] flex items-center">{text}</span>
                                                            </div> :
                                                                <div className="flex h-[44px] flex-col justify-center border-r-[1px] border-[#DCDCDC] text-[14px] text-[#333333]">
                                                                    {
                                                                        text === "yes" ?
                                                                            <svg className='m-auto size-[20px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path fillRule="evenodd" clipRule="evenodd" d="M12.5007 1.30225C6.31621 1.30225 1.30273 6.31573 1.30273 12.5002C1.30273 18.6846 6.31621 23.6981 12.5007 23.6981C18.6851 23.6981 23.6986 18.6846 23.6986 12.5002C23.6986 6.31573 18.6851 1.30225 12.5007 1.30225ZM7.84486 12.4686C7.53977 12.1635 7.04511 12.1635 6.74001 12.4686C6.43491 12.7737 6.43491 13.2683 6.74001 13.5734L9.865 16.6984C10.1701 17.0035 10.6648 17.0035 10.9698 16.6984L18.2615 9.40676C18.5666 9.10166 18.5666 8.607 18.2615 8.3019C17.9564 7.99681 17.4618 7.99681 17.1567 8.3019L10.4174 15.0411L7.84486 12.4686Z" fill="#009B77" />
                                                                            </svg>
                                                                            :
                                                                            text === "no" ?
                                                                                <svg className='m-auto size-[22px]' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                    <path d="M7.03906 17.9612L12.5001 12.5001M12.5001 12.5001L17.9612 7.03906M12.5001 12.5001L7.03906 7.03906M12.5001 12.5001L17.9612 17.9612" stroke="#FF4359" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                                                </svg>
                                                                                :
                                                                                <span className="flex items-center justify-center px-[20px]">{text}</span>
                                                                    }
                                                                </div>
                                                        }
                                                    </div>
                                                })
                                            })
                                        }
                                    </div>
                                }
                            </div>
                        })
                    }
                </div>
                {showCompareButton && <div className='absolute bottom-0 bg-white w-full h-[100px] z-40 blur-[30px]' />}
            </div>

            <button
                onClick={() => setShowCompareButton((prev) => !prev)}
                className="m-auto mt-[40px] w-[260px] hover:bg-[#0000000D] h-[40px] rounded-[5px] border-[1px] border-[#c4c4c4]">
                {showCompareButton ? "Compare features" : "Close features"}
            </button>
        </div>
    )
}

export default PlanCompare