import React, { useState } from 'react';
import style from '../styles/BlogNavigationBar.module.css';
import Logo from '../public/Assets/Logo.svg';
import Link from 'next/link';
import Image from 'next/image';
import NavigationAuth from "../Components/NavigationAuth/NavigationAuth"

const BlogNavigationBar = (props) => {
    const [showLogin, setshowLogin] = useState(false)

    return (
        <React.Fragment>
            {showLogin ? <NavigationAuth close={() => setshowLogin(false)} /> : null}
            <div className={style.navMainContainer}>
                <div className={style.navigationBar}>
                    <div className={style.row}>
                        <Link prefetch={false} href={props.slug ? props.slug : "/blog"}>
                            <a>
                                <div className={style.logo}>
                                    <Image src={Logo} alt="creatosaurus logo" />
                                </div>
                            </a>
                        </Link>

                        <Link prefetch={false} href={props.slug ? props.slug : "/blog"} passhref>
                            <a style={{ color: 'black', textDecoration: 'none' }}><span className={style.big}> CREATOSAURUS <span className={style.small}>| {props.title ? props.title : "Blog"}</span> </span></a>
                        </Link>
                    </div>

                    <div className={style.searchContainer}>
                        <div className={style.loginButtonContainer}>
                            {
                                props.inSideBlog ?
                                    null :
                                    <input style={{ display: 'none' }} type="search" onChange={props.handleOnChange} placeholder="Search" />
                            }

                            <a className={style.bold} href="https://calendly.com/malavwarke/creatosaurus">Book Demo</a>
                            <button className={style.loginButton} onClick={() => setshowLogin(true)}>Log in</button>
                            <button className={style.signupButton} onClick={() => setshowLogin(true)}>Sign up</button>
                        </div>
                    </div>

                </div>

                <div style={{ width: `${props.progress ? props.progress : 0}%`, height: "5px", background: "#ff4359", position: "sticky", zIndex: 1 }} />
            </div>
        </React.Fragment>
    )
}

export default BlogNavigationBar