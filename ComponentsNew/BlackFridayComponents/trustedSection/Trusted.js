import React from 'react'
import styles from './Trusted.module.css'
import Image from 'next/image'
import motorolaSolutions from "./assets/motorolaSolutions.svg"
import decathlon from "./assets/decathlon.svg"
import theHealthyBinge from "./assets/theHealthyBinge.svg"
import modernGamer from "./assets/modernGamer.svg"
import blueBayTravel from "./assets/blueBayTravel.svg"
import collectivePower from "./assets/collectivePower.svg"
import decim from "./assets/decim.svg"
import carCapital from "./assets/carCapital.svg"
import billyJ from "./assets/billyJ.svg"
import sAssembly from "./assets/sAssembly.svg"
import yesDevs from "./assets/yesDevs.svg"
import oneTen from "./assets/oneTen.svg"
import sooma from "./assets/sooma.svg"
import dittDesign from "./assets/dittDesign.svg"
import tenBound from "./assets/tenbound.svg"
import stateOfMind from "./assets/stateOfMind.svg"
import prognoix from "./assets/prognoix.svg"
import baseNote from "./assets/basenote.svg"

const Trusted = () => {
    return (
        <div className={styles.trustedContainer}>
            <h2>Trusted by +30K users from</h2>
            <div className={styles.creator}>
                <Image src={carCapital} priority={true} alt='car capital' />
                <Image src={theHealthyBinge} priority={true} alt='the healthy binge' />
                <Image src={billyJ} priority={true} alt='billyj' />
                <Image src={motorolaSolutions} priority={true} alt='motorola solutions' />
                <Image src={decathlon} priority={true} alt='decathlon' />
                <Image src={sAssembly} priority={true} alt='sAssembly' />
                <Image src={blueBayTravel} priority={true} alt='blue bay travel' />
                <Image src={decim} priority={true} alt='decim' />
                <Image src={yesDevs} priority={true} alt='yesDevs' />
                <Image src={oneTen} priority={true} alt='oneTen' />
            </div>

            <div className={styles.creator}>
                <Image src={sooma} priority={true} alt='sooma' />
                <Image src={dittDesign} priority={true} alt='dittDesign' />
                <Image src={tenBound} priority={true} alt='ten bound' />
                <Image src={stateOfMind} priority={true} alt='state of mind' />
                <Image src={prognoix} priority={true} alt='prognoix' />
                <Image src={modernGamer} priority={true} alt='modern gamer' />
                <Image src={baseNote} priority={true} alt='basenote' />
                <Image src={collectivePower} priority={true} alt='collective power' />
            </div>


            <div className={styles.creator1}>
                <Image src={theHealthyBinge} priority={true} alt='the healthy binge' />
                <Image src={billyJ} priority={true} alt='billyj' />
                <Image src={motorolaSolutions} priority={true} alt='motorola solutions' />
                <Image src={decathlon} priority={true} alt='decathlon' />
                <Image src={sAssembly} priority={true} alt='sAssembly' />
                <Image src={blueBayTravel} priority={true} alt='blue bay travel' />
                <Image src={decim} priority={true} alt='decim' />
                <Image src={yesDevs} priority={true} alt='yesDevs' />
                <Image src={oneTen} priority={true} alt='oneTen' />
            </div>
        </div>
    )
}

export default Trusted