import React from 'react'
import axios from 'axios'
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import constant from '../../constant'

export async function getServerSideProps(context) {
    const transactionId = context.params.transactionId
    const transaction = await axios.get(`${constant.url}invoice/${transactionId}`)

    if (!transaction) {
        return { notFound: true }
    }

    return {
        props: { transaction: transaction.data }
    }
}

const Invoice = ({ transaction }) => {
    const { userMetaData, planId, subscriptionDetails, paymentMetaData } = transaction;
    const { companyDetails, userDetails } = userMetaData;

    const isUserFromIndia = () => {
        if (companyDetails.name === "") {
            if (userDetails.country === "India") return true
        } else {
            if (companyDetails.country === "India") return true
        }

        return false
    };

    const isUserFromMaharashtra = () => {
        if (companyDetails.name === "") {
            if (userDetails.state === "Maharashtra") return true
        } else {
            if (companyDetails.state === "Maharashtra") return true
        }

        return false
    };

    const billingName = () => {
        if (companyDetails.name === "") {
            return `${userDetails.firstName}  ${userDetails.lastName}`
        } else {
            return companyDetails.name
        }
    }

    const billingAddress = () => {
        if (companyDetails.name === "") {
            return `${userDetails.state}, ${userDetails.country}, ${userDetails.postalCode}`
        } else {
            return `${companyDetails.state}, ${companyDetails.country}, ${companyDetails.postalCode}`
        }
    }

    const supplyAddress = () => {
        if (companyDetails.name === "") {
            return `${userDetails.state}, ${userDetails.country}`
        } else {
            return `${companyDetails.state}, ${companyDetails.country}`
        }
    }

    const checkGSTNumber = () => {
        if (companyDetails.name === "") return null
        if (companyDetails.country !== "India") return null
        return `GST Number - ${companyDetails.gst}`
    }

    const formatDate = (value = new Date()) => {
        const date = new Date(value)
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
        const year = date.getFullYear();

        return `${day}/${month}/${year}`;
    }

    const formatDateTime = (dateString) => {
        const date = new Date(dateString);

        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
        const year = date.getFullYear();

        let hours = date.getHours();
        const minutes = String(date.getMinutes()).padStart(2, '0');
        const ampm = hours >= 12 ? 'PM' : 'AM';

        // Convert to 12-hour format
        hours = hours % 12;
        hours = hours ? hours : 12; // The hour '0' should be '12'

        return `${day}/${month}/${year} ${hours}:${minutes} ${ampm}`;
    };

    const checkTax = (tax = 0) => {
        let taxAmount = (transaction.finalAmount * tax) / 100;
        return taxAmount
    }

    const checkPriceAfterTax = () => {
        if (isUserFromIndia()) {
            const tax = 18
            let taxAmount = (transaction.finalAmount * tax) / 100
            return transaction.finalAmount - taxAmount;
        } else {
            return transaction.finalAmount
        }
    }

    const checkCurrency = () => {
        return transaction.currency === "USD" ? "$" : "₹"
    }

    const downloadPDF = async () => {
        const invoiceElement = document.getElementById('invoice-content');

        // Configure html2canvas with higher scale for better rendering
        const canvas = await html2canvas(invoiceElement, {
            scale: 2, // Increase scale for better quality
            useCORS: true, // Enable CORS to handle external styles
        });

        const imgData = canvas.toDataURL('image/png', 0.5);
        const pdf = new jsPDF('p', 'mm', 'a4');

        const pdfWidth = pdf.internal.pageSize.getWidth(); // Width of A4 in mm
        const pdfHeight = (canvas.height * pdfWidth) / canvas.width; // Maintain aspect ratio

        // Add image to PDF
        pdf.addImage(imgData, 'PNG', 0, 0, pdfWidth, pdfHeight);
        pdf.save(`Invoice_${transaction.invoiceId}.pdf`);
    };

    return (
        <div className="max-w-[900px] mx-auto relative">
            <button onClick={downloadPDF} className="absolute right-[20px] top-[20px] sm:top-[55px] sm:right-[50px] px-[10px] py-[5px] text-[14px] bg-blue-600 text-white rounded hover:bg-blue-700"  >
                Download Invoice
            </button>

            {/* Header Section */}
            <div style={{ letterSpacing: 0.01 }} id="invoice-content" className='p-[20px] sm:p-[50px]'>
                <div className="mb-[10px]">
                    <svg className='h-[40px]' xmlns="http://www.w3.org/2000/svg" width="104" height="50" viewBox="0 0 104 50" fill="none">
                        <g clipPath="url(#clip0_633_816)">
                            <path d="M70.0879 50.0004L65.5217 47.1758L64.7978 50.0004H70.0879Z" fill="#FF4359" />
                            <path d="M76.8369 46.4403L70.7903 43.6592V46.5491L76.8369 46.4403Z" fill="#FF4359" />
                            <path d="M64.2159 49.7493L58.8496 44.7096L61.6361 39.8604L65.0322 46.6036L64.2159 49.7493Z" fill="#FF4359" />
                            <path d="M54.4414 11.3418L42.1632 25.0351L6.56396 24.7738L54.4414 11.3418Z" fill="#FF4359" />
                            <path d="M84.8755 24.8936L84.9136 31.5334H77.8275L84.8755 24.8936Z" fill="#FF4359" />
                            <path d="M80.7335 35.2892L84.8589 32.1543H80.8423L80.7335 35.2892Z" fill="#FF4359" />
                            <path d="M58.4845 44.1061L67.1489 29.0195L55.7197 34.609L58.4845 44.1061Z" fill="#FF4359" />
                            <path d="M13.1431 22.2865L6.10593 24.2621L-0.000533169 20.6592L13.1431 22.2865Z" fill="#FF4359" />
                            <path d="M97.6104 19.8651L90.1487 17.3997L89.0983 10.9775L99.4336 19.0651L97.6104 19.8651Z" fill="#FF4359" />
                            <path d="M70.1753 46.5496L62.9096 37.6458L66.0662 32.1543H70.1753V46.5496Z" fill="#FF4359" />
                            <path d="M67.0186 28.399L55.1594 34.1952L42.9247 25.1063L55.1376 11.4893L64.0959 14.9289L67.0186 28.399Z" fill="#FF4359" />
                            <path d="M103.87 8.64828L91.7547 12.273L89.0117 10.1232L87.7381 2.62891H100.789L103.87 8.64828Z" fill="#FF4359" />
                            <path d="M87.1176 2.62891L89.5068 17.2855L84.8862 24.0396L76.7225 16.1208L80.3689 9.11633L87.1176 2.62891Z" fill="#FF4359" />
                            <path d="M76.9293 31.5336H66.4199L67.8023 29.1281L64.7763 15.1572L75.7592 16.0389L84.4399 24.4584L76.9293 31.5336Z" fill="#FF4359" />
                            <path d="M94.3671 2.00827H97.3931L95.8039 0L94.3671 2.00827Z" fill="#FF4359" />
                        </g>
                        <defs>
                            <clipPath id="clip0_633_816">
                                <rect width="103.87" height="50" fill="white" transform="matrix(-1 0 0 1 103.87 0)" />
                            </clipPath>
                        </defs>
                    </svg>

                    <h1 className="text-[20px] font-semibold mt-[15px]">Creatosaurus Private Limited</h1>
                    <p className="text-[14px]  mt-[5px]">
                        Ideas to Impacts Hub, 6th Floor <br />
                        Sr. N. 284/2A, Behind Vijay Sales <br />
                        Baner, Pune, Maharashtra, India, 411045 <br />
                        GST Number - 27AAKCC0696P1ZR <br />
                        {!isUserFromIndia() && "LUT Number - AD271123001482O"}
                    </p>
                </div>

                {/* Billed To Section */}
                <div className="flex justify-between items-start mb-5">
                    <div>
                        <h2 className="font-semibold">Billed To:</h2>
                        <p className="text-[14px]">
                            <span className='font-medium'>{billingName()}</span> <br />
                            {billingAddress()} <br />
                            {checkGSTNumber()}
                        </p>
                    </div>

                    {/* Right Section: Invoice Info */}
                    <div className="text-right text-[14px] font-medium">
                        <p>Invoice No: {transaction.invoiceId}</p>
                        <p>Date: {formatDate(transaction.createdAt)}</p>
                    </div>
                </div>


                {/* Invoice Table */}
                <table className="w-full text-left border border-gray-300 mb-4">
                    <thead className='text-[14px] font-medium text-center'>
                        <tr>
                            <th className="p-[10px] border-r border-gray-300">Description</th>
                            <th className="p-[10px] border-r border-gray-300">Service Period</th>
                            <th className="p-[10px] border-r border-gray-300">Rate</th>
                            <th className="p-[10px]">Amount</th>
                        </tr>
                    </thead>
                    <tbody className="border border-gray-300 text-[14px] text-center ">
                        <tr>
                            <td className="p-[10px] border-r border-gray-300">{planId.planName}</td>
                            <td className="p-[10px] border-r border-gray-300">{formatDate(subscriptionDetails.subscriptionStartDate)} - {formatDate(subscriptionDetails.subscriptionEndDate)}</td>
                            <td className="p-[10px] border-r border-gray-300"></td>
                            <td className="p-[10px]">{checkCurrency()}{checkPriceAfterTax()}</td>
                        </tr>

                        {
                            isUserFromIndia() && <>
                                {
                                    isUserFromMaharashtra() ?
                                        <>
                                            <tr>
                                                <td className="p-[10px] border-r border-gray-300">CGST</td>
                                                <td className="p-[10px] border-r border-gray-300"></td>
                                                <td className="p-[10px] border-r border-gray-300">9%</td>
                                                <td className="p-[10px]">{checkCurrency()}{checkTax(9)}</td>
                                            </tr>

                                            <tr>
                                                <td className="p-[10px] border-r border-gray-300">SGST</td>
                                                <td className="p-[10px] border-r border-gray-300"></td>
                                                <td className="p-[10px] border-r border-gray-300">9%</td>
                                                <td className="p-[10px]">{checkCurrency()}{checkTax(9)}</td>
                                            </tr>
                                        </>
                                        :
                                        <>
                                            <tr>
                                                <td className="p-[10px] border-r border-gray-300">IGST</td>
                                                <td className="p-[10px] border-r border-gray-300"></td>
                                                <td className="p-[10px] border-r border-gray-300">18%</td>
                                                <td className="p-[10px]">{checkCurrency()}{checkTax(18)}</td>
                                            </tr>
                                        </>
                                }
                            </>
                        }
                    </tbody>
                    <tfoot className='text-[14px]'>
                        <tr>
                            <td colSpan="3" className="p-[10px] text-right border-r border-gray-300">Sub Total</td>
                            <td className="p-[10px] text-center">{checkCurrency()}{checkPriceAfterTax()}</td>
                        </tr>
                        {
                            isUserFromIndia() &&
                            <tr>
                                <td colSpan="3" className="p-[10px] text-right border-r border-gray-300">Tax Total</td>
                                <td className="p-[10px] text-center">{checkCurrency()}{checkTax(18)}</td>
                            </tr>
                        }
                        <tr>
                            <td colSpan="3" className="p-[10px] text-right font-medium border-r border-gray-300">Total</td>
                            <td className="p-[10px] font-semibold text-center">{checkCurrency()}{transaction.finalAmount}</td>
                        </tr>
                    </tfoot>
                </table>

                {/* Payment Details */}
                <div>
                    <p className="text-[14px] ">
                        Payment Platform: <span className='capitalize'>{transaction.paymentProvider}</span> <br />
                        Payment Method: {transaction?.paymentMethod} <br />
                        Payment Date: {formatDateTime(transaction.createdAt)}<br />
                        Payment Reference ID: {transaction.paymentProvider === "stripe" ? paymentMetaData.payment_intent : paymentMetaData.paymentId}
                    </p>
                </div>

                {/* Footer */}
                <div className="text-[14px]  mt-[10px]">
                    <p>Place of Supply: {supplyAddress()}</p>
                    <p>HSN Code: 998311</p>
                    <p className="mt-2">{!isUserFromIndia() && `Supply of Goods/Services meant for export under LUT - AD271123001482O`}</p>
                    <p className="mt-2"> This is a system-generated invoice, no signature, and stamp required.</p>
                    <p className="mt-2">Email: contact@creatosaurus.io</p>
                    <p>Website: creatosaurus.io</p>
                    <p>Terms: creatosaurus.io/terms</p>
                </div>
            </div>
        </div>
    );
}

export default Invoice;