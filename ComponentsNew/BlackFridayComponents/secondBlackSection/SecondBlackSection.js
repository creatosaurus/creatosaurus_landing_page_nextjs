import React, { useState } from 'react'
import styles from './SecondBlackSection.module.css'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth'

const SecondBlackSection = () => {
    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <NavigationAuth close={() => setShowLogin(false)} /> : null}
            <div className={styles.secondBlackSectionContainer}>
                <p>You focus on telling stories, we do everything else.</p>
                <button onClick={() => setShowLogin(true)} style={{ cursor: 'pointer' }}>Get Started—It&apos;s Free</button>
            </div>
        </>
    )
}

export default SecondBlackSection