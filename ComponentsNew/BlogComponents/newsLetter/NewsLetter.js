import React from 'react'
import Ad from '../../AdComponents/Ad'

const NewsLetter = ({ isVisible, setEmail, email, loading, addUserToNewsLetter, shareOnLinkedin, shareOnTwitter, shareOnFacebook, setShowLogin }) => {
    return (
        <>
            {/* Author and newsletter container */}
            {
                isVisible &&
                <div className='w-[20%] right-[3%] fixed pt-[100px] pb-[10px] flex flex-col overflow-scroll h-[100vh]' id='blogInfoContainer'>
                    <div className='flex flex-col'>
                        <h2 className='text-[14px] font-semibold'>Newsletter</h2>
                        <div className='flex flex-col gap-[10px] mt-[10px]'>
                            <input
                                className='border-[1px] border-[rgba(255,67,89,0.5)] p-[10px] h-[35px] rounded-[5px] text-[12px]'
                                type="text"
                                value={email}
                                placeholder='Enter Your Email'
                                onChange={(e) => setEmail(e.target.value)} />

                            <button className='bg-[#ff4359] h-[30px] text-white font-medium rounded-[5px] text-[12px]' onClick={addUserToNewsLetter}>
                                {loading ? <span className='loader' /> : "SUBSCRIBE"}
                            </button>
                        </div>
                    </div>

                    <div className="flex flex-col gap-[10px] mt-[15px]">
                        <h2 className='text-[14px] font-semibold'>Share This Article</h2>
                        <div className='flex gap-x-[15px]'>
                            <button onClick={shareOnTwitter} className='bg-[#f8f8f9] size-[30px] rounded-full flex justify-center items-center group hover:bg-[#19a6ff]'>
                                <svg width='21' height='22' viewBox='0 0 21 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <path className='group-hover:fill-white' d='M17.1789 7.84791C17.1903 8.00103 17.1903 8.15328 17.1903 8.30553C17.1903 12.9649 13.6439 18.3339 7.16275 18.3339C5.166 18.3339 3.311 17.7555 1.75 16.751C2.0335 16.7834 2.3065 16.7948 2.60138 16.7948C4.249 16.7948 5.76538 16.2383 6.97725 15.2889C5.42762 15.2565 4.12912 14.2415 3.68112 12.845C3.899 12.8774 4.11775 12.8993 4.347 12.8993C4.66288 12.8993 4.9805 12.8555 5.27538 12.7794C3.65925 12.4522 2.44913 11.0338 2.44913 9.32053V9.27678C2.919 9.53841 3.46412 9.70203 4.04163 9.72391C3.09225 9.09216 2.47013 8.01153 2.47013 6.78916C2.47013 6.13466 2.64425 5.53441 2.94963 5.01116C4.68475 7.14878 7.29313 8.54616 10.2174 8.69928C10.1631 8.43678 10.1299 8.16466 10.1299 7.89166C10.1299 5.94916 11.7014 4.36716 13.6544 4.36716C14.6694 4.36716 15.5855 4.79241 16.2295 5.48016C17.0258 5.32703 17.7888 5.03216 18.466 4.62878C18.2044 5.44691 17.647 6.13466 16.9164 6.57128C17.626 6.49428 18.3138 6.29828 18.9455 6.02528C18.466 6.72353 17.8666 7.34566 17.1789 7.84791Z' fill='black' />
                                </svg>
                            </button>

                            <button onClick={shareOnLinkedin} className='bg-[#f8f8f9] size-[30px] rounded-full flex justify-center items-center group hover:bg-[#0177B5]'>
                                <svg width='19' height='20' viewBox='0 0 19 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <path className='group-hover:fill-white' d='M3.94506 6.04813C4.90171 6.04813 5.67722 5.27262 5.67722 4.31597C5.67722 3.35932 4.90171 2.5838 3.94506 2.5838C2.98841 2.5838 2.21289 3.35932 2.21289 4.31597C2.21289 5.27262 2.98841 6.04813 3.94506 6.04813Z' fill='black' />
                                    <path className='group-hover:fill-white' d='M7.31261 7.36039V16.9704H10.2964V12.2181C10.2964 10.9641 10.5323 9.74964 12.0872 9.74964C13.6206 9.74964 13.6396 11.1834 13.6396 12.2972V16.9712H16.625V11.7011C16.625 9.11235 16.0677 7.12289 13.0419 7.12289C11.5892 7.12289 10.6154 7.9201 10.2172 8.67456H10.1769V7.36039H7.31261ZM2.4502 7.36039H5.43874V16.9704H2.4502V7.36039Z' fill='black' />
                                </svg>
                            </button>

                            <button onClick={shareOnFacebook} className='bg-[#f8f8f9] size-[30px] rounded-full flex justify-center items-center group hover:bg-[#1877F2]'>
                                <svg width='21' height='22' viewBox='0 0 21 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                    <path className='group-hover:fill-white' d='M11.7219 19.2228V12.0513H14.1413L14.5009 9.24347H11.7219V7.45497C11.7219 6.64472 11.9477 6.08997 13.1105 6.08997H14.584V3.5866C13.8674 3.5096 13.1464 3.47285 12.4254 3.47547C10.2869 3.47547 8.81866 4.78097 8.81866 7.1776V9.23822H6.41504V12.0461H8.82391V19.2228H11.7219Z' fill='black' />
                                </svg>
                            </button>
                        </div>
                    </div>

                    <div className='w-[99%] m-auto'>
                        <Ad setShowLogin={setShowLogin} />
                    </div>
                </div>
            }
        </>
    )
}

export default NewsLetter