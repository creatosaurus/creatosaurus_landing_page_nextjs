import React from 'react'
import Head from 'next/head';
import Image from 'next/image';
import museStyle from '../../../../../styles/muse.module.css';
import NavigationBar from '../../../../../Components/LandingPageComponents/NavigationBar';
import imgSrc from '../../../../../public/Assets/muse_1.png';
import apps from "../../../../../appsInfo";
import FrequentlyAskedQuestions from '../../../../../Components/FaqSection';
import FifthBlackSection from '../../../../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../../../../Components/LandingPageComponents/Footer';
import Image1 from '../../../../../public/Assets/Muse/Create Free New Year Card Templates Online.jpeg'

import Image2 from '../../../../../public/Assets/Muse/High-Quality New Year Card Templates.jpg'
import Image3 from '../../../../../public/Assets/Muse/Customize Your Free New Year Card Template.jpg'
import Image4 from '../../../../../public/Assets/Muse/Editable Happy New Year Poster Templates.jpg'
import Image5 from '../../../../../public/Assets/Muse/Create From Scratch Or Use Pre-Existing New Year Card Templates.jpg'
import Image6 from '../../../../../public/Assets/Muse/Downloadable Happy New Year Poster Templates.jpg'
import Image7 from '../../../../../public/Assets/Muse/Easily Save Your New Year Card Template.jpg'



const Newyear = () => {

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };


    let data = [
        {
            title: "High-Quality New Year Card Templates",
            p: "New year, new beginnings — these new beginnings are a melting pot of diverse emotions. Overall Muse has 1000+ templates that cater to a wide range of themes, tones, essence, styles, and layouts — to help you convey the right mood and spirit in your happy new year poster greetings.",
            image: Image2
        }, {
            title: "Customize Your Free New Year Card Template",
            p: "Adding your personal touch can make anything beautiful and memorable in no time — your happy new year poster too. Sprinkle royalty-free images, text, fun elements, icons, treasured pictures from your album, and personalized messages — simply by dragging, dropping, or uploading.",
            image: Image3
        }, {
            title: "Editable Happy New Year Poster Templates",
            p: "Give your new year card template a distinct personality and style — to express the true you. Pour in custom colours, mix and match different shades,and position visual elements as you like — in a few clicks. Here’s the best part — you could be a design rookie and still carry out all of this without a hitch.",
            image: Image4
        }, {
            title: "Create From Scratch Or Use Pre-Existing New Year Card Templates",
            p: "Is there a design layout you have in mind for your happy new year poster card? Let that creative zeal come to life in your Muse artboard — with all the text and visual elements you need in one place. Have no idea where to start? Muse has got your back with plenty of free new year card wishes templates.",
            image: Image5
        }, {
            title: "Downloadable Happy New Year Poster Templates",
            p: "Whether choosing a template, designing, adding text and visual icons, uploading personal pictures, or editing the layout and colours — unfold a fuss-free way to tailor your new year card template with Muse. When it is all done and dusted — download your custom new year card in one click.",
            image: Image6
        }, {
            title: "Easily Save Your New Year Card Template",
            p: "Create unlimited custom new year greeting cards. Once you have finished creating your happy new year poster card, save your work. Muse stores all your new year wishes creations in one place —  revisit them in your dashboard 24/7.",
            image: Image7
        }, {
            title: "Happy New Year Poster Templates For All",
            p: "Usher in the new year by expressing gratitude to all the people who add value to your life — your friends, family, and colleagues. Whether you are a student, working professional, employer, or homemaker — Muse has got your back with a range of new year card templates that anyone can use.",
            image: Image2
        }, {
            title: "Seamlessly Share Your Custom Happy New Year Poster",
            p: "True joy and festivity lie in the power of sharing — your personalized new year card is no exception. When you have forged your imprints to any happy new year poster template from Muse, wait no more. Download it and share it personally with your loved ones or on social media — in seconds.",
            image: Image3
        }, {
            title: "Printable New Year Card Templates",
            p: "E-cards have their perks — convenience and quickness. However, physical happy new year poster cards hold an unspoken charm. Are you bit by the old-school bug? You have come to the right place. Muse has an easy printing feature for any custom new card you create and download.",
            image: Image4
        }
    ]


    let faq = [
        {
            id: 1,
            title: "Are new year card template's free?",
            content: "Yes these are free to use.",
        }, {
            id: 2,
            title: "Is Muse design editor free to use?",
            content: "Yes it is free to use. You can upgrade to use premium features",
        }, {
            id: 3,
            title: "How many templates are available?",
            content: "Overall there are 1000+ templates available in Muse",
        }, {
            id: 4,
            title: "Is there a paid version?",
            content: "Yes, you can checkout the pricing page.",
        }
    ]

    const openUrl = (url) => {
        window.open(url)
    }

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Create Free New Year Card Templates | Customizable & Printable 2023</title>
                <meta
                    name="title"
                    content="Create Free New Year Card Templates | Customizable & Printable 2023"
                />
                <meta
                    name="description"
                    content="Send warm wishes to your dear ones by creating a tailor-made happy new year poster — design, download & print your favourite new year card template for free."
                />
                <meta
                    property="og:title"
                    content="Create Free New Year Card Templates | Customizable & Printable 2023"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Send warm wishes to your dear ones by creating a tailor-made happy new year poster — design, download & print your favourite new year card template for free."
                    key="description"
                />
            </Head>
            <NavigationBar />

            <div className={museStyle.top_block}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{ boxShadow: 'none' }}>
                        <Image src={Image1} alt='Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates' />
                    </div>
                    <div className={museStyle.blockText}>
                        {
                            /**
                             * <div className={museStyle.blockText_icon}>
                            <Image src={logo} alt='Failed to load image' />
                        </div>
                             */
                        }
                        <div className={museStyle.blockText_title}>
                            <h1>Unique New Year Card Templates To Unleash A Fresh Start</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>
                                Celebrate love, joy, and hope with your family and friends — make this happen with personalizable, original happy year new poster templates.
                            </p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button onClick={() => {
                                openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=new%20year&app=muse");
                            }}>Get Started For Free</button>
                        </div>
                    </div>
                </div>
            </div>



            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/eS5tpAUEuzA?start=492'
                    title='YouTube video player'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>



            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h1>{res.title}</h1>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button onClick={() => {
                                        openUrl("https://www.muse.creatosaurus.io/editor?width=1080&height=1080&type=instagram&category=new%20year&app=muse");
                                    }}>Get Started For Free</button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.redPoster}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h1>Free online graphic design post maker and creator</h1>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Create graphic design posts for Instagram, Facebook, and other social media platforms at scale with our massive library of templates with our easy-to-use design editor.
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.muse.creatosaurus.io/")}>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <Image src={imgSrc} alt='muse_img loading...' />
                    </div>
                </div>
            </div>



            <div className={museStyle.how_to_container}>
                <h1>3 Easy steps to create your New Year Card</h1>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <h4>1. Select a format</h4>
                        <p>Choose your resolution and type</p>
                    </div>
                    <div className={museStyle.card}>
                        <h4>2. Customise your favourite templatet</h4>
                        <p>Provide product and brand-related inputs, along with required keywords.</p>
                    </div>
                    <div className={museStyle.card}>
                        <h4>3. Download and share</h4>
                        <p>You can save, download or directly share on social media</p>
                    </div>
                </div>
            </div>


            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>;
                        })}
                    </div>
                </div>
            </div>


            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default Newyear