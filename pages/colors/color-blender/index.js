import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import style from './ColorBlender.module.css'
import { ChromePicker, SketchPicker } from 'react-color';
import tinycolor from "tinycolor2"
import Link from 'next/link'
import Head from 'next/head';
import BlogNavigationBar from '../../../Components/BlogNavigationBar';
import Footer from '../../../Components/LandingPageComponents/Footer';
import Share from '../../../ComponentsNew/QuoteComponents/Share'
import Toast from '../../../ComponentsNew/ToastComponents/Toast/Toast';
import { getSinglePostById } from '../../../lib/posts';
import BlogSection from '../../../ComponentsNew/ToolsComponents/BlogSection';
import ArticleByCard from '../../../Components/ArticleByCard';

export async function getServerSideProps() {
  const id = "668067b1da8d0c062a1db086"
  const post = await getSinglePostById(id)

  if (!post) {
    return {
      notFound: true
    }
  }

  return {
    props: { post }
  }
}

const ColorBlender = ({ post }) => {
  const router = useRouter()
  const [textColor, setTextColor] = useState("#ff0000")
  const [textColorToChange, setTextColorToChange] = useState("#ff0000")

  const [backgroundColor, setBackgroundColor] = useState("#0000ff")
  const [backgroundColorToChange, setBackgroundColorToChange] = useState("#0000ff")

  const [showColorPicker1, setShowColorPicker1] = useState(false)
  const [showColorPicker2, setShowColorPicker2] = useState(false)
  const [steps, setSteps] = useState(5)
  const [colors, setColors] = useState([])
  const [activeHoverIndex, setActiveHoverIndex] = useState(null)
  const [showMessage, setShowMessage] = useState(null)

  useEffect(() => {
    blendColors()
  }, [])

  const blendColors = (value, startColor, endColor) => {
    const start = tinycolor(startColor ? startColor : textColor);
    const end = tinycolor(endColor ? endColor : backgroundColor);
    const colorSteps = [];

    let data = value ? value : steps
    let stepsData = data + 2
    for (let i = 0; i < stepsData; i++) {
      const ratio = i / (stepsData - 1);
      const blended = tinycolor.mix(start, end, ratio * 100).toHexString();
      colorSteps.push(blended);
    }

    setColors(colorSteps);
  };

  const handleColorChange = (color) => {
    setTextColor(color.hex);
    blendColors(null, color.hex, null)
  };

  const handleColorChange2 = (color) => {
    setBackgroundColor(color.hex);
    blendColors(null, null, color.hex)
  };

  const openColorPicker1 = (event) => {
    event.stopPropagation();
    setShowColorPicker1((prev) => !prev);
    setShowColorPicker2(false)
  };

  const openColorPicker2 = (event) => {
    event.stopPropagation();
    setShowColorPicker2((prev) => !prev);
    setShowColorPicker1(false)
  };

  const closeColorPicker = () => {
    setShowColorPicker1(false)
    setShowColorPicker2(false)
  }

  function getColorOrDefault(inputColor) {
    const color = tinycolor(inputColor);
    if (color.isValid()) {
      return color.toHexString();
    } else {
      return textColor; // Return black if the color is not valid
    }
  }

  function getColorOrDefault2(inputColor) {
    const color = tinycolor(inputColor);
    if (color.isValid()) {
      return color.toHexString();
    } else {
      return backgroundColor; // Return black if the color is not valid
    }
  }

  const checkColor = () => {
    let data = getColorOrDefault(textColorToChange)
    setTextColor(data)
    setTextColorToChange(data)
    blendColors(null, data, null)
  }

  const checkColor2 = () => {
    let data = getColorOrDefault2(backgroundColorToChange)
    setBackgroundColor(data)
    setBackgroundColorToChange(data)
    blendColors(null, null, data)
  }


  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      checkColor()
    }
  }

  const handleKeyPress2 = (e) => {
    if (e.key === 'Enter') {
      checkColor2()
    }
  }

  const rangeChange = (e) => {
    const value = parseInt(e.target.value)
    setSteps(value)
    blendColors(value)
  }

  function getContrastingColor(color) {
    const r = parseInt(color.slice(1, 3), 16);
    const g = parseInt(color.slice(3, 5), 16);
    const b = parseInt(color.slice(5, 7), 16);
    const luminance = (0.299 * r + 0.587 * g + 0.114 * b) / 255;
    return luminance > 0.5 ? '#000' : '#FFF';
  }

  const copy = (color) => {
    navigator.clipboard.writeText(color);
    color = color.toUpperCase()
    setShowMessage(`${color} copied to the clipboard!`)
    setTimeout(() => {
      setShowMessage(null)
    }, 1200);
  }

  const goToPage = (e) => {
    router.push('/colors/' + e.target.value)
  }

  const getStyle = (data, index) => {
    if (index === activeHoverIndex) {
      return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.3)" : "rgba(255,255,255,0.3)" }
    } else {
      return { backgroundColor: getContrastingColor(data) === "#000" ? "rgba(0,0,0,0.1)" : "rgba(255,255,255,0.1)" }
    }
  }

  const getGradientRange = () => {
    const value = steps
    const max = 25;
    const percentage = (value / max) * 100;
    return `linear-gradient(to right, #0078FF 0%, #0078FF ${percentage}%, #ddd ${percentage}%, #ddd 100%)`;
  };

  return (
    <div className={style.blenderContainerScroll} onClick={closeColorPicker}>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
        <title>{post.title}</title>
        <meta name='description' content={post.custom_excerpt} />

        {/* Open Graph (OG) Meta Tags */}
        <meta property='og:title' content={post.ogTitle} />
        <meta property='og:image' content={post.ogImg} />
        <meta property='og:description' content={post.ogDis} />
        <meta property='og:url' content="https://www.creatosaurus.io/tools/blog-title-generator" />
        <meta property='og:card' content='summary_large_image' />
        <meta property='og:type' content='website' />
        <meta property='og:site_name' content='Creatosaurus' />


        {/* twitter meta tags  */}
        <meta name='twitter:card' content='summary_large_image' />
        <meta name='twitter:site' content='@creatosaurus' />
        <meta name='twitter:title' content={post.twitter_title} />
        <meta name='twitter:description' content={post.twitter_description} />
        <meta name='twitter:image' content={post.twitter_image} />
      </Head >

      <BlogNavigationBar title="Colors" slug="/" />

      <div className={style.blenderContainer}>
        <div className={style.head}>
          <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M7.79177 13.7091L13.4839 8.01702C13.8906 7.61021 14.5502 7.61021 14.957 8.01702L17.1667 10.2267C17.5734 10.6335 17.5734 11.2931 17.1667 11.6998L15.1575 13.7091M7.79177 13.7091L5.5289 15.972C5.43165 16.0692 5.35463 16.1847 5.30226 16.3119L4.2314 18.9125C3.70259 20.1968 4.98686 21.4811 6.27113 20.9523L8.87178 19.8815C8.99896 19.8291 9.11449 19.752 9.21174 19.6548L15.1575 13.7091M7.79177 13.7091H15.1575" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M14.957 3.59766L17.1667 5.80736M17.1667 5.80736L18.6398 4.33423C19.0467 3.92744 19.7062 3.92744 20.113 4.33423L20.8496 5.0708C21.2564 5.47759 21.2564 6.13715 20.8496 6.54394L19.3764 8.01708M17.1667 5.80736L19.3764 8.01708M21.5862 10.2268L19.3764 8.01708" stroke="black" strokeWidth="1.75" strokeLinecap="round" strokeLinejoin="round" />
          </svg>

          <h1>{post.title}</h1>
          <p>{post.custom_excerpt}</p>
          <div className={style.divider} />

          <Share />
        </div>

        <div className={style.blenderGenerator}>
          <div className={style.blenderWrapper}>
            <div className={style.leftSide}>
              <h2>Color Blender
                <svg style={{ marginLeft: 5 }} width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.8105 8.5957L10.0009 12.4052L6.19141 8.5957" stroke="#404040" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                </svg>

                <select
                  style={{ position: 'absolute', width: '100%', opacity: 0, cursor: 'pointer' }}
                  onChange={goToPage}>
                  <option value="color-blender" disabled selected>Color Blender</option>
                  <option value="color-harmony">Color Harmony</option>
                  <option value="color-image-picker">Color Image Picker</option>
                  <option value="color-mixer">Color Mixer</option>
                  <option value="color-picker">Color Picker</option>
                  <option value="color-wheel">Color Wheel</option>
                  <option value="contrast-checker">Contrast Checker</option>
                  <option value="gradient-generator">Gradient Generator</option>
                  <option value="tint-shades">Tint Shades</option>
                </select>
              </h2>
              <div className={style.row}>
                <label>Start Color</label>
                <div className={style.wrapper}>
                  <input type='text'
                    placeholder='#000000'
                    value={textColorToChange}
                    onFocus={(e) => e.target.select()}
                    onChange={(e) => setTextColorToChange(e.target.value)}
                    onBlur={checkColor}
                    onKeyDown={handleKeyPress} />
                  <span style={{ backgroundColor: textColor }} onClick={openColorPicker1} />
                </div>

                <>
                  {showColorPicker1 ? (
                    <div className={style.colorPicker} onClick={(e) => e.stopPropagation()}>
                      <ChromePicker color={textColor} onChange={handleColorChange} />
                    </div>
                  ) : null}
                </>
              </div>

              <div className={style.row} style={{ marginTop: 15 }}>
                <label>End Color</label>
                <div className={style.wrapper}>
                  <input type='text'
                    placeholder='#000000'
                    value={backgroundColorToChange}
                    onFocus={(e) => e.target.select()}
                    onChange={(e) => setBackgroundColorToChange(e.target.value)}
                    onBlur={checkColor2}
                    onKeyDown={handleKeyPress2} />
                  <span style={{ backgroundColor: backgroundColor }} onClick={openColorPicker2} />
                </div>

                <>
                  {showColorPicker2 ? (
                    <div className={style.colorPicker} onClick={(e) => e.stopPropagation()}>
                      <ChromePicker color={backgroundColor} onChange={handleColorChange2} />
                    </div>
                  ) : null}
                </>
              </div>

              <div className={style.row} style={{ marginTop: 15 }}>
                <label>Number of Inbetween Colors ({steps})</label>
                <div className={style.wrapperRange}>
                  <input type="range" min="1" max="25" step="1"
                    className={style.range}
                    style={{ background: getGradientRange() }}
                    onChange={rangeChange}
                    value={steps} />
                </div>
              </div>

              <span className={style.info}>Powered by{" "}
                <Link href="https://www.creatosaurus.io/">
                  <a target="_blank">Creatosaurus.io</a>
                </Link>
              </span>
            </div>
            <div className={style.rightSide}>
              <div className={style.wrapper}>
                {
                  colors.map((data, index) => {
                    return <div key={data + index} className={style.box} style={{ backgroundColor: data }}>
                      <div className={style.content}>
                        <div className={style.info}>
                          <svg
                            onMouseEnter={() => setActiveHoverIndex(index)}
                            onMouseLeave={() => setActiveHoverIndex(null)}
                            onClick={() => copy(data)} style={getStyle(data, index)} width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.125 13H6C5.79289 13 5.625 12.8321 5.625 12.625V6.5C5.625 6.29289 5.79289 6.125 6 6.125H12.125C12.3321 6.125 12.5 6.29289 12.5 6.5V12.625C12.5 12.8321 12.3321 13 12.125 13Z" stroke={getContrastingColor(data)} strokeLinecap="round" strokeLinejoin="round" />
                            <path d="M9.375 6.125V3.375C9.375 3.16789 9.20712 3 9 3H2.875C2.66789 3 2.5 3.16789 2.5 3.375V9.5C2.5 9.70712 2.66789 9.875 2.875 9.875H5.625" stroke={getContrastingColor(data)} strokeLinecap="round" strokeLinejoin="round" />
                          </svg>
                          <span onClick={() => copy(data)} style={{ color: getContrastingColor(data) }}>{data}</span>
                        </div>
                      </div>
                    </div>
                  })
                }
              </div>

              <Link href="https://www.creatosaurus.io/apps/muse">
                <a target="_blank">
                  Design using this color combination on Muse
                  <svg style={{ marginLeft: 5 }} width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M1.75 7.50033H12.25M12.25 7.50033L7.29167 2.54199M12.25 7.50033L7.29167 12.4587" stroke="#0078FF" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                </a>
              </Link>
            </div>
          </div>
        </div>

        <div style={{ marginTop: -50 }}>
          <BlogSection post={post} />
        </div>

        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }} >
          <ArticleByCard post={post} />
        </div>
      </div>
      <Footer />
      {showMessage ? <Toast message={showMessage} /> : null}
    </div>
  )
}

export default ColorBlender