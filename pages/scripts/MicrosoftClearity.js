import React from 'react'
import Script from 'next/script';

const MicrosoftClearity = () => {
    return (
        <Script
        id='mc'
        strategy='lazyOnload'>
            {`
            (function(c,l,a,r,i,t,y){
            c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
            t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i;
            y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
            })(window, document, "clarity", "script", "89m8ybeofx");
      `}
        </Script>
    )
}

export default MicrosoftClearity