import React, { useState, useRef, useEffect } from 'react'
import AppIntegrationImage from '../../../public/NewAssets/HeroSection/app-integration.webp'
import DesignEditorImage from '../../../public/NewAssets/HeroSection/design-editor.webp'
import AiChatImage from '../../../public/NewAssets/HeroSection/ai-chat.webp'
import StockAssetsImage from '../../../public/NewAssets/HeroSection/stock-assets.webp'
import SocialAnalyticsImage from '../../../public/NewAssets/HeroSection/social-analytics.webp'
import VideoEditorImage from '../../../public/NewAssets/HeroSection/video-editor.webp'
import DocEditorImage from '../../../public/NewAssets/HeroSection/doc-editor.webp'
import SocialInboxImage from '../../../public/NewAssets/HeroSection/social-inbox.webp'
import BrandKitImage from '../../../public/NewAssets/HeroSection/brand-kit.webp'
import PostSchedularImage from '../../../public/NewAssets/HeroSection/post-schedular.webp'
import AiImageToolsImage from '../../../public/NewAssets/HeroSection/ai-image-tools.webp'
import NavigationAuth from '../../../Components/NavigationAuth/NavigationAuth';
import jwtDecode from 'jwt-decode'
import Image from 'next/image'

const HeroSectionNew = ({ hideTitles }) => {
    const data = [{
        title: "App Integrations",
        color: "#FF4359",
        img: AppIntegrationImage
    }, {
        title: "Social Analytics",
        color: "#0078FF",
        img: SocialAnalyticsImage
    }, {
        title: "Video Editor",
        color: "#8A43FF",
        img: VideoEditorImage
    }, {
        title: "AI Chat",
        color: "#00B0A7",
        img: AiChatImage
    }, {
        title: "Design Editor",
        color: "#FF8A25",
        img: DesignEditorImage
    }, {
        title: "AI Image Tools",
        color: "#FF4359",
        img: AiImageToolsImage
    }, {
        title: "Doc Editor",
        color: "#1410C7",
        img: DocEditorImage
    }, {
        title: "Post Scheduler",
        color: "#0078FF",
        img: PostSchedularImage
    }, {
        title: "Social Inbox",
        color: "#B938D9",
        img: SocialInboxImage
    }, {
        title: "Stock Asset",
        color: "#009B77",
        img: StockAssetsImage
    }, {
        title: "Brand Kit",
        color: "#FF6543",
        img: BrandKitImage
    }]

    const scrollContainerRef = useRef(null);
    const [showLogin, setShowLogin] = useState(false)

    useEffect(() => {
        const scrollWidth = scrollContainerRef.current.scrollWidth;
        const containerWidth = scrollContainerRef.current.clientWidth;
        scrollContainerRef.current.scrollLeft = (scrollWidth - containerWidth) / 2;
    }, []);

    const getCookie = (name) => {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const checkUserLoggedInOrNot = () => {
        const value = getCookie('Xh7ERL0G');
        if (value) {
            const decoded = jwtDecode(value);
            const expirationTime = (decoded.exp * 1000) - 60000
            if (Date.now() >= expirationTime) {
                setShowLogin(true)
            } else {
                window.open("https://www.app.creatosaurus.io/", "_self")
            }
        } else {
            setShowLogin(true)
        }
    }

    const handleScrollRight = () => {
        scrollContainerRef.current.scrollBy({
            left: scrollContainerRef.current.clientWidth,
            behavior: 'smooth'
        })
    }

    const handleScrollLeft = () => {
        scrollContainerRef.current.scrollBy({
            left: -scrollContainerRef.current.clientWidth,
            behavior: 'smooth'
        })
    }

    return (
        <div className={`${hideTitles ? "mt-[50px]" : "mt-[150px]"}`}>
            {
                hideTitles ? null :
                    <>
                        {showLogin && <NavigationAuth close={() => setShowLogin(false)} />}
                        <div className='max-[960px]:px-[20px] min-[960px]:px-[80px] flex flex-col items-center'>
                            <h1 className='text-[32px] md:text-[42px] lg:text-[64px] font-semibold text-center'>AI Content Creation & Social Media Marketing Tool</h1>
                            <p className='text-[16px] md:text-[18px] lg:text-[22px] text-center mt-[10px] max-w-[800px]'>Creatosaurus makes it simple and easy for marketing teams to create, share, manage & analyze visually stunning content, together.</p>
                            <button onClick={checkUserLoggedInOrNot} className='px-[20px] lg:px-[50px] mt-[30px] bg-black text-white text-[14px] lg:text-[22px] font-semibold h-[44px] lg:h-[54px] rounded-[5px] transition-transform duration-300 ease-in-out transform hover:scale-[1.04]'>Start Creating—It's Free</button>
                        </div>
                    </>
            }

            <div className='relative flex items-center mt-[10px]'>
                <div
                    className='flex items-center gap-x-[15px] md:gap-x-[20px] overflow-x-scroll w-full px-[25px] h-[200px] md:h-[350px]'
                    ref={scrollContainerRef}>
                    {
                        data.map(info => {
                            return <div
                                style={{ backgroundColor: info.color }}
                                onClick={checkUserLoggedInOrNot}
                                className='w-[140px] min-w-[140px] md:w-[240px] md:min-w-[240px] h-[175px] md:h-[300px] rounded-[15px] p-[20px] flex flex-col transition-transform duration-300 ease-in-out transform hover:scale-[1.08] hover:z-10 cursor-pointer'>
                                <span className='mt-[5px] text-[12px] md:text-[18px] font-semibold text-white text-center mb-[18px]'>{info.title}</span>
                                <div className='relative rounded-[10px] overflow-hidden'>
                                    <Image src={info.img} alt='' />
                                </div>
                            </div>
                        })
                    }
                </div>

                <svg onClick={handleScrollLeft} className='absolute size-[34px] left-[20px] group cursor-pointer z-20' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle className='group-hover:stroke-black' cx="12.5" cy="12.5" r="12" fill="white" stroke="#DCDCDC" />
                    <path className='group-hover:stroke-black' d="M14.25 9L10.5 12.75L14.25 16.5" stroke="#333333" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>

                <svg onClick={handleScrollRight} className='absolute size-[34px] right-[20px] group cursor-pointer z-20' width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle className='group-hover:stroke-black' cx="12.5" cy="12.5" r="12" fill="white" stroke="#DCDCDC" />
                    <path className='group-hover:stroke-black' d="M10.625 8.75L14.375 12.5L10.625 16.25" stroke="#333333" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                </svg>
            </div>
        </div>
    )
}

export default HeroSectionNew