import React from 'react'
import styles from './TeamsSection.module.css'
import Image from 'next/image'
import Canva from '../../../public/NewAssets/Home/canva.png'
import Buffer from '../../../public/NewAssets/Home/buffer.png'
import Jasper from '../../../public/NewAssets/Home/jasper.png'
import Midjourney from '../../../public/NewAssets/Home/midjourney.png'
import ChatGPT from '../../../public/NewAssets/Home/chatGPT.png'
import Link from 'next/link'

const TeamsSection = () => {
  const data = [
    {
      image: Canva,
      title: "Canva",
      subTitle: "Graphic Design",
      price: "$300"
    },
    {
      image: Buffer,
      title: "Buffer",
      subTitle: "Social Media Scheduling",
      price: "$1200"
    },
    {
      image: Jasper,
      title: "Jasper",
      subTitle: "AI Copilot",
      price: "$1188"
    },
    {
      image: Midjourney,
      title: "Midjourney",
      subTitle: "AI Image",
      price: "$288"
    },
    {
      image: ChatGPT,
      title: "ChatGPT",
      subTitle: "AI Writer",
      price: "$240"
    }
  ]

  const gridData = ["Graphic Design", "Hashtag Insights", "Social Media Scheduling", "Inbox & Analytics", "AI Writer & Image", "Integrations & Apps", "Blog Editor", "Team Collaboration"]

  return (
    <div className={styles.teamsContainer}>
      <h2>Teams save more with Creatosaurus</h2>
      <p>Pro plan for 1 Year with 5 Team Members & 10 Social Accounts</p>
      <div className={styles.wrapper}>
        <div className={styles.compitition}>
          {
            data.map((cards, index) => {
              return <>
                <div className={styles.card}>
                  <div className={styles.row}>
                    <div className={styles.imageWrapper}>
                      <Image src={cards.image} alt={cards.title} />
                    </div>
                    <span>{cards.title}</span>
                  </div>

                  <span>{cards.subTitle}</span>
                  <span>{cards.price}</span>
                </div>

                {
                  index === 4 ?
                    null :
                    <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M9.66602 10.8333H4.66602V9.16663H9.66602V4.16663H11.3327V9.16663H16.3327V10.8333H11.3327V15.8333H9.66602V10.8333Z" fill="#C4C4C4" />
                    </svg>
                }
              </>
            })
          }

          <button>
            <span>All Total Yearly</span>
            <span>$3216</span>
          </button>
        </div>

        <div className={styles.compare}>
          <span>{"}"}</span>
          <span> v/s</span>
        </div>

        <div className={styles.creatosaurus}>
          <div className={styles.head}>
            <svg xmlns="http://www.w3.org/2000/svg" width="63" height="30" viewBox="0 0 63 30" fill="none">
              <g clipPath="url(#clip0_10312_6961)">
                <path d="M42.5527 30L39.813 28.3052L39.3787 30H42.5527Z" fill="black" />
                <path d="M46.6035 27.8643L42.9756 26.1957V27.9297L46.6035 27.8643Z" fill="black" />
                <path d="M39.0297 29.8498L35.8099 26.8259L37.4819 23.9164L39.5195 27.9623L39.0297 29.8498Z" fill="black" />
                <path d="M33.166 6.8053L25.7991 15.0213L4.43955 14.8645L33.166 6.8053Z" fill="black" />
                <path d="M51.4264 14.9364L51.4492 18.9203H47.1976L51.4264 14.9364Z" fill="black" />
                <path d="M48.9408 21.1734L51.416 19.2925H49.0061L48.9408 21.1734Z" fill="black" />
                <path d="M35.5904 26.4634L40.7891 17.4115L33.9315 20.7652L35.5904 26.4634Z" fill="black" />
                <path d="M8.38672 13.3722L4.16444 14.5576L0.500559 12.3958L8.38672 13.3722Z" fill="black" />
                <path d="M59.0662 11.919L54.5892 10.4397L53.959 6.58643L60.1602 11.4389L59.0662 11.919Z" fill="black" />
                <path d="M42.6055 27.9297L38.246 22.5874L40.14 19.2925H42.6055V27.9297Z" fill="black" />
                <path d="M40.7109 17.0393L33.5954 20.517L26.2546 15.0637L33.5824 6.89343L38.9574 8.95722L40.7109 17.0393Z" fill="black" />
                <path d="M62.8223 5.1889L55.5533 7.36371L53.9075 6.07384L53.1434 1.57727H60.974L62.8223 5.1889Z" fill="black" />
                <path d="M52.7715 1.57727L54.2051 10.3712L51.4327 14.4237L46.5345 9.6724L48.7223 5.46973L52.7715 1.57727Z" fill="black" />
                <path d="M46.6577 18.9203H40.352L41.1815 17.4769L39.3659 9.09442L45.9556 9.62343L51.1641 14.6751L46.6577 18.9203Z" fill="black" />
                <path d="M57.1199 1.20496H58.9355L57.982 0L57.1199 1.20496Z" fill="black" />
              </g>
              <defs>
                <clipPath id="clip0_10312_6961">
                  <rect width="62.3218" height="30" fill="white" transform="matrix(-1 0 0 1 62.8223 0)" />
                </clipPath>
              </defs>
            </svg>
            <p>Creatosaurus</p>
          </div>

          <span className={styles.subHead}>All-in-One 92% OFF</span>
          <p className={styles.info}>For the Startup pro plan, which includes all our features</p>
          <div className={styles.grid}>
            {
              gridData.map(data => {
                return <div key={data} className={styles.row}>
                  <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M4.66602 10.8334L7.99935 14.1667L16.3327 5.83337" stroke="#333333" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                  </svg>
                  <span>{data}</span>
                </div>
              })
            }
          </div>
          <Link href="#price">
            <button className={styles.mainButton}>Claim Exclusive Deal Now</button>
          </Link>
        </div>
      </div>
    </div>
  )
}

export default TeamsSection