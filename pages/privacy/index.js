import React from 'react';
import privacyStyle from '../..//styles/privacy.module.css';
import Navigation from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import Image from 'next/image';
import HeadInfo from '../../ComponentsNew/HeadTag/HeadInfo';

const PrivacyPolicy = () => {
  const privacyPage = () => {
    return (
      <div className={privacyStyle.box}>
        <div className={privacyStyle.container}>
          <h1>Privacy Policy</h1>
          <h2>January 1, 2024</h2>
          <p>
            Creatosaurus provides this Privacy Policy to inform you of our
            policies and procedures regarding the collection, use, protection,
            and disclosure of Personal Information received from your use of
            this website, located at https://creatosaurus.io (“Site”), as well
            as all related websites including our subdomains, applications,
            browser extensions, and other services provided by us (collectively,
            together with the Site, our “Service”), and in connection with our
            customer, vendor, and partner relationships. This Privacy Policy
            also tells you about your rights and choices with respect to your
            Personal Information, and how you can reach us to update your
            contact information or get answers to questions you may have about
            our privacy practices.
          </p>
          <div className={privacyStyle.points}>
            <h1>1. Personal information we may collect</h1>
            <p>
              For the purpose of this Privacy Policy, “Personal Information”
              means any information relating to an identified or identifiable
              individual. We obtain Personal Information relating to you from
              various sources described below. Where applicable, we indicate
              whether and why you must provide us with your Personal
              Information, as well as the consequences of failing to do so. If
              you do not provide Personal Information when requested, you may
              not be able to benefit from our Service if that information is
              necessary to provide you with the service or if we are legally
              required to collect it.
            </p>
            <h1>2. Personal information provided by you</h1>
            <h1>2a. Personal Information Collected from using the Service</h1>
            <br />
            <span>1. Registration</span>
            <p>
              If you desire to have access to certain restricted sections of the
              Site or request to receive marketing materials, you may be
              required to become a registered user, and to submit the following
              types of Personal Information to Creatosaurs: your name, email
              address, phone number, full user name, password, city, time zone,
              country of residence, gender, age, your postal address, telephone
              number, PAN, GST and TAN numbers.
            </p>
            <br />
            <span>2. Customer Support</span>
            <p>
              We may also collect the information you send us through the
              Service (these include communications with our customer support
              team or other communications that you may send us and their
              contents), and may collect the information you provide in User
              Content you post to the Service (such as text and photos you
              upload to use in your designs). We use this information to
              operate, maintain, and provide the features and functionality of
              the Service to you, to correspond with you, and to address any
              issues you raise about the Service.
            </p>
            <br />
            <span>3. Making a Purchase</span>
            <p>
              When you make payments through the Service, you will need to
              provide Personal Information such as your card number and billing
              address.
            </p>
            <br />
            <span>4. Social Media</span>
            <p>
              In order to allow you to post to your social media platforms, we
              may ask you to provide your username, account ids, social handle,
              time zones, and email address.
            </p>
            <br />
            <span>5. Data Storage</span>
            <p>
              In order to allow you to upload your assets such as photos, videos
              and illustrations to create graphics a videos we may ask you to
              provide your asset files.
            </p>
            <br />
            <span>6. Other</span>
            <p>
              We may also collect your contact details when you provide them in
              the context of our customer, vendor, and partner relationships.
            </p>
            <h1>
              2b. Personal Information Collected from Connected Social Media
              Accounts
            </h1>
            <p>
              If you connect your third party social media account to your
              Creatosaurus account, we may collect certain information stored in
              your social media account such as:
            </p>

            <p>
              Creatosaurus may allow you to connect your third party social
              media account such as LinkedIn, Twitter, Facebook, etc to your
              Creatosaurus account, in which case we will access certain
              information from the platform regarding your account. In
              particular, we may collect profile image, display name,
              username/page ID or profile ID, access tokens, and send posts.
              This includes the content of your post and engagement data (such
              as click rates, likes, re-shares, impressions, as well as general
              engagement counts), to the extent permitted by applicable law.
              This data will only be used by Creatosaurus to provide you with
              the Service you expect and will not be shared with any third
              parties.
            </p>

            <h1>
              2c. Personal Information Automatically Obtained from Your
              Interactions with the Service
            </h1>
            <br />
            <span>1. Log Data</span>
            <p>
              When you use our Service, our servers automatically record
              information that your browser sends whenever you visit a website
              (“Log Data”). This Log Data may include information such as your
              IP address, browser type or the domain from which you are
              visiting, the web-pages you visit, the search terms you use, and
              any advertisements on which you click.
            </p>
            <br />
            <span>2. Cookies and Similar Technologies</span>
            <p>
              Like many websites, we also use “cookie” technology to collect
              additional website usage data and to improve the Site and our
              Service. A cookie is a small data file that we transfer to your
              computer’s hard disk. A session cookie enables certain features of
              the Site and our service and is deleted from your computer when
              you disconnect from or leave the Site. A persistent cookie remains
              after you close your browser and may be used by your browser on
              subsequent visits to the Site. Persistent cookies can be removed
              by following your web browser help file directions. Most Internet
              browsers automatically accept cookies. Creatosaurus may use both
              session cookies and persistent cookies to better understand how
              you interact with the Site and our Service, to monitor aggregate
              usage by our users and web traffic routing on the Site, and to
              improve the Site and our Service. We may also automatically record
              certain information from your device by using various types of
              technology, including “clear gifs” or “web beacons.” This
              automatically-collected information may include your IP address or
              other device address or ID, web browser and/or device type, the
              web pages or sites that you visit just before or just after you
              use the Service, the pages or other content you view or otherwise
              interact with on the Service, and the dates and times that you
              visit, access, or use the Service. We also may use these
              technologies to collect information regarding your interaction
              with email messages, such as whether you opened, clicked on, or
              forwarded a message, to the extent permitted under applicable law.
              You can instruct your browser, by editing its options, to stop
              accepting cookies or to prompt you before accepting a cookie from
              the websites you visit. Please note that if you delete, or choose
              not to accept, cookies from the Service, you may not be able to
              utilize the features of the Service to their fullest potential.
            </p>
            <br />
            <span>3. Third-Party Web Beacons and Third Party Buttons</span>
            <p>
              We may display third-party content on the Service, including
              third-party advertising. Third-party content may use cookies, web
              beacons, or other mechanisms for obtaining data in connection with
              your viewing of the third party content on the Service.
              Additionally, we may implement third party buttons, such as
              Facebook “share” buttons, that may function as web beacons even
              when you do not interact with the button. Information collected
              through third-party web beacons and buttons is collected directly
              by these third parties, not by Creatosaurus. Please consult such
              third party’s data collection, use, and disclosure policies for
              more information.
            </p>
            <br />
            <span>4. Links to Other Websites</span>
            <p>
              Our Site contains links to other websites. The fact that we link
              to a website is not an endorsement, authorization or
              representation of our affiliation with that third party. We do not
              exercise control over third party websites. These other websites
              may place their own cookies or other files on your computer,
              collect data or solicit Personal Information from you. Other sites
              follow different rules regarding the use or disclosure of the
              Personal Information you submit to them. We are not responsible
              for the content, privacy and security practices, and policies of
              third-party sites or services to which links or access are
              provided through the Service. We encourage you to read the privacy
              policies or statements of the other websites you visit.
            </p>
            <br />
            <span>5. Links to Other Websites</span>
            <p>
              Our Site contains links to other websites. The fact that we link
              to a website is not an endorsement, authorization or
              representation of our affiliation with that third party. We do not
              exercise control over third party websites. These other websites
              may place their own cookies or other files on your computer,
              collect data or solicit Personal Information from you. Other sites
              follow different rules regarding the use or disclosure of the
              Personal Information you submit to them. We are not responsible
              for the content, privacy and security practices, and policies of
              third-party sites or services to which links or access are
              provided through the Service. We encourage you to read the privacy
              policies or statements of the other websites you visit.
            </p>
            <h1>3. How we may use your personal information</h1>
            <p>We may use the Personal Information we obtain about you to:</p>
            <p>
              (a) Providing you with the Service: We use the information that
              you provide us to provide the Service to you. This includes
              allowing you to log in to Creatosaurus, operating and maintaining
              the Services like (i) Providing you with the Service: We use the
              information that you provide us to provide the Service to you.
              This includes allowing you to log in to Creatosaurus, operating
              and maintaining the Services like (ii) publish your content,
              comments or messages on social media platforms (iii) provide
              analytics to your social media platforms (iv) create and manage
              your designs and videos (v) access and manage your asset files (b)
              Customising the Service for you: Communicate with you to verify
              your account and for informational and operational purposes, such
              as account management, customer service, or system maintenance,
              including by periodically emailing you service-related
              announcements; (c) For data analytics: We use information about
              you to help us improve the Creatosaurus Service and our users’
              experience, including by monitoring aggregate metrics such as a
              total number of visitors, traffic, and demographic patterns. (d)
              Tailor our Service: e.g., we may use cookies and similar
              technologies to remember your preferences. (e) provide customer
              support (f) operate, evaluate and improve our business (including
              by developing new products and services; managing our
              communications; determining the effectiveness of our advertising;
              analyzing how the Service is being accessed and used; tracking the
              performance of the Service; debugging the Service; facilitating
              the use of our Service (g) send you marketing communications about
              products, services, offers, programs and promotions of
              Creatosaurus, and affiliated companies (h) ensure the security of
              our Service (i) manage our customer, service provider and partner
              relationships (j) enforce our agreements related to our Service
              and our other legal rights (k) comply with applicable legal
              requirements, industry standards and our policies.
            </p>
            <br />
            <span>
              Legal bases for processing information under the GDPR (for users
              in the EEA)
            </span>
            <p>
              If you are located in the European Economic Area(EEA) Creatosaurus
              processes your information in accordance with European laws and
              regulations such as the General Data Protection Regulation (GDPR).
              The GDPR governs how Creatosaurus may process your information and
              the rights that EEA users have in relation to it. We may process
              your Personal Information for the above purposes when:
            </p>
            <p>
              (a) you have consented to the use of your Personal Information,
              For example, we may seek to obtain your consent for our uses of
              cookies or similar technologies, or to send you marketing
              communications. (b) we need your Personal Information to provide
              you with services and products requested by you, or to respond to
              your inquiries (c) It is in our legitimate interests to provide a
              useful service, to send you marketing and to enhance our Service
              via research and development (but only where our legitimate
              interest isn’t overridden by your interest in protecting your data
              (d) You consent to us using your information in a certain way –
              for example, to hear about new features or offers. (e) we have a
              legal obligation to use your Personal Information (f) we have a
              legitimate interest in using your Personal Information. In
              particular, we have a legitimate interest in using your Personal
              Information to ensure and improve the safety, security, and
              performance of our Service, to anonymize Personal Information and
              carry out data analyses.
            </p>

            <h1>4. How we share personal information</h1>
            <p>
              We may disclose the Personal Information we collect about you as
              described below or otherwise disclosed to you at the time the data
              is collected, including with:
            </p>
            <br />
            <span>a. Social Media Platforms</span>
            <p>
              By using your information we help you to publish your content on
              social platforms, allow you to track metrics for analytical
              purposes, and engage with customers through public replies,
              comments and conversations (direct messages or “DMs”). We may
              allow you to link your account on Creatosaurus with an account on
              a third party social network platform, such as Twitter or
              Facebook, and to transfer your information to and from the
              applicable third-party platform. Once you share your content to a
              social media platform, its use will be governed by that platform’s
              privacy policy.
            </p>
            <br />
            <span>b. Service Providers</span>
            <p>
              We engage certain trusted third parties to perform functions and
              provide services to us, including hosting and maintenance,
              analytics, error monitoring, debugging, performance monitoring,
              billing, customer relationship, database storage and management,
              and direct marketing campaigns. We may share your Personal
              Information with these third parties, but only to the extent
              necessary to perform these functions and provide such services. We
              also require these third parties to maintain the privacy and
              security of the Personal Information they process on our behalf.
            </p>
            <br />
            <span>c. Compliance with Laws, Law Enforcement, Government Agencies, and Professional Advisors</span>
            {/* <p>
              Creatosaurus cooperates with government and law enforcement
              officials or private parties to enforce and comply with the law.
              To the extent permitted under applicable law, we may disclose any
              information about you to government or law enforcement officials
              or private parties as we believe is necessary or appropriate to
              investigate, respond to, and defend against claims, for the legal
              process (including subpoenas), to protect the property and rights
              of Creatosaurus or a third party, to protect Creatosaurus against
              liability, for the safety of the public or any person, to prevent
              or stop any illegal, unethical, fraudulent, abusive, or legally
              actionable activity, to protect the security or integrity of the
              Service and any equipment used to make the Service available, or
              to comply with the law.
            </p> */}
            <p>
              We may need to disclose information about you where we believe that it is reasonably necessary to comply with a law or regulation, or if we are otherwise legally required to do so, such as in response to a court order or legal process, or to establish, protect, or exercise our legal rights or to defend against legal claims or demands. For governmental data access requests concerning you or your organization, we would first attempt to redirect the request to you and/or we would first attempt to notify you unless we are legally prohibited from doing so. For further information, refer to our <a href="https://creatosaurus.io/privacy/government-data-request-policy" target='_blank' rel="noreferrer noopener">Government Data Request Policy</a>.
            </p>
            <p>
              In addition, we may disclose information about you if we believe it is necessary to investigate, prevent, or take action: (a) against illegal activities, fraud, situations involving potential threats to our rights or property (or to the rights or property of those who use our Services), or to protect the personal safety of any person; or (b) regarding situations that involve the security of our Services, abuse of the Services infrastructure, or the Internet in general (such as voluminous spamming, or denial of service attacks).
            </p>
            <p>
              We also use professional advisors, including lawyers and accountants, and may be required to disclose information about you when engaging them for their services and as necessary for audits, financial and other regulatory reviews.
            </p>
            <br />
            <span>d. Business Transfers</span>
            <p>
              Creatosaurus may sell, transfer or otherwise share some or all of
              its assets, including Personal Information, in connection with a
              merger, acquisition, reorganization, sale of assets, or similar
              transaction, or in the event of insolvency or bankruptcy. You will
              have the opportunity to opt-out of any such transfer if the new
              entity’s planned processing of your information differs materially
              from that set forth in this Privacy Policy.
            </p>
            <br />
            <span>e. Other Third Parties</span>
            <p>
              We may share Personal Information with our headquarters and
              affiliates, and business partners to whom it is reasonably
              necessary or desirable for us to disclose your data for the
              purposes described in this Privacy Policy. We may also make
              certain non-Personal Information available to third parties for
              various purposes, including for business or marketing purposes or
              to assist third parties in understanding our users’ interest,
              habits, and usage patterns for certain programs, content,
              services, advertisements, promotions, and functionality available
              through the Service.
            </p>
            <h1>5. How we protect personal information</h1>
            <p>
              Creatosaurus cares deeply about safeguarding the confidentiality
              of your Personal Information. We employ administrative and
              electronic measures designed to appropriately protect your
              Personal Information against accidental or unlawful destruction,
              accidental loss, unauthorized alteration, unauthorized disclosure
              or access, misuse, and any other unlawful form of processing of
              the Personal Information in our possession. Please be aware that
              no security measures are perfect or impenetrable. We cannot
              guarantee that information about you will not be accessed, viewed,
              disclosed, altered, or destroyed by breach of any of our
              administrative, physical, and electronic safeguards, subject to
              requirements under applicable law to ensure or warrant information
              security. Your privacy settings may also be affected by changes to
              the functionality of third-party sites and services that you add
              to the Creatosaurus Service, such as social networks. Creatosaurus
              is not responsible for the functionality or security measures of
              any third party. We will make any legally-required disclosures of
              any breach of the security, confidentiality, or integrity of your
              unencrypted electronically stored Personal Information to you via
              email or conspicuous posting on our Site in the most expedient
              time possible and without unreasonable delay, consistent with (i)
              the legitimate needs of law enforcement or (ii) any measures
              necessary to determine the scope of the breach and restore the
              reasonable integrity of the data system, and any other disclosures
              that may be required under applicable law. We also take measures
              to delete your Personal Information or keep it in a form that does
              not permit identifying you when this information is no longer
              necessary for the purposes for which we process it unless we are
              required by law to keep this information for a longer period. When
              determining the retention period, we take into account various
              criteria, such as the type of products and services requested by
              or provided to you, the nature and length of our relationship with
              you, possible re-enrollment with our products or services, the
              impact on the services we provide to you if we delete some
              information from or about you, mandatory retention periods
              provided by law and the statute of limitations.
            </p>
            <h1>6. Your rights and choices</h1>
            <p>
              If you decide at any time that you no longer wish to receive
              marketing communications from us, please follow the unsubscribe
              instructions provided in any of the communications. You may also
              opt-out from receiving commercial email from us by sending your
              request to us by email at contact@creatosaurus.io. Please be aware
              that, even after you opt-out from receiving commercial messages
              from us, you will continue to receive administrative messages from
              us regarding the Service. In certain jurisdictions you have the
              right to request access and receive information about the Personal
              Information we maintain about you, to update and correct
              inaccuracies in your Personal Information, to restrict or object
              to the processing of your Personal Information, to have the
              information blocked, anonymized or deleted, as appropriate, or to
              exercise your right to data portability to transfer your Personal
              Information to another company. Those rights may be limited in
              some circumstances by local law requirements. In addition to the
              above-mentioned rights, you also have the right to lodge a
              complaint with a competent supervisory authority subject to
              applicable law. Where required by law, we obtain your consent for
              the processing of certain Personal Information collected by
              cookies or similar technologies or used to send you direct
              marketing communications, or when we carry out other processing
              activities for which consent may be required. If we rely on
              consent for the processing of your Personal Information, you have
              the right to withdraw it at any time and free of charge. When you
              do so, this will not affect the lawfulness of the processing
              before your consent withdrawal. To update your preferences, ask us
              to remove your information from our mailing lists, delete your
              account or submit a request to exercise your rights under
              applicable law, please contact us as specified in the “How to
              Contact Us” section below.
            </p>
            <h1>7. Data transfers</h1>
            <p>
              Creatosaurus is based in India and operates on the basis of
              applicable Law. Personal Information that we collect may be
              transferred to, and stored at, any of our affiliates, partners or
              service providers which may be inside or outside the European
              Economic Area (“EEA”) and Switzerland, including the United
              States. By submitting your personal data, you agree to such
              transfers. Your Personal Information may be transferred to
              countries that do not have the same data protection laws as the
              country in which you initially provided the information. When we
              transfer or disclose your Personal Information to other countries,
              we will protect that information as described in this Privacy
              Policy.
            </p>
            <h1>8. Children&#39;s privacy</h1>
            <p>
              The Site is not directed to persons under 13. If a parent or
              guardian becomes aware that his or her child has provided us with
              Personal Information without their consent, he or she should
              contact us at contact@creatosaurus.io. We do not knowingly collect
              Personal Information from children under 13. If we become aware
              that a child under 13 has provided us with Personal Information,
              we will delete such information from our files.
            </p>
            <h1>9. Information that cannot be uploaded</h1>
            <p>
              While using the Services, you shall not host, display, upload,
              modify, publish, transmit, update or share any information on to
              the Services that: I. belongs to another person and to which you
              do not have any right to; II. is grossly harmful, harassing,
              blasphemous defamatory, obscene, pornographic, paedophilic,
              libellous, invasive of another’s privacy, hateful, or racially,
              ethnically objectionable, disparaging, relating or encouraging
              money laundering or gambling, or otherwise unlawful in any manner
              whatever; III. harms minors in any way; IV. infringes any patent,
              trademark, copyright or other proprietary rights; V. violates any
              law for the time being in force; VI. deceives or misleads the
              addressee about the origin of such messages or communicates any
              information which is grossly offensive or menacing in nature; VII.
              impersonates another person; VIII. contains software viruses or
              any other computer code, files or programs designed to interrupt,
              destroy or limit the functionality of any computer resource; IX.
              threatens the unity, integrity, defence, security or sovereignty
              of India, friendly relations with foreign states, or public order
              or causes incitement to the commission of any cognizable offence
              or prevents investigation of any offence or is insulting any other
              nation.
            </p>
            <h1>10. How long do we retain your information?</h1>
            <p>
              In general for the Creatosaurus Services, we do not store Content from Social Networks. Rather, when you login to the Services, we retrieve data from Social Networks in real time so that it is displayed in the platform for viewing during your session. We store other Content that you produce (such as draft Content for publication on Social Networks) so that you can easily access this material on the Services.
            </p>
            <p>
              Our services, Creatosaurus Inbox and Creatosaurus Analytics, are committed to safeguarding user privacy by refraining from storing any messages, analytics, or insights. Instead, we exclusively maintain essential identifiers such as conversation IDs for Creatosaurus Inbox and social media account IDs for Creatosaurus Analytics, facilitating seamless mapping of data from social media networks. With both services designed for real-time functionality, there&apos;s no need to retain users&apos; social messages or analytics or personal data or insights, ensuring a privacy-focused and efficient user experience. Upon receiving a data deletion request, we promptly remove these identifiers from our system, further enhancing user privacy and data security.
            </p>
            <p>Aggregated data is used by Creatosaurus for analysis, product improvement, and troubleshooting purposes. In some cases, Content may continue to exist on the Social Networks even after you or we delete it from our Services, and you will need to contact the relevant Social Network directly if you want it to remove this Content.</p>
            <p>We only retain the above mentioned information as long as required to provide the Services requested by you, for record keeping purposes, to comply with our legal obligations, resolve disputes, and enforce the terms for the Services. After it is no longer necessary for us to retain information about you, or otherwise upon your request, we will dispose of it in a secure manner or anonymize the information.</p>

            <h1>11. Creatosaurus&apos;s roles under the GDPR and UK data protection laws</h1>
            <p>Depending on the situation and the type of data involved, Creatosaurus may act as a data controller or a data processor.</p>
            <h1>Creatosaurus as a data controller</h1>
            <p>Creatosaurus may act as a data controller when we are:</p>
            <p>a. Collecting information from you to set up and administer your Creatosaurus account (for example, Account information such as your name and email address);</p>
            <p>b. Monitoring usage information on our website;</p>
            <p>c. Managing your contact and other related information to send marketing, Services, and other communications to you;</p>
            <p>d. Responding to a support or general inquiry; and</p>
            <p>e. Recruiting individuals for job opportunities.</p>
            <h1>Legal bases for processing when Creatosaurus is a data controller</h1>
            <p>The legal bases for processing information about you include:</p>
            <p>a. Your consent (for example, when you have provided your information to sign up for an account or for a webinar; or you have provided your employment history when applying for a job). Where we rely on your consent to process personal data, you have the right to withdraw your consent at any time.</p>
            <p>b. It is necessary to perform a contract (for example, we may need your information to fulfill our obligations of providing Services to you under the terms relevant to the Services you have acquired).</p>
            <p>c. Legitimate interest (for example, to provide, maintain and improve the Services for you, to maintain the security of the Services, and to attract new customers to maintain demand for the Services, all of which are described in the &quot;3. How we may use your personal information&quot; section above).</p>
            <p>d. In some cases, we may have a legal obligation to process your personal data to comply with relevant laws (for example, processing payroll and tax information to comply with relevant employment and tax legislation); or processing is necessary to protect your vital interests or those of another person (for example, obtaining health-related information during a medical emergency).</p>
            <h1>Your rights when Creatosaurus is a data controller</h1>
            <p>You may have the following rights: </p>
            <p>a. Right to object to processing: you may request that Creatosaurus stops processing information about you (for example, to stop sending you marketing communications).</p>
            <p>b. Right to restrict processing: you may request that we restrict processing information about you (for example, where you believe that this information is inaccurate).</p>
            <p>c. Right to data portability: you may request that we provide you with information Creatosaurus has about you in a structured, machine-readable, and commonly used format, and you may request that we transfer this information to another data controller.</p>
            <h1>Creatosaurus as a data processor</h1>
            <p>Where you are using our Services and making decisions about the personal data that is being processed in the Services (including selecting the Social Network accounts you wish to connect to the Services, or uploading and using Content), you are acting as a data controller and Creatosaurus is acting as a data processor.</p>
            <p>There are certain obligations under the GDPR that you have as a data controller, including being responsible for managing Content on the Services. As a data processor, Creatosaurus will only access and process Content to provide you with the Services in accordance with your instructions (which you provide through the Services), the Terms of Service, the Social Networks’ terms, and applicable laws. As part of delivering the Services, we may process Content to further improve the Services, such as enhancing usability and developing new features.</p>
            <p>If you, as a data controller, require Creatosaurus to agree to data protection requirements under Article 28, GDPR, or under UK data protection laws. Creatosaurus makes available  data processing addendum that meets these requirements. <a href="https://www.creatosaurus.io/contact" target='_blank' rel="noreferrer noopener">Contact Us</a></p>
            <p>If you are using the Services as an authorized user of a Creatosaurus customer (whether that customer is your employer, another organization, or an individual), that customer determines its own policies (if any) regarding storage, access, modification, deletion, sharing, and retention of personal data and Content, which may apply to your use of the Services. Please check with that customer about the policies and settings it has in place.</p>




            <h1>12. Updates to this Privacy Policy</h1>
            <p>
              This Privacy Policy may be updated from time to time for any
              reason; each version will apply to information collected while it
              was in place. We will notify you of any modifications to our
              Privacy Policy by posting the new Privacy Policy on our Site and
              indicating the date of the latest revision. You are advised to
              consult this Privacy Policy regularly for any changes. In the
              event that the modifications materially alter your rights or
              obligations hereunder, we will make reasonable efforts to notify
              you of the change. For example, we may send a message to your
              email address or generate a pop-up or similar notification when
              you access the Service for the first time after such material
              changes are made. Your continued use of the Service after the
              revised Privacy Policy has become effective indicates that you
              have read, understood and agreed to the current version of this
              Privacy Policy.
            </p>
            <h1>13. Your California privacy rights</h1>
            <p>
              California law affords California residents certain additional
              rights regarding our collection and use of your personal
              information. If you would like to exercise your rights to your
              Personal Information, you may contact us by emailing us at
              contact@creatosaurus.io.
            </p>
            <h1>14. Contact us</h1>
            <p>
              Creatosaurus is the entity responsible for the processing of your
              Personal Information. If you have any questions or comments
              regarding this Privacy Policy, or if you would like to exercise
              your rights to your Personal Information, you may contact us by
              emailing us at contact@creatosaurus.io.
            </p>
          </div>
        </div>
      </div>
    );
  };

  const blackBox = () => {
    return (
      <div className={privacyStyle.blackBoxContainer}>
        <div className={privacyStyle.blackBox}>
          <div className={privacyStyle.taglines}>
            <h1>You focus on telling stories,</h1>
            <h1> we do everything else.</h1>
            <button>Signup for Free</button>
          </div>
          <div className={privacyStyle.blackBoxImage}>
            <Image
              src='/Assets/Girl4.png'
              width={448}
              height={399}
              alt='Failed to load image'
            />
          </div>
        </div>
      </div>
    );
  };

  return (
    <div>
      <HeadInfo />
      <Navigation />
      {privacyPage()}
      {blackBox()}
      <Footer />
    </div>
  );
};

export default PrivacyPolicy;
