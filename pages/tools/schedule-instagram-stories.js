import React, { useState } from 'react'
import Head from 'next/head';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import museStyle from '../../styles/muse.module.css';
import Image from 'next/image';
import Image6 from '../../public/Assets/cache/6.jpeg'
import FrequentlyAskedQuestions from '../../Components/FaqSection';
import FifthBlackSection from '../../Components/LandingPageComponents/FifthBlackSection';
import Footer from '../../Components/LandingPageComponents/Footer';
import apps from "../../appsInfo";

import Image1 from '../../public/Assets/cache/1.jpeg'
import Image2 from '../../public/Assets/cache/2.jpeg'
import Image3 from '../../public/Assets/cache/3.jpeg'
import Image4 from '../../public/Assets/cache/4.jpeg'
import Image5 from '../../public/Assets/cache/5.webp'

import RatingComponent from '../../Components/RatingComponent';

const ScheduleInstagramStories = () => {
    const openUrl = (url) => {
        window.open(url)
    }

    const [showVideo, setShowVideo] = useState(false)

    const svg = {
        leftArrow: (
            <svg
                width='14'
                height='10'
                viewBox='0 0 14 10'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
            >
                <path
                    d='M1.07129 5H12.6338M12.6338 5L8.88379 1.25M12.6338 5L8.88379 8.75'
                    stroke='black'
                    strokeWidth='1.5'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                />
            </svg>
        ),
    };

    let faq = [
        {
            id: 1,
            title: "Is Instagram Story Scheduler free?",
            content: "Yes, the Instagram story scheduler provided by Creatosaurus is absolutely free. Take advantage of this valuable tool to schedule and manage your Instagram stories effortlessly.",
        }, {
            id: 2,
            title: "Can you schedule Instagram Stories and Reels?",
            content: "Yes, with Creatosaurus, you can schedule both your Instagram stories and reels, saving you time and helping you stay consistent in your content strategy.",
        }, {
            id: 3,
            title: "Which time is best for a story on Instagram?",
            content: "The best time to post a story on Instagram varies, but typically it's during peak active hours of your audience.",
        }, {
            id: 4,
            title: "Which type of Instagram accounts are supported for scheduling?",
            content: "Scheduling posts on Instagram is only supported for business accounts. Personal accounts do not have this feature. To schedule your posts, simply convert your personal account to a business account and enjoy the convenience of planning and organizing your content in advance.",
        }, {
            id: 5,
            title: "Does Creatosaurus provide more feature apart from scheduling Instagram Stories?",
            content: "Yes, it provides more features for scheudling like YouTube Shorts or Facebook Video Scheduler or Twitter Tweets or Pinterest Pins and more. Creatosaurus is the way to unlock the efficiency of your work with it’s all in one social media marketing platform. Get started for free.",
        }
    ]

    let data = [
        {
            title: "Importance of Scheduling Instagram Stories for Your Business",
            p: "Explore the importance of scheduling Instagram stories for your business and maximize your impact with an Instagram story planner. With strategic planning and timely updates, you can maintain brand consistency, engage your audience, and drive business growth. Take control of your Instagram storytelling and unlock your business's full potential.",
            image: Image1
        }
    ]

    let data2 = [
        {
            title: "Set a specific date and time for scheduling your story using the Instagram story planner",
            p: "Take Full Control with the Instagram Story Planner. Set Specific Dates and Times to Publish Your Stories, Engage Your Audience at Optimal Moments, and Streamline Your Social Media Strategy for Maximum Impact. Empower Your Instagram Presence with Our Intuitive Scheduling Tool Today.",
            image: Image4
        },
        {
            title: "Best app to schedule Instagram stories",
            p: "Our Instagram story planner allows you to seamlessly schedule and organize your stories in advance. Plan your content, add captions, and ensure your stories are posted at the perfect time. Take control of your Instagram strategy now!",
            image: Image3
        },
        {
            title: "Efficiently Save and Create Drafts of Instagram Stories with our Instagram Story Scheduler",
            p: "Optimize Your Instagram Story Process: Save Valuable Time and Effort with Our Instagram Story Scheduler. Effortlessly Create and Manage Drafts of Stories, All in One Convenient Platform. Unlock the Full Potential of Your Instagram Storytelling with our Advanced Story Planner. Get Started Today and Elevate Your Instagram Game!",
            image: Image2
        }
    ]

    return (
        <div>
            <Head>
                <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
                <title>Schedule Instagram Stories for Free | Instagram Story Scheduler</title>
                <meta
                    name="title"
                    content="Schedule Instagram Stories for Free | Instagram Story Scheduler"
                />
                <meta
                    name="description"
                    content="Effortlessly manage & schedule Instagram stories for free with our Instagram Story Scheduler. Save time, stay consistent, and boost Instagram story engagement."
                />
                <meta
                    property="og:title"
                    content="Schedule Instagram Stories for Free | Instagram Story Scheduler"
                    key="title"
                />
                <meta
                    property="og:description"
                    content="Effortlessly manage & schedule Instagram stories for free with our Instagram Story Scheduler. Save time, stay consistent, and boost Instagram story engagement."
                    key="description"
                />
            </Head>
            <NavigationBar />


            <div className={museStyle.top_block}>
                <div className={museStyle.dataBlocks_dataBox_reverse}>
                    <div className={museStyle.blockImage} style={{ boxShadow: 'none' }}>
                        <Image src={Image6} alt='Create Free New Year Card Templates Online. Customizable & Printable 2023 New Year Graphic Templates' />
                    </div>
                    <div className={museStyle.blockText}>
                        {
                            /**
                             * <div className={museStyle.blockText_icon}>
                            <Image src={logo} alt='Failed to load image' />
                        </div>
                             */
                        }
                        <div className={museStyle.blockText_title}>
                            <h1>Schedule Instagram Stories Effortlessly for Free</h1>
                        </div>
                        <div className={museStyle.blockText_description}>
                            <p>Our Instagram story scheduler simplifies your content planning and posting process, ensuring timely and consistent delivery of engaging stories to captivate your audience.</p>
                        </div>
                        <div className={museStyle.blockText_button}>
                            <button
                                style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                onClick={() => {
                                    openUrl("https://www.cache.creatosaurus.io/");
                                }}>Upload Your Story
                                <svg style={{ marginLeft: 10 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#FFFFFF">
                                    <path d="M12 22v-9m0 0l3.5 3.5M12 13l-3.5 3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#FFFFFF" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                                </svg>
                            </button>
                        </div>
                        <span style={{ color: '#828282', fontSize: 10 }}>Try for free. No credit card required</span>
                    </div>
                </div>
            </div>

            <RatingComponent />

            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.cache.creatosaurus.io/");
                                        }}>Upload Your Story
                                        <svg style={{ marginLeft: 10 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#FFFFFF">
                                            <path d="M12 22v-9m0 0l3.5 3.5M12 13l-3.5 3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#FFFFFF" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>


            <div className={museStyle.how_to_container}>
                <h2>How to use an Instagram Story Scheduler in 3 easy steps</h2>
                <div className={museStyle.card_container}>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="2" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <rect width="7" height="5" rx="0.6" transform="matrix(1 0 0 -1 3 22)" stroke="#000000" strokeWidth="2" />
                            <rect width="7" height="5" rx="0.6" transform="matrix(1 0 0 -1 8.5 7)" stroke="#000000" strokeWidth="2" />
                            <rect width="7" height="5" rx="0.6" transform="matrix(1 0 0 -1 14 22)" stroke="#000000" strokeWidth="2" />
                            <path d="M6.5 17v-3.5a2 2 0 012-2h7a2 2 0 012 2V17M12 11.5V7" stroke="#000000" strokeWidth="2" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>1. Connect your Instagram account</h4>
                        <p>Connect your Instagram account to our Instagram Story Planner to start scheduling and organizing your stories with ease.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="2" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#FFFFFF">
                            <path d="M12 22v-9m0 0l3.5 3.5M12 13l-3.5 3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#000000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>2. Upload your Instagram Story</h4>
                        <p>Upload your Instagram Story from your computer using our Instagram Story Planner, making it convenient and efficient to share content seamlessly.</p>
                    </div>
                    <div className={museStyle.card}>
                        <svg style={{ width: 35, height: 35, marginBottom: 15 }} width="16px" height="16px" strokeWidth="2" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000">
                            <path d="M9 2h6M12 10v4M12 22a8 8 0 100-16 8 8 0 000 16z" stroke="#000000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        <h4 style={{ marginBottom: 10 }}>3. Schedule Your Instagram Stories</h4>
                        <p>Schedule Your Instagram Stories with Time Adjustment. Plan and publish your</p>
                    </div>
                </div>
            </div>


            <div className={museStyle.dataBlocks} style={{ marginTop: 100 }}>
                {data2.map((res, index) => {
                    return (
                        <div
                            className={
                                index % 2 == 0
                                    ? museStyle.dataBlocks_dataBox
                                    : museStyle.dataBlocks_dataBox_reverse
                            }
                            key={`blocks-data-muse-${index}`}
                        >
                            <div className={museStyle.blockImage}>
                                <Image src={res.image} alt={res.title} />
                            </div>
                            <div className={museStyle.blockText}>
                                <div className={museStyle.blockText_title}>
                                    <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 10 }}>{res.title}</h2>
                                </div>
                                <div className={museStyle.blockText_description}>
                                    <p>{res.p}</p>
                                </div>
                                <div className={museStyle.blockText_button}>
                                    <button
                                        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                                        onClick={() => {
                                            openUrl("https://www.cache.creatosaurus.io/");
                                        }}>Upload Your Story
                                        <svg style={{ marginLeft: 10 }} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" color="#FFFFFF">
                                            <path d="M12 22v-9m0 0l3.5 3.5M12 13l-3.5 3.5M20 17.607c1.494-.585 3-1.918 3-4.607 0-4-3.333-5-5-5 0-2 0-6-6-6S6 6 6 8c-1.667 0-5 1-5 5 0 2.689 1.506 4.022 3 4.607" stroke="#FFFFFF" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>

            <div className={museStyle.videoContainer}>
                <iframe
                    className={museStyle.videoBlock}
                    src='https://www.youtube.com/embed/38iFIMfbEzI'
                    title='Instagram Story Scheduler Tool | Schedule Instagram Stories for Free | Cache by Creatosaurus'
                    allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                    allowFullScreen
                ></iframe>
            </div>


            <div className={museStyle.tipsBlock}>
                <h2 style={{ fontSize: 34, fontWeight: 600, marginBottom: 30 }}>5 tips to maximize your Instagram Stories engagement</h2>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>1. Consistent Posting Schedule :</span>  Maintain a regular posting schedule using the Instagram story scheduler for increased engagement.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>2. Strategic Content Planning : </span> Plan and schedule your story content in advance to align with your marketing goals.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>3. Engaging Visuals : </span> Use high-quality images and videos to capture the audience&apos;s attention and boost engagement.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>4. Interactive Features : </span> Incorporate polls, quizzes, and question stickers to encourage audience interaction and participation.</p>
                <p style={{ fontSize: 20, marginBottom: 15 }}><span style={{ fontSize: 20, fontWeight: 500 }}>5. Analyze and Optimize : </span> Monitor analytics, track engagement metrics, and adjust your strategy to optimize future story content.</p>
            </div>


            <div className={museStyle.redPoster} style={{ backgroundColor: '#0078FF' }}>
                <div className={museStyle.redPosterContainer}>
                    <div className={museStyle.redPosterTop}>
                        <div className={museStyle.redPosterHeading}>
                            <h2 style={{ fontSize: 34, fontWeight: 700, color: "#FFFFFF" }}>Free Social Post Scheduling & Social Media Management Tool</h2>
                        </div>
                        <div className={museStyle.redPosterPara}>
                            <p>
                                Streamline your social media presence with our scheduler tool. Stay organized and save time with our intuitive social media scheduler. Effortlessly plan, schedule, and automate your social media posts on Facebook, Twitter, Instagram, YouTube, Pinterest, LinkedIn, Tumblr and more...
                            </p>
                        </div>
                        <div className={museStyle.redPosterButton}>
                            <button onClick={() => openUrl("https://www.cache.creatosaurus.io/")}>Get Started for Free {svg.leftArrow}</button>
                        </div>
                    </div>
                    <div className={museStyle.redPosterBottom}>
                        <svg onClick={()=> setShowVideo(true)} style={showVideo ? { display: 'none' } : null} width="16px" height="16px" strokeWidth="1.5" viewBox="0 0 24 24" fill="#FFF" xmlns="http://www.w3.org/2000/svg" color="#FFF">
                            <path d="M6.906 4.537A.6.6 0 006 5.053v13.894a.6.6 0 00.906.516l11.723-6.947a.6.6 0 000-1.032L6.906 4.537z" stroke="#FFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                        </svg>
                        {
                            showVideo ? <iframe
                                className={museStyle.videoBlock}
                                src='https://www.youtube.com/embed/eS5tpAUEuzA?start=732&autoplay=1'
                                title='YouTube video player'
                                allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
                                allowFullScreen
                            >
                            </iframe> : <Image src={Image5} alt='muse_img loading...' />
                        }
                    </div>
                </div>
            </div>

            <div className={museStyle.discover_apps_container}>
                <div className={museStyle.more_apps_container}>
                    <h1>Discover more:</h1>
                    <div className={museStyle.button_container}>
                        {apps.map((data, index) => {
                            return <button key={index} onClick={() => {
                                window.open(data.url);
                            }}>{data.title}</button>;
                        })}
                    </div>
                </div>
            </div>

            <div className={museStyle.questionsContainer}>
                <div className={museStyle.heading}>
                    <span>Frequently Asked Questions</span>
                </div>
                {faq.map(({ title, content, id }) => (
                    <FrequentlyAskedQuestions title={title} content={content} key={id} />
                ))}
            </div>

            <FifthBlackSection />
            <Footer />
        </div>
    )
}

export default ScheduleInstagramStories