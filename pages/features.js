import React from 'react'
import styles from '../styles/Feature.module.css';
import Navbar from '../Components/LandingPageComponents/NavigationBar';
import Footer from '../Components/LandingPageComponents/Footer';
import Head from 'next/head';

const features = () => {

  const data = [
    {
      emoji: '✍🏻',
      title: 'AI Content Writer',
      description:
        'Create marketing copies in a matter of minutes with our AI copy generator.',
    },
    {
      emoji: '🖼',
      title: 'Create Beautiful Graphics',
      description:
        'Our easy-to-use comprehensive design editor lets you create amazing designs.',
    },
    {
      emoji: '📂',
      title: 'Upload & Organise',
      description:
        'Access your assets across the platform by uploading and organizing them.',
    },
    {
      emoji: '#️⃣',
      title: 'Hashtag Manager',
      description:
        'Find & manage your hashtags to optimize your social media workflow.',
    },
    {
      emoji: '🔍',
      title: 'Quote Finder',
      description:
        'Search from more than a million quotes and save them for later use.',
    },
    {
      emoji: '📅',
      title: 'Schedule Your Post',
      description:
        'Manage your social media accounts and save time by scheduling posts.',
    },
    {
      emoji: '🗺',
      title: 'Templates',
      description:
        'Thousands of visually appealing graphic templates for your creativity.',
    },
    {
      emoji: '🏞',
      title: 'Free Stock Assets',
      description:
        'Take advantage of millions of copyright-free stock assets to stand out.',
    },
    {
      emoji: '📊',
      title: 'Manage & Analyse',
      description:
        'Assess and improve your account data and content performance.',
    },
  ];

  return (
    <React.Fragment>
      <Head>
        <link rel='icon' href='/Assets/creatosaurusfavicon.ico' />
      </Head>
      <Navbar />
      <div className={styles.featuresContainer}>
        <div className={styles.head}>
          <h3>Features that you ❤️ in one place</h3>
          <a href="https://www.app.creatosaurus.io/signup">
            <button>Get Started-It&apos;s Free</button>
          </a>
        </div>

        <div className={styles.grid}>
          {
            data.map((data, index) => {
              return <div key={index}>
                <span>{data.emoji}</span>
                <h4>{data.title}</h4>
                <p>{data.description}</p>
              </div>
            })
          }
        </div>

        <div className={styles.blackBox}>
          <h4>You focus on telling stories, we do everything else.</h4>
          <a href="https://www.app.creatosaurus.io/signup">
            <button>Signup for Free</button>
          </a>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  )
}

export default features