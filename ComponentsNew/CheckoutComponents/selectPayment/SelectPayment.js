import React, { useState, useEffect } from 'react'
import styles from './SelectPayment.module.css'
import axios from 'axios'
import jwtDecode from 'jwt-decode'
import constant from '../../../constant'
import CryptoJS from 'crypto-js'
import { v4 as uuidv4 } from 'uuid';
import Image from 'next/image'
import Stripe from '../paymentAssets/stripe.svg'
import PayPalImage from '../paymentAssets/paypal.svg'
import CC from '../paymentAssets/ccavenue.svg'
import PhonePe from '../paymentAssets/phonepe.svg'
import { toast } from 'react-toastify'
const ct = require('countries-and-timezones');
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import PaypalPaymentForm from '../../../Components/Paypal/PaypalPaymentForm'

const countryList = ["Afghanistan", "Åland Islands", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Ascension Island", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Caribbean Netherlands", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo - Brazzaville", "Congo - Kinshasa", "Cook Islands", "Costa Rica", "Côte d’Ivoire", "Croatia", "Curaçao", "Cyprus", "Czechia", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Eswatini", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong SAR China", "Hungary", "Iceland", "India", "Indonesia", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao SAR China", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar (Burma)", "Namibia", "Nauru", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "North Macedonia", "Norway", "Oman", "Pakistan", "Palestinian Territories", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Réunion", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", "São Tomé & Príncipe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Sint Maarten", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia & South Sandwich Islands", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Barthélemy", "St Helena", "St Kitts & Nevis", "St Lucia", "St Martin", "St Pierre & Miquelon", "St Vincent & Grenadines", "Suriname", "Svalbard & Jan Mayen", "Sweden", "Switzerland", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tokelau", "Tonga", "Trinidad & Tobago", "Tristan da Cunha", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Wallis & Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"];

const SelectPayment = ({ planDuration, planName, showIndianPrice, invitationCode, userDetails, isBlakFridayDeal }) => {
    const userData = ["654f11e77678036f8117e6dd", "63d9f3acb6ebb5b19865444b", "654f26fa7678036f8117e954", "654fa5077678036f8117f6f8", "60c2097766b6965311ea5956", "655880d37b55de18e5bbdf7d", "60c2087a66b6965311ea5951", "6113504ad02ac268a2f9b596", "6534b46d260bf8f289ff4000", "62396355bd67be0b54c652ce", "624d77357ec3dda91d7ca433"]

    const codes = [{
        code: "bf10",
        discount: 10
    }, {
        code: "nvxcz",
        discount: 99.95
    }, {
        code: "nv34$@f",
        discount: 93
    }]

    const [name, setName] = useState("")
    const [lastName, setLastName] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [country, setCountry] = useState("")
    const [state, setState] = useState("")
    const [city, setCity] = useState("")
    const [street, setStreet] = useState("")
    const [zipCode, setZipCode] = useState("")
    const [loading, setLoading] = useState(false)

    const [showCompanyDetails, setShowCompanyDetails] = useState(false)
    const [companyName, setCompanyName] = useState("")
    const [gstNumber, setGstNumber] = useState("")
    const [companyAddress, setCompanyAddress] = useState("")

    const [haveCouponCode, setHaveCouponCode] = useState(false)
    const [couponCode, setCouponCode] = useState("")
    const [couponCodeApplied, setCouponCodeApplied] = useState(false)

    const [activePaymentGateway, setActivePaymentGateway] = useState("credit")
    const [paypalClientToken, setPaypalClientToken] = useState("");
    const [paypalPaymentInfo, setPaypalPaymentInfo] = useState(null)


    // NOTE: Paypal Payment Options
    const initialOptions = {
        "client-id": "Adfxbh1lifScdMC-AUUSj4zgcderQ2tRVBnx1HRAiw4dsCcJjEpzJNErb39VB9BVmdpmZ1GXlHVyugWT",
        "data-client-token": paypalClientToken,
        components: "hosted-fields,buttons",
        "enable-funding": "paylater,venmo",
        "disable-funding": "card",
        "data-sdk-integration-source": "integrationbuilder_ac",
    };


    // NOTE: get paypal client token
    const getPaypalClientToken = async () => {
        try {
            setLoading(true)
            const response = await axios.post(`${constant.url}paypal/client/token`)
            setPaypalClientToken(response.data.token)
            setLoading(false)
        } catch (error) {
            console.log(error)
            setLoading(false)
        }
    }


    useEffect(() => {
        const timezone = ct.getTimezone(Intl.DateTimeFormat().resolvedOptions().timeZone);
        if (timezone !== null) {
            if (timezone.countries.length !== 0) {
                const countries = ct.getAllCountries()
                const countryData = countries[timezone.countries[0]]
                if (countryData !== null || countryData !== undefined) {
                    setCountry(countryData.name)
                }
            }
        }
    }, [])


    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    const formatCurrencyAmount = (amount) => {
        const roundedAmount = Number(amount).toFixed(2);

        const regex = /^-?\d{1,15}(\.\d{2})?$/;

        if (!regex.test(roundedAmount)) {
            throw new Error("Invalid currency amount format");
        }

        const parts = roundedAmount.split(".");

        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        const formattedAmount = parts.join(".");

        return formattedAmount;
    };

    const generateShortId = (length) => {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let shortId = '';
        for (let i = 0; i < length; i++) {
            const randomIndex = Math.floor(Math.random() * characters.length);
            shortId += characters.charAt(randomIndex);
        }
        return shortId;
    }

    const pay = async () => {
        try {
            if (loading) return

            if (name.trim() === "") return toast.error("please enter your first name")
            if (lastName.trim() === "") return toast.error("please enter your last name")
            if (country.trim() === "") return toast.error("plese select your country")
            if (state.trim() === "") return toast.error("plese enter your state name")
            if (city.trim() === "") return toast.error("plese enter your city name")
            if (zipCode.trim() === "") return toast.error("plese enter your zip code")

            if (showCompanyDetails) {
                if (companyName.trim() === "") return toast.error("plese enter your company name")
                if (companyAddress.trim() === "") return toast.error("plese enter your company address")
            }

            setLoading(true)
            const value = getCookie('Xh7ERL0G');
            const decoded = jwtDecode(value)
            const priceWithoutDiscount = parseFloat(checkOriginalPrice().discountPrice - calculateCouponCodeDiscount()).toFixed(2)
            const taxPrice = calculateTax()
            const couponCodeDiscount = couponCodeApplied ? codes.find((data) => data.code === couponCode.toLocaleLowerCase()) : null
            const originalPrice = parseFloat((checkOriginalPrice().discountPrice + calculateTax()) - calculateCouponCodeDiscount()).toFixed(2)
            const planId = planName === "agency" ? "625fd4c1c7770407d20177fc" : planName === "startup" ? "6247081137b553074fb6b258" : "6247085137b553074fb6b25a"

            const userDetails = {
                firstName: name,
                lastName: lastName,
                phoneNumber: phoneNumber,
                country: country,
                state: state,
                city: city,
                street: street,
                zipCode: zipCode,
                showCompanyDetails: showCompanyDetails,
                companyName: companyName,
                taxNumber: gstNumber,
                companyAddress: companyAddress
            }

            let response;
            if (activePaymentGateway === "credit") {
                response = await axios.post(constant.url + "stripe/checkout", {
                    planName: planName === "agency" ? "Agency" : planName === "startup" ? "Startup" : "Creator",
                    currency: showIndianPrice ? "inr" : "usd",
                    amount: originalPrice * 100,
                    userId: decoded.id,
                    subscriptionType: planDuration === "monthly" ? "monthly" : "yearly",
                    planId: planId,
                    hasm: CryptoJS.SHA256(originalPrice * 100).toString(),
                    hpsl: CryptoJS.SHA256(planId).toString(),
                    userDetails: userDetails,
                    invitationCode: invitationCode,
                    originalPriceOfPlan: parseFloat(priceWithoutDiscount).toFixed(2),
                    tax: parseFloat(taxPrice).toFixed(2),
                    couponCode: couponCodeDiscount
                });
            } else if (activePaymentGateway === "paypal") {
                await getPaypalClientToken()
                setPaypalPaymentInfo({
                    order_id: uuidv4(),
                    planName: planName === "agency" ? "Agency" : planName === "startup" ? "Startup" : "Creator",
                    currency: showIndianPrice ? "INR" : "USD",
                    amount: formatCurrencyAmount(originalPrice),
                    userId: decoded.id,
                    subscriptionType: planDuration === "monthly" ? "monthly" : "yearly",
                    planId: planId,
                    hasm: CryptoJS.SHA256(formatCurrencyAmount(originalPrice)).toString(),
                    hpsl: CryptoJS.SHA256(planId).toString(),
                    userDetails: userDetails,
                    invitationCode: invitationCode,
                    originalPriceOfPlan: parseFloat(priceWithoutDiscount).toFixed(2),
                    tax: parseFloat(taxPrice).toFixed(2),
                    couponCode: couponCodeDiscount
                })
            } else if (activePaymentGateway === "phonepe") {
                response = await axios.post(constant.url + "phonepe/checkout", {
                    order_id: 'M' + generateShortId(33),
                    planName: planName === "agency" ? "Agency" : planName === "startup" ? "Startup" : "Creator",
                    currency: showIndianPrice ? "INR" : "USD",
                    amount: originalPrice,
                    userId: decoded.id,
                    subscriptionType: planDuration === "monthly" ? "monthly" : "yearly",
                    planId: planId,
                    hasm: CryptoJS.SHA256(originalPrice).toString(),
                    hpsl: CryptoJS.SHA256(planId).toString(),
                    userDetails: userDetails,
                    invitationCode: invitationCode,
                    originalPriceOfPlan: parseFloat(priceWithoutDiscount).toFixed(2),
                    tax: parseFloat(taxPrice).toFixed(2),
                    couponCode: couponCodeDiscount
                });
            } else {
                response = await axios.post(constant.url + "ccavenue/checkout", {
                    order_id: uuidv4(),
                    planName: planName === "agency" ? "Agency" : planName === "startup" ? "Startup" : "Creator",
                    currency: showIndianPrice ? "INR" : "USD",
                    amount: originalPrice,
                    userId: decoded.id,
                    subscriptionType: planDuration === "monthly" ? "monthly" : "yearly",
                    planId: planId,
                    hasm: CryptoJS.SHA256(originalPrice).toString(),
                    hpsl: CryptoJS.SHA256(planId).toString(),
                    userDetails: userDetails,
                    invitationCode: invitationCode,
                    originalPriceOfPlan: parseFloat(priceWithoutDiscount).toFixed(2),
                    tax: parseFloat(taxPrice).toFixed(2),
                    couponCode: couponCodeDiscount
                });
            }

            setLoading(false)

            if (activePaymentGateway !== "paypal") {
                window.open(response.data, "_self", "noopener,noreferrer");
            }
        } catch (error) {
            setLoading(false)
            console.log(error)
        }
    }

    const checkOriginalPrice = (planDiscountOnMonth) => {
        if (planDiscountOnMonth) {
            if (isBlakFridayDeal) {
                if (showIndianPrice) {
                    return { originalPrice: planName === "agency" ? (12999 * 3) : planName === "startup" ? 4999 : 0 }
                } else {
                    return { originalPrice: planName === "agency" ? (220 * 3) : planName === "startup" ? 80 : 0 }
                }
            } else {
                if (showIndianPrice) {
                    return { originalPrice: planName === "agency" ? (12999 * 2) : planName === "startup" ? (4999 * 2) : (1999 * 2) }
                } else {
                    return { originalPrice: planName === "agency" ? (220 * 2) : planName === "startup" ? (80 * 2) : (30 * 2) }
                }
            }
        }

        if (isBlakFridayDeal) {
            if (planDuration === "monthly") {
                if (showIndianPrice) {
                    return {
                        originalPrice: planName === "agency" ? 12999 : planName === "startup" ? 4999 : 1999,
                        discountPrice: planName === "agency" ? 12999 : planName === "startup" ? 4999 : 1999,
                    }
                } else {
                    return {
                        originalPrice: planName === "agency" ? 220 : planName === "startup" ? 80 : 30,
                        discountPrice: planName === "agency" ? 220 : planName === "startup" ? 80 : 30,
                    }
                }
            } else {
                if (showIndianPrice) {
                    return {
                        originalPrice: planName === "agency" ? (12999 * 9) : planName === "startup" ? (4999 * 11) : (1999 * 12),
                        discountPrice: planName === "agency" ? (1444 * 9) : planName === "startup" ? (499 * 11) : (167 * 12),
                    }
                } else {
                    return {
                        originalPrice: planName === "agency" ? (220 * 9) : planName === "startup" ? (80 * 11) : (30 * 12),
                        discountPrice: planName === "agency" ? (17.67 * 9) : planName === "startup" ? (6.28 * 11) : (2.47 * 12),
                    }
                }
            }
        } else {
            if (planDuration === "monthly") {
                if (showIndianPrice) {
                    return {
                        originalPrice: planName === "agency" ? 12999 : planName === "startup" ? 4999 : 1999,
                        discountPrice: planName === "agency" ? 12999 : planName === "startup" ? 4999 : 1999,
                    }
                } else {
                    return {
                        originalPrice: planName === "agency" ? 220 : planName === "startup" ? 80 : 30,
                        discountPrice: planName === "agency" ? 220 : planName === "startup" ? 80 : 30,
                    }
                }
            } else {
                if (showIndianPrice) {
                    return {
                        originalPrice: planName === "agency" ? (12999 * 12) : planName === "startup" ? (4999 * 12) : (1999 * 12),
                        discountPrice: planName === "agency" ? (12999 * 10) : planName === "startup" ? (4999 * 10) : (1999 * 10),
                    }
                } else {
                    return {
                        originalPrice: planName === "agency" ? (220 * 12) : planName === "startup" ? (80 * 12) : (30 * 12),
                        discountPrice: planName === "agency" ? (220 * 9) : planName === "startup" ? (80 * 11) : (30 * 12),
                    }
                }
            }
        }
    }

    const calculatePercentage = () => {
        if (isBlakFridayDeal) {
            return parseInt((92 / 100) * (checkOriginalPrice().originalPrice))
        } else {
            return checkOriginalPrice().originalPrice * 2
        }
    }

    const calculateTax = () => {
        if (showIndianPrice) {
            return (18 / 100) * (checkOriginalPrice().discountPrice - calculateCouponCodeDiscount())
        } else {
            return 0
        }
    }

    const checkCouponCode = () => {
        let isCouponCodeThere = codes.find((data) => data.code === couponCode.toLocaleLowerCase())

        if (isCouponCodeThere) {
            if (isCouponCodeThere.code === "nvxcz" /*&& userData.includes(userDetails.id)*/) {
                setCouponCodeApplied(true)
                toast.success("Coupon code applied")
            }

            if (isCouponCodeThere.code !== "nvxcz") {
                setCouponCodeApplied(true)
                toast.success("Coupon code applied")
            }
        }
    }

    const calculateCouponCodeDiscount = () => {
        let isCouponCodeThere = codes.find((data) => data.code === couponCode.toLocaleLowerCase())

        if (couponCodeApplied) {
            if (planDuration === "monthly") {
                return parseFloat((isCouponCodeThere.discount / 100) * (checkOriginalPrice().originalPrice)).toFixed(2)
            } else {
                return parseFloat((isCouponCodeThere.discount / 100) * (checkOriginalPrice().discountPrice)).toFixed(2)
            }
        } else {
            return 0
        }
    }

    const checkCouponCodeName = () => {
        let isCouponCodeThere = codes.find((data) => data.code === couponCode.toLocaleLowerCase())
        return isCouponCodeThere.code
    }

    return (
        <div className={styles.selectPaymentContainer}>
            <span className={styles.title}>2. Select payment</span>
            <div className={styles.paymentSectionWrapper}>
                <div className={styles.buttonContainer}>
                    <button onClick={() => setActivePaymentGateway("credit")} className={activePaymentGateway === "credit" ? `${styles.gateWay}` : `${styles.border}`}>
                        <div className={activePaymentGateway === "credit" ? `${styles.check}` : `${styles.circle}`}>
                            {activePaymentGateway === "credit" ? <div className={styles.round} /> : null}
                        </div>
                        <div className={styles.wrapper}>
                            <span>Credit Card</span>
                            <div className={styles.logo}>
                                <Image src={Stripe} alt='credit card' />
                            </div>
                        </div>
                    </button>

                    <button style={showIndianPrice ? null : { display: 'none' }} onClick={() => setActivePaymentGateway("phonepe")} className={activePaymentGateway === "phone" ? `${styles.gateWay}` : `${styles.border}`}>
                        <div className={activePaymentGateway === "phonepe" ? `${styles.check}` : `${styles.circle}`}>
                            {activePaymentGateway === "phonepe" ? <div className={styles.round} /> : null}
                        </div>
                        <div className={styles.wrapper}>
                            <span>PhonePe</span>
                            <div className={styles.logo}>
                                <Image src={PhonePe} alt='Phone' />
                            </div>
                        </div>
                    </button>

                    <button style={showIndianPrice ? { display: 'none' } : null} onClick={() => setActivePaymentGateway("paypal")} className={activePaymentGateway === "paypal" ? `${styles.gateWay}` : `${styles.border}`}>
                        <div className={activePaymentGateway === "paypal" ? `${styles.check}` : `${styles.circle}`}>
                            {activePaymentGateway === "paypal" ? <div className={styles.round} /> : null}
                        </div>
                        <div className={styles.wrapper}>
                            <span>PayPal</span>
                            <div className={styles.logo}>
                                <Image src={PayPalImage} alt='paypal' />
                            </div>
                        </div>
                    </button>

                    <button style={showIndianPrice ? null : { display: 'none' }} onClick={() => setActivePaymentGateway("debit")} className={activePaymentGateway === "debit" ? `${styles.gateWay}` : `${styles.border}`}>
                        <div className={activePaymentGateway === "debit" ? `${styles.check}` : `${styles.circle}`}>
                            {activePaymentGateway === "debit" ? <div className={styles.round} /> : null}
                        </div>
                        <div className={styles.wrapper}>
                            <span>CCAvenue</span>
                            <div className={styles.logo}>
                                <Image src={CC} alt='debit card' />
                            </div>
                        </div>
                    </button>
                </div>
                <div className={styles.info}>
                    <div className={styles.head}>
                        {
                            isBlakFridayDeal ?
                                <span style={{ textTransform: 'capitalize' }}>{planName} Plan - {planDuration === "monthly" ? "1" : planName === "agency" ? "9" : planName === "startup" ? "11" : "12"} Month Plan</span>
                                :
                                <span style={{ textTransform: 'capitalize' }}>{planName} Plan - {planDuration === "monthly" ? "1" : "10"} Month Plan</span>
                        }
                        <div><span style={planDuration === "monthly" ? { display: 'none' } : null}>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice().originalPrice}</span>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice().discountPrice}</div>
                    </div>

                    {
                        isBlakFridayDeal ?
                            <div style={planDuration === "monthly" || planName === "creator" ? { display: 'none' } : { marginTop: 30 }} className={styles.head}>
                                <span style={{ fontWeight: 500 }}>{planName === "agency" ? "3 Month Free" : planName === "startup" ? "1 Month Free" : null}</span>
                                <div><span>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice(true).originalPrice}</span> {showIndianPrice ? "₹" : "$"}0</div>
                            </div>
                            :
                            <div style={planDuration === "monthly" ? { display: 'none' } : { marginTop: 30 }} className={styles.head}>
                                <span style={{ fontWeight: 500 }}>2 Month Free</span>
                                <div><span>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice(true).originalPrice}</span> {showIndianPrice ? "₹" : "$"}0</div>
                            </div>
                    }

                    <div className={styles.divider} />

                    <div className={styles.companyWrapper}>
                        <span style={{ margin: 0 }}>Billing Details</span>
                        <div title="Billing details will be used for invoice purpose" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft: 5 }}>
                            <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.00065 11.8333C8.18954 11.8333 8.34787 11.7694 8.47565 11.6416C8.60343 11.5138 8.66732 11.3555 8.66732 11.1666C8.66732 10.9777 8.60343 10.8194 8.47565 10.6916C8.34787 10.5638 8.18954 10.4999 8.00065 10.4999C7.81176 10.4999 7.65343 10.5638 7.52565 10.6916C7.39787 10.8194 7.33398 10.9777 7.33398 11.1666C7.33398 11.3555 7.39787 11.5138 7.52565 11.6416C7.65343 11.7694 7.81176 11.8333 8.00065 11.8333ZM7.33398 9.16658H8.66732V5.16658H7.33398V9.16658ZM8.00065 15.1666C7.07843 15.1666 6.21176 14.9916 5.40065 14.6416C4.58954 14.2916 3.88398 13.8166 3.28398 13.2166C2.68398 12.6166 2.20898 11.911 1.85898 11.0999C1.50898 10.2888 1.33398 9.42214 1.33398 8.49992C1.33398 7.5777 1.50898 6.71103 1.85898 5.89992C2.20898 5.08881 2.68398 4.38325 3.28398 3.78325C3.88398 3.18325 4.58954 2.70825 5.40065 2.35825C6.21176 2.00825 7.07843 1.83325 8.00065 1.83325C8.92287 1.83325 9.78954 2.00825 10.6007 2.35825C11.4118 2.70825 12.1173 3.18325 12.7173 3.78325C13.3173 4.38325 13.7923 5.08881 14.1423 5.89992C14.4923 6.71103 14.6673 7.5777 14.6673 8.49992C14.6673 9.42214 14.4923 10.2888 14.1423 11.0999C13.7923 11.911 13.3173 12.6166 12.7173 13.2166C12.1173 13.8166 11.4118 14.2916 10.6007 14.6416C9.78954 14.9916 8.92287 15.1666 8.00065 15.1666ZM8.00065 13.8333C9.48954 13.8333 10.7507 13.3166 11.784 12.2833C12.8173 11.2499 13.334 9.98881 13.334 8.49992C13.334 7.01103 12.8173 5.74992 11.784 4.71659C10.7507 3.68325 9.48954 3.16659 8.00065 3.16659C6.51176 3.16659 5.25065 3.68325 4.21732 4.71659C3.18398 5.74992 2.66732 7.01103 2.66732 8.49992C2.66732 9.98881 3.18398 11.2499 4.21732 12.2833C5.25065 13.3166 6.51176 13.8333 8.00065 13.8333Z" fill="#808080" />
                            </svg>
                        </div>
                    </div>

                    <div className={styles.inputWrapper}>
                        <input value={name} type='text' placeholder='First Name' onChange={(e) => setName(e.target.value)} />
                        <input value={lastName} type='text' placeholder='Last Name' onChange={(e) => setLastName(e.target.value)} />
                    </div>

                    <div className={styles.inputWrapper}>
                        <input maxLength={12} type='text' placeholder='Phone Number (optional)' onChange={(e) => setPhoneNumber(e.target.value)} />
                    </div>

                    <div className={styles.inputWrapper}>
                        <select value={country} onChange={(e) => setCountry(e.target.value)}>
                            <option value="" disabled selected>Select your country</option>
                            {
                                countryList.map((data) => {
                                    return <option key={data}>{data}</option>
                                })
                            }
                        </select>
                    </div>

                    <div className={styles.inputWrapper}>
                        <input value={state} type='text' placeholder='State' onChange={(e) => setState(e.target.value)} />
                        <input value={city} type='text' placeholder='City' onChange={(e) => setCity(e.target.value)} />
                    </div>

                    <div className={styles.inputWrapper}>
                        <input value={street} type='text' placeholder='Street Address' onChange={(e) => setStreet(e.target.value)} />
                        <input value={zipCode} type='text' placeholder='ZIP Code' onChange={(e) => setZipCode(e.target.value)} />
                    </div>

                    <div className={styles.companyWrapper}>
                        <div className={styles.checkBox} onClick={() => setShowCompanyDetails((prev) => !prev)} style={showCompanyDetails ? { backgroundColor: '#0078FF', borderColor: '#0078FF' } : null}>
                            <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M5.125 11.625L7.625 14.125L13.875 7.875" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </div>
                        <span>Add company details (Optional)</span>
                        <div title="Company details will be used for invoice purpose" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.00065 11.8333C8.18954 11.8333 8.34787 11.7694 8.47565 11.6416C8.60343 11.5138 8.66732 11.3555 8.66732 11.1666C8.66732 10.9777 8.60343 10.8194 8.47565 10.6916C8.34787 10.5638 8.18954 10.4999 8.00065 10.4999C7.81176 10.4999 7.65343 10.5638 7.52565 10.6916C7.39787 10.8194 7.33398 10.9777 7.33398 11.1666C7.33398 11.3555 7.39787 11.5138 7.52565 11.6416C7.65343 11.7694 7.81176 11.8333 8.00065 11.8333ZM7.33398 9.16658H8.66732V5.16658H7.33398V9.16658ZM8.00065 15.1666C7.07843 15.1666 6.21176 14.9916 5.40065 14.6416C4.58954 14.2916 3.88398 13.8166 3.28398 13.2166C2.68398 12.6166 2.20898 11.911 1.85898 11.0999C1.50898 10.2888 1.33398 9.42214 1.33398 8.49992C1.33398 7.5777 1.50898 6.71103 1.85898 5.89992C2.20898 5.08881 2.68398 4.38325 3.28398 3.78325C3.88398 3.18325 4.58954 2.70825 5.40065 2.35825C6.21176 2.00825 7.07843 1.83325 8.00065 1.83325C8.92287 1.83325 9.78954 2.00825 10.6007 2.35825C11.4118 2.70825 12.1173 3.18325 12.7173 3.78325C13.3173 4.38325 13.7923 5.08881 14.1423 5.89992C14.4923 6.71103 14.6673 7.5777 14.6673 8.49992C14.6673 9.42214 14.4923 10.2888 14.1423 11.0999C13.7923 11.911 13.3173 12.6166 12.7173 13.2166C12.1173 13.8166 11.4118 14.2916 10.6007 14.6416C9.78954 14.9916 8.92287 15.1666 8.00065 15.1666ZM8.00065 13.8333C9.48954 13.8333 10.7507 13.3166 11.784 12.2833C12.8173 11.2499 13.334 9.98881 13.334 8.49992C13.334 7.01103 12.8173 5.74992 11.784 4.71659C10.7507 3.68325 9.48954 3.16659 8.00065 3.16659C6.51176 3.16659 5.25065 3.68325 4.21732 4.71659C3.18398 5.74992 2.66732 7.01103 2.66732 8.49992C2.66732 9.98881 3.18398 11.2499 4.21732 12.2833C5.25065 13.3166 6.51176 13.8333 8.00065 13.8333Z" fill="#808080" />
                            </svg>
                        </div>
                    </div>

                    {
                        showCompanyDetails ?
                            <>
                                <div className={styles.inputWrapper}>
                                    <input value={companyName} type='text' placeholder='Company Name' onChange={(e) => setCompanyName(e.target.value)} />
                                    <input value={gstNumber} type='text' placeholder={showIndianPrice ? 'GST Number' : 'EIN/VAT/CIN/TAX Number'} onChange={(e) => setGstNumber(e.target.value)} />
                                </div>

                                <div className={styles.inputWrapper}>
                                    <input value={companyAddress} type='text' placeholder='Company Address' onChange={(e) => setCompanyAddress(e.target.value)} />
                                </div>
                            </> : null
                    }

                    <div className={styles.divider} />

                    <div className={styles.row}>
                        {
                            isBlakFridayDeal ?
                                <span>Plan Discount - {planDuration === "monthly" ? "0%" : "92%"}</span>
                                :
                                <span>Plan Discount - {planDuration === "monthly" ? "0%" : "2 Months Free"}</span>
                        }


                        {
                            isBlakFridayDeal ?
                                <span style={{ color: '#0078FF' }}>-{showIndianPrice ? "₹" : "$"}{planDuration === "monthly" ? 0 : calculatePercentage()}</span>
                                :
                                <span style={{ color: '#0078FF' }}>-{showIndianPrice ? "₹" : "$"}{planDuration === "monthly" ? 0 : calculatePercentage()}</span>
                        }
                    </div>

                    {
                        couponCodeApplied ? <div className={styles.row}>
                            <span>Coupon Code Discount</span>
                            <span style={{ color: '#0078FF' }}>-{showIndianPrice ? "₹" : "$"}{calculateCouponCodeDiscount()}</span>
                        </div> : null
                    }

                    <div className={styles.row}>
                        <div>
                            <span>Taxes & Fees</span>
                            <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8.00065 11.8333C8.18954 11.8333 8.34787 11.7694 8.47565 11.6416C8.60343 11.5138 8.66732 11.3555 8.66732 11.1666C8.66732 10.9777 8.60343 10.8194 8.47565 10.6916C8.34787 10.5638 8.18954 10.4999 8.00065 10.4999C7.81176 10.4999 7.65343 10.5638 7.52565 10.6916C7.39787 10.8194 7.33398 10.9777 7.33398 11.1666C7.33398 11.3555 7.39787 11.5138 7.52565 11.6416C7.65343 11.7694 7.81176 11.8333 8.00065 11.8333ZM7.33398 9.16658H8.66732V5.16658H7.33398V9.16658ZM8.00065 15.1666C7.07843 15.1666 6.21176 14.9916 5.40065 14.6416C4.58954 14.2916 3.88398 13.8166 3.28398 13.2166C2.68398 12.6166 2.20898 11.911 1.85898 11.0999C1.50898 10.2888 1.33398 9.42214 1.33398 8.49992C1.33398 7.5777 1.50898 6.71103 1.85898 5.89992C2.20898 5.08881 2.68398 4.38325 3.28398 3.78325C3.88398 3.18325 4.58954 2.70825 5.40065 2.35825C6.21176 2.00825 7.07843 1.83325 8.00065 1.83325C8.92287 1.83325 9.78954 2.00825 10.6007 2.35825C11.4118 2.70825 12.1173 3.18325 12.7173 3.78325C13.3173 4.38325 13.7923 5.08881 14.1423 5.89992C14.4923 6.71103 14.6673 7.5777 14.6673 8.49992C14.6673 9.42214 14.4923 10.2888 14.1423 11.0999C13.7923 11.911 13.3173 12.6166 12.7173 13.2166C12.1173 13.8166 11.4118 14.2916 10.6007 14.6416C9.78954 14.9916 8.92287 15.1666 8.00065 15.1666ZM8.00065 13.8333C9.48954 13.8333 10.7507 13.3166 11.784 12.2833C12.8173 11.2499 13.334 9.98881 13.334 8.49992C13.334 7.01103 12.8173 5.74992 11.784 4.71659C10.7507 3.68325 9.48954 3.16659 8.00065 3.16659C6.51176 3.16659 5.25065 3.68325 4.21732 4.71659C3.18398 5.74992 2.66732 7.01103 2.66732 8.49992C2.66732 9.98881 3.18398 11.2499 4.21732 12.2833C5.25065 13.3166 6.51176 13.8333 8.00065 13.8333Z" fill="#808080" />
                            </svg>
                        </div>
                        <span>{showIndianPrice ? "₹" : "$"}{parseFloat(calculateTax()).toFixed(2)}</span>
                    </div>

                    <div className={styles.divider} />

                    <div className={styles.head} style={{ marginTop: 30 }}>
                        <span className={styles.total}>Total</span>
                        {
                            isBlakFridayDeal ?
                                <div><span style={planDuration === "monthly" ? { display: 'none' } : null}>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice().originalPrice}</span> {showIndianPrice ? "₹" : "$"}{parseFloat((checkOriginalPrice().discountPrice + calculateTax()) - calculateCouponCodeDiscount()).toFixed(2)}</div>
                                :
                                <div><span style={planDuration === "monthly" ? { display: 'none' } : null}>{showIndianPrice ? "₹" : "$"}{checkOriginalPrice().originalPrice * 12}</span> {showIndianPrice ? "₹" : "$"}{parseFloat((checkOriginalPrice().discountPrice + calculateTax()) - calculateCouponCodeDiscount()).toFixed(2)}</div>
                        }
                    </div>

                    {
                        couponCodeApplied ? null :
                            <div className={styles.companyWrapper}>
                                <div className={styles.checkBox} onClick={() => setHaveCouponCode((prev) => !prev)} style={haveCouponCode ? { backgroundColor: '#0078FF', borderColor: '#0078FF' } : null}>
                                    <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.125 11.625L7.625 14.125L13.875 7.875" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                    </svg>
                                </div>
                                <span>Have a coupon code?</span>
                            </div>
                    }

                    {
                        couponCodeApplied ? <div className={styles.couponCodeApplied}>
                            <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect y="0.5" width="20" height="20" rx="3" fill="white" />
                                <path d="M5.125 11.625L7.625 14.125L13.875 7.875" stroke="#009B77" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                            <span>Coupon applied!</span>
                            <button>
                                {checkCouponCodeName()}
                                <svg onClick={() => setCouponCodeApplied(false)} width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M4.60065 10.3333L7.00065 7.93325L9.40065 10.3333L10.334 9.39992L7.93398 6.99992L10.334 4.59992L9.40065 3.66658L7.00065 6.06659L4.60065 3.66658L3.66732 4.59992L6.06732 6.99992L3.66732 9.39992L4.60065 10.3333ZM7.00065 13.6666C6.07843 13.6666 5.21176 13.4916 4.40065 13.1416C3.58954 12.7916 2.88398 12.3166 2.28398 11.7166C1.68398 11.1166 1.20898 10.411 0.858984 9.59992C0.508984 8.78881 0.333984 7.92214 0.333984 6.99992C0.333984 6.0777 0.508984 5.21103 0.858984 4.39992C1.20898 3.58881 1.68398 2.88325 2.28398 2.28325C2.88398 1.68325 3.58954 1.20825 4.40065 0.858252C5.21176 0.508252 6.07843 0.333252 7.00065 0.333252C7.92287 0.333252 8.78954 0.508252 9.60065 0.858252C10.4118 1.20825 11.1173 1.68325 11.7173 2.28325C12.3173 2.88325 12.7923 3.58881 13.1423 4.39992C13.4923 5.21103 13.6673 6.0777 13.6673 6.99992C13.6673 7.92214 13.4923 8.78881 13.1423 9.59992C12.7923 10.411 12.3173 11.1166 11.7173 11.7166C11.1173 12.3166 10.4118 12.7916 9.60065 13.1416C8.78954 13.4916 7.92287 13.6666 7.00065 13.6666ZM7.00065 12.3333C8.48954 12.3333 9.75065 11.8166 10.784 10.7833C11.8173 9.74992 12.334 8.48881 12.334 6.99992C12.334 5.51103 11.8173 4.24992 10.784 3.21659C9.75065 2.18325 8.48954 1.66659 7.00065 1.66659C5.51176 1.66659 4.25065 2.18325 3.21732 3.21659C2.18398 4.24992 1.66732 5.51103 1.66732 6.99992C1.66732 8.48881 2.18398 9.74992 3.21732 10.7833C4.25065 11.8166 5.51176 12.3333 7.00065 12.3333Z" fill="black" />
                                </svg>
                            </button>
                        </div> : null
                    }

                    {
                        haveCouponCode && !couponCodeApplied ?
                            <div className={styles.couponCodeWrapper}>
                                <input value={couponCode} type='text' placeholder='Enter a coupon code' onChange={(e) => setCouponCode(e.target.value)} />
                                <button style={couponCode.trim().length === 0 ? { cursor: 'pointer' } : { cursor: 'pointer', backgroundColor: '#0078FF' }} onClick={checkCouponCode}>Apply</button>
                            </div> : null
                    }

                    <div className={styles.divider} />

                    <div className={styles.payWrapper} style={activePaymentGateway === "paypal" && paypalClientToken !== "" ? {
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "center"
                    } : null}>
                        {activePaymentGateway === "paypal" && paypalClientToken !== "" ? <PayPalScriptProvider options={initialOptions}>
                            <PaypalPaymentForm payment={paypalPaymentInfo} />
                        </PayPalScriptProvider> : <button style={{ cursor: 'pointer' }} onClick={pay}>{loading ? "Loading . . ." : "Proceed & Pay"}</button>}

                        <div className={styles.info1} style={activePaymentGateway === "paypal" && paypalClientToken !== "" ? {
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            gap: "20px"
                        } : null}>
                            <div>
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M8 14C6.46667 14 5.13056 13.4917 3.99167 12.475C2.85278 11.4583 2.2 10.1889 2.03333 8.66667H3.4C3.55556 9.82222 4.06944 10.7778 4.94167 11.5333C5.81389 12.2889 6.83333 12.6667 8 12.6667C9.3 12.6667 10.4028 12.2139 11.3083 11.3083C12.2139 10.4028 12.6667 9.3 12.6667 8C12.6667 6.7 12.2139 5.59722 11.3083 4.69167C10.4028 3.78611 9.3 3.33333 8 3.33333C7.23333 3.33333 6.51667 3.51111 5.85 3.86667C5.18333 4.22222 4.62222 4.71111 4.16667 5.33333H6V6.66667H2V2.66667H3.33333V4.23333C3.9 3.52222 4.59167 2.97222 5.40833 2.58333C6.225 2.19444 7.08889 2 8 2C8.83333 2 9.61389 2.15833 10.3417 2.475C11.0694 2.79167 11.7028 3.21944 12.2417 3.75833C12.7806 4.29722 13.2083 4.93056 13.525 5.65833C13.8417 6.38611 14 7.16667 14 8C14 8.83333 13.8417 9.61389 13.525 10.3417C13.2083 11.0694 12.7806 11.7028 12.2417 12.2417C11.7028 12.7806 11.0694 13.2083 10.3417 13.525C9.61389 13.8417 8.83333 14 8 14ZM9.86667 10.8L7.33333 8.26667V4.66667H8.66667V7.73333L10.8 9.86667L9.86667 10.8Z" fill="#009B77" />
                                </svg>
                                14 Day Money Back Guarantee
                            </div>
                            <div>
                                <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M3.99935 14.6667C3.63268 14.6667 3.31879 14.5362 3.05768 14.2751C2.79657 14.014 2.66602 13.7001 2.66602 13.3334V6.66675C2.66602 6.30008 2.79657 5.98619 3.05768 5.72508C3.31879 5.46397 3.63268 5.33342 3.99935 5.33342H4.66602V4.00008C4.66602 3.07786 4.99102 2.29175 5.64102 1.64175C6.29102 0.991748 7.07713 0.666748 7.99935 0.666748C8.92157 0.666748 9.70768 0.991748 10.3577 1.64175C11.0077 2.29175 11.3327 3.07786 11.3327 4.00008V5.33342H11.9993C12.366 5.33342 12.6799 5.46397 12.941 5.72508C13.2021 5.98619 13.3327 6.30008 13.3327 6.66675V13.3334C13.3327 13.7001 13.2021 14.014 12.941 14.2751C12.6799 14.5362 12.366 14.6667 11.9993 14.6667H3.99935ZM3.99935 13.3334H11.9993V6.66675H3.99935V13.3334ZM7.99935 11.3334C8.36602 11.3334 8.6799 11.2029 8.94102 10.9417C9.20213 10.6806 9.33268 10.3667 9.33268 10.0001C9.33268 9.63342 9.20213 9.31953 8.94102 9.05842C8.6799 8.7973 8.36602 8.66675 7.99935 8.66675C7.63268 8.66675 7.31879 8.7973 7.05768 9.05842C6.79657 9.31953 6.66602 9.63342 6.66602 10.0001C6.66602 10.3667 6.79657 10.6806 7.05768 10.9417C7.31879 11.2029 7.63268 11.3334 7.99935 11.3334ZM5.99935 5.33342H9.99935V4.00008C9.99935 3.44453 9.8049 2.9723 9.41602 2.58341C9.02713 2.19453 8.5549 2.00008 7.99935 2.00008C7.44379 2.00008 6.97157 2.19453 6.58268 2.58341C6.19379 2.9723 5.99935 3.44453 5.99935 4.00008V5.33342Z" fill="#009B77" />
                                </svg>
                                Encrypted and Secure Payments
                            </div>
                        </div>
                    </div>

                    <p>By checking out you agree with our <a href="https://www.creatosaurus.io/terms" rel="noopener noreferrer" target="_blank">Terms of Service</a> and confirm that you have read our <a href="https://www.creatosaurus.io/privacy" rel="noopener noreferrer" target="_blank">Privacy Policy.</a> You can cancel recurring payments at any time.</p>
                </div>
            </div>
        </div>
    )
}

export default SelectPayment