import React, { useState, useEffect } from 'react'
import { useRouter } from 'next/router';
import NavigationBar from '../../Components/LandingPageComponents/NavigationBar';
import Footer from '../../Components/LandingPageComponents/Footer';
import NavigationAuth from '../../Components/NavigationAuth/NavigationAuth';
import NewPricingSection from '../../ComponentsNew/PricingComponents/newPricingSection/NewPricingSection';
import PricingCards from '../../ComponentsNew/PricingComponents/newPricingSection/PricingCards';
import RatingComponent from '../../Components/RatingComponent';
import PriceCutSection from '../../ComponentsNew/BlackFridayComponents/priceCutSection/PriceCutSection';
import CompareWithOtherAiTools from '../../ComponentsNew/PricingComponents/newPricingSection/CompareWithOtherAiTools';
import StoryTelling from '../../ComponentsNew/BlackFridayComponents/storyTelling/StoryTelling';
import PlanCompare from '../../ComponentsNew/PricingComponents/newPricingSection/PlanCompare';
import ContactSales from '../../ComponentsNew/PricingComponents/newPricingSection/ContactSales';
import BlackSectionSales from '../../ComponentsNew/PricingComponents/newPricingSection/BlackSectionSales';
import FAQSection from '../../ComponentsNew/PricingComponents/newPricingSection/FAQSection';
import BlackSeactionBottom from '../../ComponentsNew/PricingComponents/newPricingSection/BlackSeactionBottom';
import CheckoutPage2 from '../../ComponentsNew/CheckOutPopup/CheckoutPage2';
import CheckoutPage1 from '../../ComponentsNew/CheckOutPopup/CheckoutPage1';
import PaymentSuccess from '../../ComponentsNew/CheckOutPopup/PaymentSuccess';
import TrustedPartners from '../../ComponentsNew/PricingComponents/newPricingSection/TrustedPartners';
import PaymentFailer from '../../ComponentsNew/CheckOutPopup/PaymentFailer';
import HeadInfo from '../../ComponentsNew/HeadTag/HeadInfo';

const Index = () => {
  const router = useRouter()
  const [showLogin, setshowLogin] = useState(false)
  const [isYearlyDuration, setIsYearlyDuration] = useState(false)
  const [isIndian, setIsIndian] = useState(false)
  const [selectedPlan, setSelectedPlan] = useState("")
  const [showCheckOut, setShowCheckOut] = useState(false)
  const [showSecondCheckout, setShowSecondCheckout] = useState(false)
  const [paymentComplete, setPaymentComplete] = useState(false)
  const [couponCode, setCouponCode] = useState(null)
  const [paymentFailed, setPaymentFailed] = useState(false)
  const { query } = router

  const plans = [{
    color: "#F8F8F9",
    borderColor: "#E8E8E8",
    tag: "For individual",
    showPremium: false,
    planName: "Creatosaurus Free",
    info: "Create anything & turn your ideas into reality at no cost",
    month: 0,
    year: 0,
    buttonName: "Get Started",
    buttonColor: "#808080",
    buttonHover: "#676767",
    planHead: "Features you'll love:",
    showTrial: false,
    hideMonthYear: true,
    features: ["1  Workspace", "2 Brand Kits", "3 Social accounts", "5k/month AI words credits", "20/month AI image credits", "1 GB Cloud storage", "Easy drag-and-drop editor", "Plan and schedule social content", "AI-generated writing and images", "Pro design, video & document editor", "1000+ content types (social posts, videos, docs and more)", "Professionally designed templates", "3M+ stock photos and graphics"]
  }, {
    id: "6247085137b553074fb6b25a",
    color: "#E6F2FF80",
    borderColor: "#CCE4FF",
    tag: "For creator",
    showPremium: false,
    planName: "Creatosaurus Pro",
    info: "Access premium templates, robust content tools and AI features",
    month: isIndian ? "1000" : "25",
    year: isIndian ? "10000" : "250",
    buttonName: "Get Creatosaurus Pro",
    buttonColor: "#0078FF",
    buttonHover: "#006ce6",
    planHead: "Everything in Free, plus:",
    showTrial: false,
    features: ["2  Workspace", "5 Brand Kits", "5 Social accounts", "30k/month AI words credits", "100/month AI image credits", "10 GB Cloud storage", "Unlimited team members", "All in one social inbox", "Social media analytics", "Unlimited schedule social content", "Quickly resize and translate designs", "Remove backgrounds in a click", "Boost creativity with 20+ AI tools", "Unlimited premium templates", "100M+ photos, videos, graphics, audio", "Online customer support"]
  }, {
    id: "6247081137b553074fb6b258",
    color: "#E6F2FF80",
    borderColor: "#0078FF",
    tag: "For startup",
    showPremium: true,
    planName: "Creatosaurus Teams",
    info: "Enhance collaboration, grow your brand & streamline your workflows.",
    month: isIndian ? "2000" : "50",
    year: isIndian ? "20000" : "500",
    buttonName: "Get Creatosaurus Teams",
    buttonColor: "#0078FF",
    buttonHover: "#006ce6",
    planHead: "Everything in Pro, plus:",
    showTrial: false,
    features: ["3  Workspace", "10 Brand Kits", "15 Social accounts", "100k/month AI words credits", "250/month AI image credits", "50 GB Cloud storage", "Reports and insights", "Competitor analysis", "Hashtag analysis & management", "Ensure brand consistency with approvals", "Edit, comment, and collaborate in real time", "Generate on brand copy with AI", "Online customer support"]
  }, {
    id: "625fd4c1c7770407d20177fc",
    color: "#E6F2FF80",
    borderColor: "#CCE4FF",
    tag: "For agency",
    showPremium: false,
    planName: "Creatosaurus Agency",
    info: "Empower your clients with an all in one creative workplace solution",
    month: isIndian ? "4000" : "100",
    year: isIndian ? "40000" : "1000",
    buttonName: "Get Creatosaurus Agency",
    buttonColor: "#0078FF",
    buttonHover: "#006ce6",
    planHead: "Everything in Teams, plus:",
    showTrial: false,
    features: ["10  Workspace", "100 Brand Kits", "50 Social accounts", "300k/month AI words credits", "750/month AI image credits", "250 GB Cloud storage", "Branded Reports", "Brand templates", "Individual approvals and permission by use case", "Org wide Brand Kits and Templates", "Admin Controls", "Scale your brand and centralise assets", "New app integrations", "Priority customer support"]
  }]

  useEffect(() => {
    if (Intl.DateTimeFormat().resolvedOptions().timeZone === "Asia/Calcutta") {
      setIsIndian(true)
    }

    const script = document.createElement('script');
    script.src = 'https://checkout.razorpay.com/v1/checkout.js';
    script.async = true;
    document.body.appendChild(script);
  }, [])


  useEffect(() => {
    const { id, failed } = query

    if (failed) {
      router.replace('/pricing')
      setPaymentFailed(true)
    } else if (id) {
      const isPlanThere = plans.find((data) => data.id === id)
      if (isPlanThere) {
        router.replace('/pricing')
        setSelectedPlan(isPlanThere)
        setPaymentComplete(true)
      }
    }
  }, [query])


  const changeDuration = (type) => {
    if (type === "yearly") {
      setIsYearlyDuration(true)
    } else {
      setIsYearlyDuration(false)
    }
  }


  const selectPlan = (planDetails) => {
    const token = getCookie("Xh7ERL0G")

    if (token) {
      setSelectedPlan(planDetails)
      setShowCheckOut(true)
    } else {
      setSelectedPlan(planDetails)
      setshowLogin(true)
    }
  }

  const closeCheckoutPage1 = () => {
    setShowCheckOut(false)
  }

  const openSecondCheckoutPage = () => {
    setShowCheckOut(false)
    setShowSecondCheckout(true)
  }

  const handlePaymetComplete = () => {
    setPaymentComplete(true)
  }

  const handleSecondCheckoutBack = () => {
    setShowCheckOut(true)
    setShowSecondCheckout(false)
  }

  const handleCode = (value) => {
    setCouponCode(value)
  }

  const getCookie = (name) => {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }

  const checkLogin = () => {
    const token = getCookie("Xh7ERL0G")
    if (token) {
      window.open("https://www.app.creatosaurus.io/", "_blank")
    } else {
      setshowLogin(true)
    }
  }

  const changeUserLoggedIn = () => {
    setShowCheckOut(true)
    setshowLogin(false)
  }

  return (
    <React.Fragment>
      <HeadInfo />
      <div>
        {
          paymentFailed && <PaymentFailer close={() => setPaymentFailed(false)} />
        }

        {
          paymentComplete && <PaymentSuccess
            selectedPlan={selectedPlan}
            close={() => setPaymentComplete(false)} />
        }

        {
          showCheckOut &&
          <CheckoutPage1
            isYearlyDuration={isYearlyDuration}
            isIndian={isIndian}
            selectedPlan={selectedPlan}
            handleCode={handleCode}
            couponCode={couponCode}
            changeDuration={changeDuration}
            next={openSecondCheckoutPage}
            close={closeCheckoutPage1} />
        }

        {
          showSecondCheckout &&
          <CheckoutPage2
            isYearlyDuration={isYearlyDuration}
            isIndian={isIndian}
            selectedPlan={selectedPlan}
            back={handleSecondCheckoutBack}
            handlePaymetComplete={handlePaymetComplete}
            couponCode={couponCode}
            close={() => setShowSecondCheckout(false)} />
        }

        <NavigationBar />
        {showLogin ? <NavigationAuth changeUserLoggedIn={changeUserLoggedIn} close={() => setshowLogin(false)} /> : null}
        <NewPricingSection isYearlyDuration={isYearlyDuration} changeDuration={changeDuration} />
        <PricingCards plans={plans} checkLogin={checkLogin} isYearlyDuration={isYearlyDuration} isIndian={isIndian} selectPlan={selectPlan} />
        <RatingComponent />
        <PriceCutSection pricing={true} />
        <CompareWithOtherAiTools />
        <StoryTelling hideTitle={true} />
        <PlanCompare checkLogin={checkLogin} isYearlyDuration={isYearlyDuration} isIndian={isIndian} selectPlan={selectPlan} />
        <TrustedPartners />
        <ContactSales />
        <BlackSectionSales />
        <FAQSection />
        <BlackSeactionBottom />
        <Footer />
      </div>
    </React.Fragment>
  )
}

export default Index