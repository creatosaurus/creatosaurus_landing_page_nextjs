import React from 'react'
import Head from 'next/head';
import Auth from '../Components/Auth/Auth'
import HeadInfo from '../ComponentsNew/HeadTag/HeadInfo';

const login = () => {
    return (
        <div className='bg-white h-[100vh] overflow-hidden'>
            <HeadInfo />
            <Auth title="Login" />
        </div>
    )
}

export default login