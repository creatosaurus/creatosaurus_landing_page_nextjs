import { getAllBlogs } from '../lib/posts';



const BlogSitemap = () => { };

export const getServerSideProps = async ({ res }) => {
    const posts = await getAllBlogs();

    const routes = posts.map((post) => `/blog/${post.slug}`);


    const pages = routes;

    const urlSet = pages
        .map((page) => {
            // Remove none route related parts of filename.
            const path = page
                .replace('pages', '')
                .replace('_content', '')
                .replace(/(.tsx|.ts)/, '')
                .replace('.mdx', '');
            // Remove the word index from route
            const route = path === '/index' ? '' : path;
            // Build url portion of sitemap.xml
            return `<url><loc>https://www.creatosaurus.io${route}</loc> <lastmod>${new Date().toISOString()}</lastmod>
      <changefreq>daily</changefreq>
      <priority>1.0</priority></url>`;
        })
        .join('');

    // Add urlSet to entire sitemap string
    const sitemap = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${urlSet}</urlset>`;

    // set response content header to xml
    res.setHeader('Content-Type', 'text/xml');
    // write the sitemap
    res.write(sitemap);
    res.end();

    return {
        props: {},
    };
};

export default BlogSitemap;
