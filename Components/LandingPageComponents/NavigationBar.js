/* eslint-disable react/no-unknown-property */

import React, { useState, useEffect } from 'react'
import styles from '../LandingPageComponentCss/NavigationBar.module.css';
import Menu from '../../public/Assets/menu.svg';
import Logo from '../../public/Assets/Logo.svg';
import Image from 'next/image';
import Link from 'next/link';
import Close from '../../public/Assets/close.svg';
import NavigationAuth from '../NavigationAuth/NavigationAuth';
import { useRouter } from 'next/router'
import axios from 'axios';
import constant from '../../constant';
import jwtDecode from 'jwt-decode';
import AppSumoPopup from '../Auth/AppSumoPopup';
import NavigationAuthGooglePopup from '../NavigationAuthGooglePopup/NavigationAuthGooglePopup';

const NavigationBar = ({ userLoggedIn }) => {
  const router = useRouter()
  const { pathname, query } = router
  const [open, setopen] = useState(false)
  const [showLogin, setshowLogin] = useState(false)
  const [showUserLoggedIn, setShowUserLoggedIn] = useState(false)
  const [isAppSumoDealActive, setIsAppSumoDealActive] = useState(false)
  const [isAppSumoPopupActive, setIsAppSumoPopupActive] = useState(false);
  const [appSumoCode, setAppSumoCode] = useState("")
  const [userDetails, setUserDetails] = useState(null)

  const toggle = () => {
    setopen(prev => !prev)
  }

  function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
  }

  const setRefCookie = (name, value) => {
    if (process.env.NODE_ENV === 'development') {
      document.cookie = `${name}=${value}`
    } else {
      const currentDate = new Date();
      const expiryDate = new Date(currentDate.getTime() + 30 * 24 * 60 * 60 * 1000); // 30 days in milliseconds
      const expires = "expires=" + expiryDate.toUTCString();
      const domain = ".creatosaurus.io";

      document.cookie = `${name}=${value}; ${expires}; domain=${domain}; path=/`;
    }
  }

  useEffect(() => {
    if (query.ref !== undefined) {
      addLinkClicked(query.ref)
      setRefCookie("ref", query.ref)
    }

    if (query.appsumo !== undefined) {
      if (query.appsumo === "blackfriday") {
        setIsAppSumoDealActive(true)
      }
    }

    const value = getCookie('Xh7ERL0G');
    if (value) {
      const decoded = jwtDecode(value);
      const expirationTime = (decoded.exp * 1000) - 60000
      if (Date.now() >= expirationTime) {

      } else {
        setShowUserLoggedIn(true)
        setUserDetails(decoded)
      }
    }
  }, [query.ref])


  const addLinkClicked = async (userId) => {
    try {
      await axios.post(constant.url + "link/clicked", {
        userId: userId
      })
    } catch (error) {
      console.log(error)
    }
  }

  const handleLogin = () => {
    if (isAppSumoDealActive) {
      setIsAppSumoPopupActive(true)
    } else {
      setshowLogin(true)
    }
  }


  const closeAppSumoPopup = () => {
    setIsAppSumoPopupActive(!isAppSumoPopupActive)
    setAppSumoCode("")
  }

  const deleteCookie = (name) => {
    document.cookie = `${encodeURIComponent(name)}=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/; domain=.creatosaurus.io`;
  };

  const logOut = () => {
    deleteCookie('Xh7ERL0G');
    localStorage.clear()
    window.open('https://www.creatosaurus.io', "_self")
  }

  return (
    <React.Fragment>
      {isAppSumoPopupActive ? <AppSumoPopup setAppSumoCode={setAppSumoCode} appSumoCode={appSumoCode} goBack={closeAppSumoPopup} setshowLogin={setshowLogin} setIsAppSumoPopupActive={setIsAppSumoPopupActive} /> :
        showLogin ? <NavigationAuth close={() => setshowLogin(false)} appSumoCode={appSumoCode} /> :
          !showUserLoggedIn && pathname === "/" ? <NavigationAuthGooglePopup appSumoCode={appSumoCode} /> : null}
      {/* Mobile Style */}
      <div className={styles.navbar}>
        <React.Fragment>
          {
            open ?
              // side nav bar
              <div className={styles.sideMenu}>
                <div className={styles.header}>
                  <Link href="/" passHref prefetch={false}>
                    <span className={styles.big}>CREATOSAURUS</span>
                  </Link>


                  <div className={styles.menu} onClick={toggle}>
                    <Image src={Close} alt='close' />
                  </div>
                </div>

                <div className={styles.list}>
                  <Link href="/apps" passHref prefetch={false}>Apps</Link>
                  <Link href="/blog" passHref prefetch={false}>Blog</Link>
                  <Link href="/pricing" passHref prefetch={false}>Pricing</Link>
                  <Link href="/login" passHref prefetch={false}>Login</Link>
                  <Link href="/signup" passHref prefetch={false}>Signup</Link>
                  <Link href="https://calendly.com/malavwarke/creatosaurus" passHref>Book Demo</Link>
                </div>
              </div>
              :
              // navigation bar
              <React.Fragment>
                <div className={styles.row}>
                  <Link href="/" passHref prefetch={false}>
                    <div className={styles.logo}>
                      <Image src={Logo} alt='creatosaurus' />
                    </div>
                  </Link>

                  <Link href="/" passHref prefetch={false}>
                    <span className={styles.big}>CREATOSAURUS</span>
                  </Link>
                </div>

                <div className={styles.menu} onClick={toggle}>
                  <Image src={Menu} alt='menu' />
                </div>
              </React.Fragment>
          }
        </React.Fragment>
      </div>


      <div className='flex justify-center items-center'>
        <div className={styles.navbarLaptop}>
          <div className={styles.row}>
            <Link href="/" passHref>
              <div className={styles.logo} prefetch={false}>
                <Image src={Logo} alt='creatosaurus' />
              </div>
            </Link>

            <Link href="/" passHref prefetch={false}>
              <span className={styles.big}>CREATOSAURUS</span>
            </Link>
          </div>

          <div className={styles.loginButtonContainer} style={pathname === "/apps/muse/waitlist" ? { display: 'none' } : null}>
            {
              /** <a style={{display:'flex', justifyContent:'center', alignItems:'center'}} href="https://www.producthunt.com/posts/noice-3?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-noice&#0045;3" target="_blank" rel="noreferrer">
                <img
                  src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=416515&theme=light"
                  alt="Noice - 11&#0044;876&#0043;&#0032;free&#0032;CC0&#0044;&#0032;open&#0032;source&#0032;illustrations&#0032;library&#0032;in&#0032;SVG | Product Hunt"
                  style={{ width: 150, height: 40 }} width="150" height="40" />
              </a> */
            }
            {
              pathname === "/checkout" && userDetails !== null ?
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <span>{userDetails.userName}</span>
                  <button className={styles.dashboardButton} style={{ width: "auto", border: "none", marginLeft: 15, fontWeight: 500 }} onClick={logOut}>Logout</button>
                </div> :
                pathname === "/affiliate" ? <button className={styles.dashboardButton} onClick={handleLogin}>Join Now</button> :
                  showUserLoggedIn || userLoggedIn ?
                    <button className={styles.dashboardButton} onClick={() => window.open("https://www.app.creatosaurus.io/", "_self")}>Go to Dashboard</button>
                    :
                    <>
                      <a className={styles.bold} href="https://calendly.com/malavwarke/creatosaurus">Book Demo</a>
                      {
                        pathname === "/affiliate" ?
                          <button className={styles.dashboardButton} onClick={handleLogin}>Join Now</button>
                          :
                          <>
                            <button className={styles.loginButton} onClick={handleLogin}>Log in</button>
                            <button className={styles.signupButton} onClick={handleLogin}>Sign up</button>
                          </>
                      }
                    </>
            }
          </div>


          <div className={styles.loginButtonContainer} style={pathname !== "/apps/muse/waitlist" ? { display: 'none' } : null}>
            <button style={{ border: "1px solid #000", borderRadius: 5, padding: '6px 10px', backgroundColor: '#fff', display: 'flex', alignItems: 'center', cursor: 'pointer' }} onClick={() => window.open("https://twitter.com/intent/tweet?text=creatosaurus")}>
              <span style={{ fontWeight: 600 }}>Share</span>
              <svg style={{ marginLeft: 5 }} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M21.1056 6.24544C20.5112 6.50125 19.8863 6.67939 19.2463 6.77544C19.5455 6.72415 19.9857 6.1856 20.161 5.96762C20.4272 5.63881 20.6301 5.26345 20.7594 4.8606C20.7594 4.83068 20.7893 4.78794 20.7594 4.76656C20.7443 4.75833 20.7274 4.75402 20.7102 4.75402C20.693 4.75402 20.6761 4.75833 20.6611 4.76656C19.9662 5.14288 19.2266 5.43009 18.4599 5.62141C18.4331 5.62957 18.4047 5.6303 18.3776 5.62353C18.3505 5.61675 18.3257 5.60272 18.306 5.58294C18.2463 5.51187 18.1821 5.44477 18.1136 5.38205C17.8009 5.10185 17.4461 4.87253 17.0622 4.70245C16.544 4.48984 15.9842 4.39777 15.4252 4.43318C14.8828 4.46743 14.3532 4.61292 13.8694 4.8606C13.393 5.12172 12.9742 5.47646 12.6384 5.9035C12.2851 6.34304 12.0301 6.85314 11.8904 7.39948C11.7752 7.91916 11.7622 8.45627 11.852 8.98093C11.852 9.07069 11.852 9.08351 11.775 9.07069C8.72751 8.6219 6.22711 7.54052 4.18404 5.21963C4.09428 5.11705 4.04727 5.11705 3.97461 5.21963C3.08557 6.57028 3.51727 8.70738 4.62856 9.76311C4.77816 9.90416 4.93203 10.0409 5.09445 10.1692C4.58493 10.133 4.08785 9.99491 3.63267 9.76311C3.54719 9.70755 3.50017 9.73747 3.4959 9.84005C3.48378 9.98226 3.48378 10.1253 3.4959 10.2675C3.58508 10.949 3.85368 11.5947 4.27423 12.1384C4.69478 12.6821 5.25218 13.1044 5.88945 13.362C6.0448 13.4285 6.20667 13.4787 6.37243 13.5116C5.90074 13.6045 5.41696 13.6189 4.94057 13.5543C4.83799 13.533 4.79953 13.5885 4.83799 13.6868C5.4663 15.3965 6.82977 15.918 7.82993 16.2086C7.96671 16.23 8.10348 16.23 8.25735 16.2642C8.25735 16.2642 8.25735 16.2642 8.23171 16.2898C7.93679 16.8284 6.74429 17.1917 6.19719 17.3798C5.1986 17.7384 4.13391 17.8755 3.07703 17.7815C2.91033 17.7559 2.87186 17.7602 2.82912 17.7815C2.78638 17.8029 2.82912 17.8499 2.87614 17.8927C3.08985 18.0337 3.30356 18.1577 3.52582 18.2773C4.18748 18.6382 4.88699 18.9249 5.61162 19.1322C9.36437 20.1665 13.5873 19.4057 16.404 16.6061C18.618 14.4092 19.3959 11.3788 19.3959 8.34408C19.3959 8.22867 19.537 8.16028 19.6182 8.10045C20.1783 7.664 20.6721 7.14858 21.0842 6.57028C21.1556 6.48408 21.1922 6.37432 21.1868 6.26254C21.1868 6.19842 21.1868 6.21125 21.1056 6.24544Z" fill="#000000" />
              </svg>
            </button>
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default NavigationBar