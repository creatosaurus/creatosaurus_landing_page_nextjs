import React, { useState, useEffect } from 'react'
import styles from './Auth.module.css'
import { useRouter } from 'next/router'
import LoginEmailPopup from './LoginEmailPopup';
import axios from 'axios'
import { useGoogleOneTapLogin, useGoogleLogin } from '@react-oauth/google';
import constant from '../../constant';
import jwt_decode from "jwt-decode";
import Link from 'next/link';
import Poster from '../../public/NewAssets/Home/poster.webp'
import Image from 'next/image';

const Auth = (props) => {
    const router = useRouter()
    const { query } = router

    const [invitationCode, setinvitationCode] = useState(null)
    const [loading, setLoading] = useState(false);
    const [redirectTo, setredirectTo] = useState(null);
    const [showLoginPopup, setShowLoginPopup] = useState(false)
    const [affiliate, setAffiliate] = useState(false)

    const setRefCookie = (name, value) => {
        if (process.env.NODE_ENV === 'development') {
            document.cookie = `${name}=${value}`
        } else {
            const currentDate = new Date();
            const expiryDate = new Date(currentDate.getTime() + 30 * 24 * 60 * 60 * 1000); // 30 days in milliseconds
            const expires = "expires=" + expiryDate.toUTCString();
            const domain = ".creatosaurus.io";

            document.cookie = `${name}=${value}; ${expires}; domain=${domain}; path=/`;
        }
    }

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }

    useEffect(() => {
        setredirectTo(query.app)
        setinvitationCode(query.ref)
        setAffiliate(query.affiliate)

        if (query.ref !== undefined) {
            addLinkClicked(query.ref)
            setRefCookie("ref", query.ref)
        } else {
            const value = getCookie('ref');

            if (value !== undefined) {
                setinvitationCode(value)
            }
        }

        const value = getCookie('Xh7ERL0G');
        if (value) {
            const decoded = jwt_decode(value);
            const expirationTime = (decoded.exp * 1000) - 60000
            if (Date.now() >= expirationTime) {
                // Do something if expired
            } else {
                window.open(`https://www.app.creatosaurus.io`, "_self");
            }
        }
    }, [query]);

    const addLinkClicked = async (userId) => {
        try {
            await axios.post(constant.url + "link/clicked", {
                userId: userId
            })
        } catch (error) {

        }
    }

    function setCookie(name, value) {
        if (process.env.NODE_ENV === 'development') {
            document.cookie = `${name}=${value}`
        } else {
            const currentDate = new Date();
            const expiryDate = new Date(currentDate.getTime() + 15 * 24 * 60 * 60 * 1000); // 15 days in milliseconds
            const expires = "expires=" + expiryDate.toUTCString();
            const domain = ".creatosaurus.io";

            document.cookie = `${name}=${value}; ${expires}; domain=${domain}; path=/`;
        }
    }

    const addDataToLocalStorage = (res, email) => {
        try {
            setCookie("Xh7ERL0G", res.data.token)
            setLoading(false);

            const decoded = jwt_decode(res.data.token)

            const queryString = router.asPath.split("?")
            const checkIsNewUser = res.data?.isNewUser

            if (checkIsNewUser) {
                window.open("https://www.app.creatosaurus.io/?newUser=true", "_self");
            } else if (decoded.onboarding_status === false) {
                if (redirectTo !== null) {
                    window.open(`https://www.app.creatosaurus.io/?${redirectTo}`, "_self");
                } else {
                    window.open("https://www.app.creatosaurus.io/", "_self");
                }
            } else if (email === "mayurgaikwad859@gmail.com") {
                window.open("https://www.analytics.creatosaurus.io/", "_self");
            } else if (redirectTo === null) {
                window.open("https://www.app.creatosaurus.io/", "_self");
            } else if (redirectTo === "muse") {
                const category = query.category;
                if (category !== undefined) return window.open(`https://www.muse.creatosaurus.io/editor?${queryString[1]}`, "_self");
                window.open("https://www.muse.creatosaurus.io", "_self");
            } else if (redirectTo === "apollo") {
                window.open("https://www.apollo.creatosaurus.io", "_self");
            } else if (redirectTo === "cache") {
                window.open("https://www.cache.creatosaurus.io", "_self");
            } else if (redirectTo === "captionator") {
                window.open("https://www.captionator.creatosaurus.io/", "_self");
                const template = query.template
                if (template !== undefined) return window.open(`https://www.captionator.creatosaurus.io?${queryString[1]}`, "_self");
            } else if (redirectTo === "quotes") {
                window.open("https://www.quotes.creatosaurus.io/", "_self");
            } else if (redirectTo === "hashtags") {
                window.open("https://www.hashtags.creatosaurus.io", "_self");
            } else if (redirectTo === "steno") {
                window.open("https://www.steno.creatosaurus.io/", "_self");
            } else {
                window.open("https://www.app.creatosaurus.io/", "_self");
            }
        } catch (error) {
            setLoading(false);
        }
    }

    const loginWithGoogle = async (response, type) => {
        try {
            let token = type === "oneTap" ? response.credential : response.access_token

            setLoading(true);
            const res = await axios.post(constant.url + "login", {
                token: token,
                authType: "google",
                type: type,
                timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                invitationCodeFromUser: invitationCode,
                affiliate: affiliate,
                appSumoCode: ""
            })

            setLoading(false)
            addDataToLocalStorage(res)
        } catch (error) {
            setLoading(false);
        }
    }

    useGoogleOneTapLogin({
        onSuccess: res => loginWithGoogle(res, "oneTap")
    })

    const googleLogin = useGoogleLogin({
        onSuccess: res => loginWithGoogle(res, "normal")
    })


    return (
        <div className='flex flex-row p-[30px] gap-x-[20px]  md:gap-x-[80px] justify-between'>
            <div className='hidden md:flex'>
                <div className={styles.img}>
                    <Image src={Poster} alt="" />
                </div>
            </div>
            <div className='h-[calc(100vh-100px)] w-full md:w-[60%]'>

                <Link href="/">
                    <div className='flex items-center cursor-pointer'>
                        <svg width="74" height="35" viewBox="0 0 74 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g clipPath="url(#clip0_3923_28766)">
                                <path d="M49.5625 34.9997L46.3661 33.0225L45.8594 34.9997H49.5625Z" fill="black" />
                                <path d="M54.2871 32.5083L50.0545 30.5615V32.5845L54.2871 32.5083Z" fill="black" />
                                <path d="M45.452 34.8246L41.6956 31.2968L43.6462 27.9023L46.0234 32.6226L45.452 34.8246Z" fill="black" />
                                <path d="M38.6094 7.93945L30.0146 17.5247L5.09516 17.3419L38.6094 7.93945Z" fill="black" />
                                <path d="M59.9128 17.4258L59.9395 22.0736H54.9792L59.9128 17.4258Z" fill="black" />
                                <path d="M57.0146 24.7022L59.9023 22.5078H57.0908L57.0146 24.7022Z" fill="black" />
                                <path d="M41.4388 30.8741L47.5039 20.3135L39.5035 24.2261L41.4388 30.8741Z" fill="black" />
                                <path d="M9.70117 15.601L4.77518 16.984L0.500652 14.4619L9.70117 15.601Z" fill="black" />
                                <path d="M68.8273 13.9059L63.6041 12.1801L62.8688 7.68457L70.1035 13.3458L68.8273 13.9059Z" fill="black" />
                                <path d="M49.623 32.5846L44.537 26.3518L46.7467 22.5078H49.623V32.5846Z" fill="black" />
                                <path d="M47.4121 19.8788L39.1107 23.9362L30.5464 17.5739L39.0955 8.04199L45.3663 10.4497L47.4121 19.8788Z" fill="black" />
                                <path d="M73.209 6.05341L64.7285 8.59069L62.8084 7.08584L61.9169 1.83984H71.0527L73.209 6.05341Z" fill="black" />
                                <path d="M61.4818 1.83984L63.1543 12.0994L59.9198 16.8273L54.2052 11.2842L56.7578 6.38104L61.4818 1.83984Z" fill="black" />
                                <path d="M54.35 22.0738H46.9934L47.9611 20.3899L45.8429 10.6104L53.5309 11.2275L59.6074 17.1212L54.35 22.0738Z" fill="black" />
                                <path d="M66.5576 1.40579H68.6758L67.5633 0L66.5576 1.40579Z" fill="black" />
                            </g>
                            <defs>
                                <clipPath id="clip0_3923_28766">
                                    <rect width="72.7087" height="35" fill="white" transform="matrix(-1 0 0 1 73.209 0)" />
                                </clipPath>
                            </defs>
                        </svg>

                        <span className='text-[20px] font-semibold ml-[5px]'>Creatosaurus</span>
                    </div>
                </Link>

                {
                    showLoginPopup ?
                        <div className='flex justify-center items-center h-full'>
                            <LoginEmailPopup
                                fromPage={true}
                                invitationCode={invitationCode}
                                addDataToLocalStorage={addDataToLocalStorage}
                                closeEmailLogin={() => setShowLoginPopup(false)}
                                affiliate={affiliate} />
                        </div>
                        :
                        <>
                            <div className='flex flex-col justify-center h-full'>
                                <span className='text-[22px] font-semibold'>Log in or sign up in seconds 👋🏻</span>
                                <p className='text-[14px] text-[#333333] mt-[15px]'>Use your Google account or email to continue with Creatosaurus (it&apos;s free)!</p>

                                <button onClick={googleLogin} className='flex justify-center items-center bg-[#FAFAFA] border-[1px] border-[#DCDCDC] mt-[30px] text-[16px] font-semibold h-[50px] w-full rounded-[10px]'>
                                    {
                                        loading ?
                                            <div role="status">
                                                <svg aria-hidden="true" className="size-[24px] text-white animate-spin fill-[#ff4359]" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor" />
                                                    <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill" />
                                                </svg>
                                                <span className="sr-only">Loading...</span>
                                            </div>
                                            :
                                            <>
                                                <svg className='size-[28px] mr-[20px]' width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M29.5741 13.3887H28.5001V13.3334H16.5001V18.6667H24.0354C22.9361 21.7714 19.9821 24.0001 16.5001 24.0001C12.0821 24.0001 8.50008 20.4181 8.50008 16.0001C8.50008 11.5821 12.0821 8.00008 16.5001 8.00008C18.5394 8.00008 20.3947 8.76941 21.8074 10.0261L25.5787 6.25475C23.1974 4.03541 20.0121 2.66675 16.5001 2.66675C9.13675 2.66675 3.16675 8.63675 3.16675 16.0001C3.16675 23.3634 9.13675 29.3334 16.5001 29.3334C23.8634 29.3334 29.8334 23.3634 29.8334 16.0001C29.8334 15.1061 29.7414 14.2334 29.5741 13.3887Z" fill="#FFC107" />
                                                    <path d="M4.7041 9.79408L9.08477 13.0067C10.2701 10.0721 13.1408 8.00008 16.5001 8.00008C18.5394 8.00008 20.3948 8.76941 21.8074 10.0261L25.5788 6.25475C23.1974 4.03541 20.0121 2.66675 16.5001 2.66675C11.3788 2.66675 6.93743 5.55808 4.7041 9.79408Z" fill="#FF3D00" />
                                                    <path d="M16.4999 29.3333C19.9439 29.3333 23.0732 28.0153 25.4392 25.872L21.3125 22.38C19.9739 23.394 18.3099 24 16.4999 24C13.0319 24 10.0872 21.7886 8.97788 18.7026L4.62988 22.0526C6.83655 26.3706 11.3179 29.3333 16.4999 29.3333Z" fill="#4CAF50" />
                                                    <path d="M29.574 13.3886H28.5V13.3333H16.5V18.6666H24.0353C23.5073 20.1579 22.548 21.4439 21.3107 22.3806L21.3127 22.3793L25.4393 25.8713C25.1473 26.1366 29.8333 22.6666 29.8333 15.9999C29.8333 15.1059 29.7413 14.2333 29.574 13.3886Z" fill="#1976D2" />
                                                </svg>
                                                <span>Continue with Google</span>
                                            </>
                                    }
                                </button>

                                <button onClick={() => setShowLoginPopup(true)} className='flex justify-center items-center bg-[#FAFAFA] border-[1px] border-[#DCDCDC] mt-[20px] text-[16px] font-semibold h-[50px] w-full rounded-[10px]'>
                                    <svg className='size-[28px] mr-[20px]' width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M5.45825 28.3334C4.89159 28.3334 4.39575 28.1209 3.97075 27.6959C3.54575 27.2709 3.33325 26.7751 3.33325 26.2084V7.79175C3.33325 7.22508 3.54575 6.72925 3.97075 6.30425C4.39575 5.87925 4.89159 5.66675 5.45825 5.66675H29.5416C30.1083 5.66675 30.6041 5.87925 31.0291 6.30425C31.4541 6.72925 31.6666 7.22508 31.6666 7.79175V26.2084C31.6666 26.7751 31.4541 27.2709 31.0291 27.6959C30.6041 28.1209 30.1083 28.3334 29.5416 28.3334H5.45825ZM17.4999 17.6376L5.45825 9.73966V26.2084H29.5416V9.73966L17.4999 17.6376ZM17.4999 15.5126L29.3999 7.79175H5.63534L17.4999 15.5126ZM5.45825 9.73966V7.79175V26.2084V9.73966Z" fill="black" />
                                    </svg>
                                    <span>Continue with Email</span>
                                </button>

                                <p className='text-[12px] text-[#808080] block mt-[25px] bottom-[40px]'>
                                    By joining, you agree to the Creatosaurus’s&nbsp;
                                    <span className='underline'><Link href="/terms">Terms of Service.</Link></span>&nbsp;Read our&nbsp;
                                    <span className='underline'><Link href="/privacy"> Privacy Policy.</Link></span>
                                </p>
                            </div>
                        </>
                }
            </div>
        </div>
    )
}

export default Auth