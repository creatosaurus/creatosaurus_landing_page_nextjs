import React, { useEffect, useRef, useState } from 'react';
import iro from '@jaames/iro';
import colornamer from 'color-namer';
import convert from 'color-convert';
import tinycolor from 'tinycolor2';

const ColorPicker = () => {
    const colorPickerRef = useRef(null);
    const [colorName, setColorName] = useState("Red");
    const [colorValue, setColorValue] = useState("rgb(255, 0, 0)");
    const [color, setColor] = useState({})
    const [hex, setHex] = useState("#ff0000")

    useEffect(() => {
        const colorPicker = new iro.ColorPicker(colorPickerRef.current, {
            width: 300,
            color: "#ff0000"
        });

        colorPicker.on('color:change', (color) => {
            const rgb = color.rgb;
            const hex = color.hexString;
            setColorValue(`rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`);

            const bestMatch = colornamer(hex, { pick: ['ntc'] });
            setColorName(bestMatch.ntc[0].name);

            const labColor = convert.rgb.lab(rgb.r, rgb.g, rgb.b)

            setHex(hex)

            let obj = {
                HEX: hex,
                LAB: labColor.join(', '),
                RGB: `rgb(${rgb.r}, ${rgb.g}, ${rgb.b})`,
                XYZ: convert.rgb.xyz(rgb.r, rgb.g, rgb.b).join(', '),
                CMYK: convert.rgb.cmyk(rgb.r, rgb.g, rgb.b).join(', '),
                LCH: convert.lab.lch(labColor).join(', '),
                HSB: convert.rgb.hsv(rgb.r, rgb.g, rgb.b).join(', '),
                HSL: convert.rgb.hsl(rgb.r, rgb.g, rgb.b).join(', '),
                HWB: convert.rgb.hwb(rgb.r, rgb.g, rgb.b).join(', '),
                HCG: convert.rgb.hcg(rgb.r, rgb.g, rgb.b).join(', ')
            }

            setColor(obj)
        });
    }, []);

    function getAnalogousColors1(baseColor) {
        const color = new iro.Color(baseColor);
        const baseHue = color.hue;
        const angles = [30, -30]; // Angles for analogous colors
        const analogousColors = angles.map(angle => {
            let newHue = (baseHue + angle) % 360;
            if (newHue < 0) newHue += 360; // Ensure hue is positive
            return new iro.Color({ h: newHue, s: color.saturation, v: color.value });
        });
        let colorsData = analogousColors.map(color => color.hexString);
        return [baseColor, ...colorsData]
    }

    function getTetradColors1(baseColor) {
        const color = new iro.Color(baseColor);
        const baseHue = color.hue;
        const angles = [90, 180, 270]; // Angles for analogous colors
        const analogousColors = angles.map(angle => {
            let newHue = (baseHue + angle) % 360;
            if (newHue < 0) newHue += 360; // Ensure hue is positive
            return new iro.Color({ h: newHue, s: color.saturation, v: color.value });
        });
        let colorsData = analogousColors.map(color => color.hexString);
        return [baseColor, ...colorsData]
    }

    function getTriadicColors1(baseColor) {
        const color = new iro.Color(baseColor);
        const baseHue = color.hue;
        const angles = [120, 240]; // Angles for analogous colors
        const analogousColors = angles.map(angle => {
            let newHue = (baseHue + angle) % 360;
            if (newHue < 0) newHue += 360; // Ensure hue is positive
            return new iro.Color({ h: newHue, s: color.saturation, v: color.value });
        });
        let colorsData = analogousColors.map(color => color.hexString);
        return [baseColor, ...colorsData]
    }

    function getSplitColors1(baseColor) {
        const color = new iro.Color(baseColor);
        const baseHue = color.hue;
        const angles = [150, -150]; // Angles for analogous colors
        const analogousColors = angles.map(angle => {
            let newHue = (baseHue + angle) % 360;
            if (newHue < 0) newHue += 360; // Ensure hue is positive
            return new iro.Color({ h: newHue, s: color.saturation, v: color.value });
        });
        let colorsData = analogousColors.map(color => color.hexString);
        return [baseColor, ...colorsData]
    }

    function getComplementoyColors1(baseColor) {
        const color = new iro.Color(baseColor);
        const baseHue = color.hue;
        const angles = [180]; // Angles for analogous colors
        const analogousColors = angles.map(angle => {
            let newHue = (baseHue + angle) % 360;
            if (newHue < 0) newHue += 360; // Ensure hue is positive
            return new iro.Color({ h: newHue, s: color.saturation, v: color.value });
        });
        let colorsData = analogousColors.map(color => color.hexString);
        return [baseColor, ...colorsData]
    }

    const getAnalogousColors = () => {
        const colors = tinycolor(hex).analogous();
        return colors.map(function (t) { return t.toHexString(); });
    }

    const getMonochromaticColors = () => {
        const colors = tinycolor(hex).monochromatic(4);
        return colors.map(function (t) { return t.toHexString(); });
    }

    const getSplitcomplementColors = () => {
        const colors = tinycolor(hex).splitcomplement();
        return colors.map(function (t) { return t.toHexString(); });
    }

    const getTriadColors = () => {
        const colors = tinycolor(hex).triad();
        return colors.map(function (t) { return t.toHexString(); });
    }

    const getTetradColors = () => {
        const colors = tinycolor(hex).tetrad();
        return colors.map(function (t) { return t.toHexString(); });
    }


    const getComplementColors = () => {
        const colors = tinycolor(hex).complement().toHexString();
        return [hex, colors]
    }


    const generateShades = (baseColor, numberOfShades) => {
        const shades = [];
        for (let i = 0; i < numberOfShades; i++) {
            const ratio = i / (numberOfShades - 1);
            shades.push(tinycolor.mix(baseColor, 'black', ratio * 100).toHexString());
        }
        return shades;
    };

    const generateTints = (baseColor, numberOfShades) => {
        const shades = [];
        for (let i = 0; i < numberOfShades; i++) {
            const ratio = i / (numberOfShades - 1);
            shades.push(tinycolor.mix(baseColor, 'white', ratio * 100).toHexString());
        }
        return shades;
    };

    const generateTones = (hexColor, numOfTones) => {
        let colors = [];
        for (let i = 0; i < numOfTones; i++) {
            const ratio = i / (numOfTones - 1);
            let tone = tinycolor(hexColor).desaturate(ratio * 100).toHexString();
            colors.push(tone);
        }
        return colors;
    };


    const shades = generateShades(hex, 15);
    const tints = generateTints(hex, 15);
    const tones = generateTones(hex, 15);



    const ColorPalette = ({ title, colors }) => (
        <div className="palette">
            <h3>{title}</h3>
            <div style={{ display: 'flex' }}>
                {colors.map((color, index) => (
                    <div
                        key={index}
                        className="color"
                        style={{ backgroundColor: color, width: 100, height: 50, display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                    >
                        {color}
                    </div>
                ))}
            </div>
        </div>
    );

    return (
        <div>
            <div ref={colorPickerRef} style={{ margin: '20px auto', width: '300px', height: '300px' }}></div>
            <p>Selected Color Name: <span>{colorName}</span></p>
            <p>RGB Value: <span>{colorValue}</span></p>
            <p>Conversions</p>
            <div>
                {Object.entries(color)?.map(([key, value]) => (
                    <div key={key}>
                        <strong>{key}:</strong> {value}
                    </div>
                ))}
            </div>

            <div>
                <ColorPalette title="Shades" colors={shades} />
                <ColorPalette title="Tints" colors={tints} />
                <ColorPalette title="Tones" colors={tones} />
            </div>

            <div>
                <ColorPalette title="Analogous" colors={getAnalogousColors1(hex)} />
                <ColorPalette title="Monochromatic" colors={getMonochromaticColors()} />
                <ColorPalette title="Splitcomplement" colors={getSplitColors1(hex)} />
                <ColorPalette title="Triad" colors={getTriadicColors1(hex)} />
                <ColorPalette title="Tetrad" colors={getTetradColors1(hex)} />
                <ColorPalette title="Complement" colors={getComplementoyColors1(hex)} />
            </div>
        </div>
    );
}

export default ColorPicker;
